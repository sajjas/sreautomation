/* Setup the bootstrap default permissioning */  

USE [EYDBSRE]
GO

-- delete process group
delete from [automation].[ProcessGroupMapping]

-- delete from Application Mapping
delete from [automation].[ApplicationMapping]

-- delete user mapping;
delete from [automation].[GroupMapping]

-- delete group;
delete from [automation].[Group]


-- Add Admin group
INSERT INTO [automation].[Group] (GroupName, Description, EmailGroup, StatusId, CreationDate)
VALUES ('ADMIN','ADMIN','test@ey.com','ACTIVE', GETUTCDATE()) ;

-- Add Default Users (Santhosh and Shijo) into ADMIN Group
INSERT INTO [automation].[GroupMapping] (GroupId, UserId, StatusId, CreationDate)
VALUES(1, 1, 'ACTIVE', GETUTCDATE());

INSERT INTO [automation].[GroupMapping] (GroupId, UserId, StatusId, CreationDate)
VALUES(1, 2, 'ACTIVE', GETUTCDATE());

-- New Groups for READ ONLY
INSERT INTO [automation].[Group] (GroupName, Description, EmailGroup, StatusId, CreationDate)
SELECT CONCAT(ApplicationName,'-ReadOnly'),CONCAT(ApplicationName,'-ReadOnly'),ApplicationName, StatusId, GETUTCDATE()
FROM  [automation].[Application]
WHERE StatusId = 'ACTIVE';

-- New Group for EXEC
INSERT INTO [automation].[Group] (GroupName, Description, EmailGroup, StatusId, CreationDate)
SELECT CONCAT(ApplicationName,'-Exec'), CONCAT(ApplicationName,'-Exec'), ApplicationName, StatusId, GETUTCDATE()
FROM  [automation].[Application]
WHERE StatusId = 'ACTIVE';

-- Insert Application Mapping for all
INSERT INTO [automation].[ApplicationMapping] (ApplicationId, GroupId, CreationDate)
SELECT A.id, G.id, GETUTCDATE()
FROM  [automation].[Application] A, [automation].[Group] G
WHERE A.ApplicationName = G.EmailGroup;

-- Add to the Exec GROUP 
INSERT INTO [automation].[ProcessGroupMapping] (GroupId,ProcessId,CreationDate)
SELECT g.Id, p.Id, GETUTCDATE()
FROM  [automation].[Process] p, [automation].[Group] g
WHERE p.Action IN ('BACKUP_NOW', 'REBALANCE_NOW', 'SCHEDULE_REBALANCE')  
and g.GroupName LIKE '%Exec';

-- Add to the READONLY GROUP 
/* Not needed based on Jun-9 Discussions
INSERT INTO ProcessGroupMapping (GroupId,ProcessId,CreationDate)
SELECT g.Id, p.Id,GETDATE()
FROM  Process p, Group g
WHERE 	p.[Action] IN ("One", "Two")
and g.GroupName LIKE "%ReadOnly"
*/

