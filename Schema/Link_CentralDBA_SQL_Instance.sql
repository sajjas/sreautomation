USE [master]
GO

/****** Object:  LinkedServer [USSECVMPDBTSQ01.EY.NET]    Script Date: 6/11/2020 11:59:57 AM ******/
EXEC master.dbo.sp_addlinkedserver @server = N'USSECVMPDBTSQ01.EY.NET', @srvproduct=N'SQL Server'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'USSECVMPDBTSQ01.EY.NET',@useself=N'False',@locallogin=NULL,@rmtuser=N'SRE_Automation',@rmtpassword='########'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'rpc', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'rpc out', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'USSECVMPDBTSQ01.EY.NET', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO


