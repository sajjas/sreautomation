USE [EYDBSRE_UAT]
GO

/****** Object:  Trigger [automation].[after_insert_scheduledtask]    Script Date: 16/06/2020 10:51:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [automation].[after_insert_scheduledtask]

ON  [automation].[ScheduledTasks]
AFTER INSERT, UPDATE AS
BEGIN
	Declare @TaskId Integer;
	select @TaskId= i.id From Inserted i;
	IF UPDATE(ServerName)
		UPDATE automation.ScheduledTasks
		SET ServerNoDomain =CASE 
					WHEN CHARINDEX('.', ServerName) > 0  THEN SUBSTRING(ServerName, 0, CHARINDEX('.', ServerName))
					ELSE ServerName
				  end
		WHERE Id IN (SELECT i.Id from inserted i)
END; 


--Alter table [automation].ScheduledTasks
--Add ServerNoDomain [nvarchar](64) NULL
GO

ALTER TABLE [automation].[ScheduledTasks] ENABLE TRIGGER [after_insert_scheduledtask]
GO


/****** Object:  Trigger [automation].[after_insert_alert]    Script Date: 16/06/2020 10:49:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [automation].[after_insert_alert]

ON  [automation].[Alert]
AFTER INSERT, UPDATE AS
BEGIN
	Declare @AlertId Integer;
	select @AlertId= i.id From Inserted i;
	IF UPDATE(ServerName)
		UPDATE automation.Alert
		SET ServerNoDomain =CASE 
					WHEN CHARINDEX('.', ServerName) > 0  THEN SUBSTRING(ServerName, 0, CHARINDEX('.', ServerName))
					ELSE ServerName
				  end
		WHERE Id IN (SELECT i.Id from inserted i)
END; 
GO

ALTER TABLE [automation].[Alert] ENABLE TRIGGER [after_insert_alert]
GO


/****** Object:  Trigger [automation].[after_insert_ServiceRequest]    Script Date: 16/06/2020 10:47:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [automation].[after_insert_ServiceRequest]

ON  [automation].[ServiceRequest]
AFTER INSERT, UPDATE AS
BEGIN

	IF UPDATE(ServerName)
		UPDATE automation.ServiceRequest
		SET ServerNoDomain =CASE 
					WHEN CHARINDEX('.', ServerName) > 0  THEN SUBSTRING(ServerName, 0, CHARINDEX('.', ServerName))
					ELSE ServerName
				  end
		WHERE Id IN (SELECT i.Id from inserted i)

	IF UPDATE(StatusId)
		UPDATE automation.ServiceRequest
		SET CompletedDate = GETDATE()
		WHERE Id IN (SELECT i.Id from inserted i WHERE i.StatusId = 'COMPLETED')
END; 
GO

ALTER TABLE [automation].[ServiceRequest] ENABLE TRIGGER [after_insert_ServiceRequest]
GO