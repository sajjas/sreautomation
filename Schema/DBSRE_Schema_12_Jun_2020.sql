USE [master]
GO
/****** Object:  Database [EYDBSRE_UAT]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE DATABASE [EYDBSRE_UAT]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EYDBSRE_UAT', FILENAME = N'G:\SQLDATA\EYDBSRE_UAT.mdf' , SIZE = 262144KB , MAXSIZE = UNLIMITED, FILEGROWTH = 524288KB )
 LOG ON 
( NAME = N'EYDBSRE_UAT_log', FILENAME = N'H:\SQLLOG\EYDBSRE_UAT_log.ldf' , SIZE = 131072KB , MAXSIZE = 2048GB , FILEGROWTH = 131072KB )
GO
ALTER DATABASE [EYDBSRE_UAT] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EYDBSRE_UAT].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EYDBSRE_UAT] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET ARITHABORT OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EYDBSRE_UAT] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EYDBSRE_UAT] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EYDBSRE_UAT] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EYDBSRE_UAT] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [EYDBSRE_UAT] SET  MULTI_USER 
GO
ALTER DATABASE [EYDBSRE_UAT] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EYDBSRE_UAT] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EYDBSRE_UAT] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EYDBSRE_UAT] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [EYDBSRE_UAT] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'EYDBSRE_UAT', N'ON'
GO
ALTER DATABASE [EYDBSRE_UAT] SET QUERY_STORE = OFF
GO
USE [EYDBSRE_UAT]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [EYDBSRE_UAT]
GO
/****** Object:  User [US\VZ398HP]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE USER [US\VZ398HP] FOR LOGIN [US\VZ398HP] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [EY\P.SQLMON.1]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE USER [EY\P.SQLMON.1] FOR LOGIN [EY\P.SQLMON.1] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [DigitalOU]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE USER [DigitalOU] FOR LOGIN [DigitalOU] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [US\VZ398HP]
GO
ALTER ROLE [db_owner] ADD MEMBER [DigitalOU]
GO
/****** Object:  Schema [automation]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE SCHEMA [automation]
GO
/****** Object:  Table [automation].[ApplicationMapping]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[ApplicationMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupId] [int] NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_ApplicationMapping9] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_ApplicationMapping_GroupAppId] UNIQUE NONCLUSTERED 
(
	[GroupId] ASC,
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[GroupMapping]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[GroupMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[StatusId] [nvarchar](64) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_GroupMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_GroupMapping_GroupUserId] UNIQUE NONCLUSTERED 
(
	[GroupId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[ProcessGroupMapping]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[ProcessGroupMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupId] [int] NOT NULL,
	[ProcessId] [int] NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_ProcessGroupMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_ProcessGroupMapping_GroupProcessId] UNIQUE NONCLUSTERED 
(
	[GroupId] ASC,
	[ProcessId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Server]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Server](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServerName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[DomainName] [nvarchar](256) NOT NULL,
	[RegionName] [nvarchar](256) NOT NULL,
	[StatusId] [nvarchar](64) NOT NULL,
	[CreationDate] [datetime] NULL,
	[Last_Modified_Date] [datetime] NULL,
	[PhysicalServer] [nvarchar](256) NULL,
	[IsClustered] [tinyint] NULL,
 CONSTRAINT [PK_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[ServerMapping]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[ServerMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[ServerId] [int] NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_ServerMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_ServerMapping_AppServerId] UNIQUE NONCLUSTERED 
(
	[ApplicationId] ASC,
	[ServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [automation].[vw_user_group_app_process_mapping]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [automation].[vw_user_group_app_process_mapping]
WITH SCHEMABINDING
AS
SELECT  A.GroupId, A.ApplicationId, PG.ProcessId, G.UserId, M.ServerId, S.ServerName,S.DomainName
FROM [automation].ProcessGroupMapping PG, [automation].ApplicationMapping A, [automation].[GroupMapping] G, [automation].ServerMapping M, [automation].[Server] S
WHERE PG.GroupId  = A.GroupId
AND PG.GroupId = G.GroupId
AND A.ApplicationId = M.ApplicationId
AND M.ServerId = S.Id;
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [ux_vw_user_group_app_process_mapping]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE CLUSTERED INDEX [ux_vw_user_group_app_process_mapping] ON [automation].[vw_user_group_app_process_mapping]
(
	[ServerId] ASC,
	[UserId] ASC,
	[ProcessId] ASC,
	[ApplicationId] ASC,
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Alert]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Alert](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [nvarchar](64) NOT NULL,
	[CreationDate] [datetime] NULL,
	[StatusId] [nvarchar](64) NOT NULL,
	[Info] [nvarchar](max) NULL,
	[Last_Modified_Date] [datetime] NULL,
	[ServerName] [nvarchar](64) NOT NULL,
	[InstanceName] [nvarchar](64) NOT NULL,
	[DBName] [nvarchar](64) NOT NULL,
	[ServerNoDomain] [nvarchar](64) NULL,
 CONSTRAINT [PK_Alert] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [automation].[AlertSuppression]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[AlertSuppression](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AlertType] [nvarchar](64) NOT NULL,
	[ServerName] [nvarchar](64) NOT NULL,
	[InstanceName] [nvarchar](64) NULL,
	[DBName] [nvarchar](64) NULL,
	[ConfigKeys] [nvarchar](1024) NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AlertSuppression] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Application]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Application](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationName] [nvarchar](250) NOT NULL,
	[Manageable] [bit] NOT NULL,
	[StatusId] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](1024) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Application_ApplicationName] UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[ConfigTemplates]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[ConfigTemplates](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ConfigType] [nvarchar](64) NOT NULL,
	[InstanceType] [nvarchar](64) NULL,
	[ConfigId] [int] NOT NULL,
	[ConfigName] [nvarchar](128) NOT NULL,
	[ValueType] [nvarchar](64) NOT NULL,
	[StdValue] [nvarchar](1024) NULL,
	[CreationDate] [datetime] NULL,
	[DefaultValue] [nvarchar](64) NULL,
 CONSTRAINT [PK_ConfigTemplates] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_ConfigTemplates_ConfigInstance] UNIQUE NONCLUSTERED 
(
	[ConfigType] ASC,
	[InstanceType] ASC,
	[ConfigId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[ConfigValues]    Script Date: 16/06/2020 14:02:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[ConfigValues](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InstanceType] [nvarchar](500) NULL,
	[ConfigType] [nvarchar](500) NULL,
	[ConfigId] [int] NULL,
	[ConfigName] [nvarchar](500) NULL,
	[ConfigDescription] [nvarchar](500) NULL,
	[ConfigClass] [nvarchar](500) NULL,
	[IsShown] [bit] NULL,
	[IsChangeable] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[StdValue] [int] NULL,
	[FEDisplay] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[DBBackupPlans]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[DBBackupPlans](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DBBackupPlanName] [nvarchar](64) NOT NULL,
	[FullBackupFreq] [nvarchar](64) NOT NULL,
	[FullBackupSchedule] [nvarchar](512) NULL,
	[DiffBackupFreq] [nvarchar](64) NOT NULL,
	[DiffBackupSchedule] [nvarchar](512) NULL,
	[LogBackupFreq] [nvarchar](64) NOT NULL,
	[LogBackupSchedule] [nvarchar](512) NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_DBBackupPlans] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_DBBackupPlans_DBBackupPlanName] UNIQUE NONCLUSTERED 
(
	[DBBackupPlanName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Domain]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Domain](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DomainName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_Domain] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Domain] UNIQUE NONCLUSTERED 
(
	[DomainName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[emailTemplate]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[emailTemplate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NOT NULL,
	[Subject] [nvarchar](500) NOT NULL,
	[TopBody] [nvarchar](500) NOT NULL,
	[BottomBody] [nvarchar](500) NOT NULL,
	[From] [nvarchar](500) NOT NULL,
	[Bcc] [nvarchar](500) NULL,
	[Cc] [nvarchar](500) NULL,
	[To] [nvarchar](500) NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_emailTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Entity]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Entity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EntityName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_Entity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Entity] UNIQUE NONCLUSTERED 
(
	[EntityName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Event]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Event](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProcessId] [int] NOT NULL,
	[ServerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateTimeStarted] [datetime] NOT NULL,
	[DateTimeCompleted] [datetime] NOT NULL,
	[ProcessParameters] [nvarchar](500) NOT NULL,
	[ServiceNowIncidentId] [nvarchar](20) NOT NULL,
	[ProcessStatusId] [int] NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Group]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Group](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[EmailGroup] [nvarchar](250) NOT NULL,
	[StatusId] [nvarchar](64) NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Group_GroupName] UNIQUE NONCLUSTERED 
(
	[GroupName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Instance]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Instance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InstanceName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[ServerId] [int] NOT NULL,
	[StatusId] [nvarchar](64) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_Instance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Instance_InstanceServerId] UNIQUE NONCLUSTERED 
(
	[InstanceName] ASC,
	[ServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[LookupMaster]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[LookupMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LookupType] [nvarchar](64) NOT NULL,
	[LookupCode] [nvarchar](64) NOT NULL,
	[LookupName] [nvarchar](250) NOT NULL,
	[CreationDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_LookupMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[NASShare]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[NASShare](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RegionId] [nvarchar](50) NOT NULL,
	[BackupPath] [nvarchar](500) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_NASShare] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_NASShare] UNIQUE NONCLUSTERED 
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Process]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Process](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProcessName] [nvarchar](50) NOT NULL,
	[EntityId] [int] NOT NULL,
	[Action] [nvarchar](250) NOT NULL,
	[StatusId] [nvarchar](64) NULL,
	[CreationDate] [datetime] NULL,
	[scriptFileName] [nvarchar](255) NULL,
 CONSTRAINT [PK_Process] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[Region]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[Region](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RegionName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Region_RegionName] UNIQUE NONCLUSTERED 
(
	[RegionName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[ScheduledJobs]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[ScheduledJobs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobName] [nvarchar](500) NOT NULL,
	[AlertType] [nvarchar](64) NOT NULL,
	[JobTarget] [nvarchar](max) NOT NULL,
	[JobScriptFile] [nvarchar](500) NOT NULL,
	[JobInputs] [nvarchar](2048) NOT NULL,
	[ScheduledJobFrequency] [nvarchar](64) NOT NULL,
	[ScheduledJobTime] [nvarchar](512) NOT NULL,
	[ScheduledJobStatus] [nvarchar](64) NOT NULL,
	[CreatedBy] [nvarchar](64) NOT NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [automation].[ScheduledJobsRunHistory]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[ScheduledJobsRunHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScheduledJobid] [int] NOT NULL,
	[ServerName] [nvarchar](64) NULL,
	[InstanceName] [nvarchar](64) NULL,
	[DBName] [nvarchar](64) NULL,
	[RunStarttime] [datetime] NULL,
	[RunEndtime] [datetime] NULL,
	[RunStatus] [nvarchar](64) NULL,
	[RunOutput] [nvarchar](2048) NULL,
 CONSTRAINT [PK_ScheduledJobsRunHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[ScheduledTasks]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[ScheduledTasks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProcessId] [int] NULL,
	[StatusId] [nvarchar](64) NOT NULL,
	[ServerName] [nvarchar](64) NULL,
	[InstanceName] [nvarchar](64) NULL,
	[DBName] [nvarchar](64) NULL,
	[TaskScript] [nvarchar](64) NULL,
	[TaskInfo] [nvarchar](max) NULL,
	[ScheduledTime] [datetime] NOT NULL,
	[ReminderMins] [int] NOT NULL,
	[ReminderEmail] [nvarchar](64) NOT NULL,
	[ReminderCount] [int] NOT NULL,
	[SNRefNo] [nvarchar](64) NULL,
	[ReturnInfo] [nvarchar](1024) NULL,
	[ScheduledUser] [nvarchar](64) NULL,
	[CreationDate] [datetime] NULL,
	[Last_Modified_Date] [datetime] NULL,
	[CancellationReason] [nvarchar](500) NULL,
	[CancelledUser] [nvarchar](100) NULL,
	[ServerNoDomain] [nvarchar](64) NULL,
 CONSTRAINT [PK_ScheduledTasks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [automation].[ServiceRequest]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[ServiceRequest](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [nvarchar](64) NOT NULL,
	[StatusId] [nvarchar](64) NOT NULL,
	[AlertId] [int] NULL,
	[ServerName] [nvarchar](64) NULL,
	[InstanceName] [nvarchar](64) NULL,
	[DBName] [nvarchar](64) NULL,
	[SNRefNo] [nvarchar](64) NULL,
	[Info] [nvarchar](2048) NULL,
	[Comments] [nvarchar](1024) NULL,
	[RequestedUser] [nvarchar](64) NULL,
	[CreationDate] [datetime] NULL,
	[ServerNoDomain] [nvarchar](64) NULL,
	[CompletedDate] [datetime] NULL,
 CONSTRAINT [PK_ServiceRequest] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [automation].[User]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [automation].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[StatusId] [nvarchar](64) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_User_UserName] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Alert_StatusId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_Alert_StatusId] ON [automation].[Alert]
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Alert_TypeId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_Alert_TypeId] ON [automation].[Alert]
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ_AlertSuppression]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_AlertSuppression] ON [automation].[AlertSuppression]
(
	[AlertType] ASC,
	[ServerName] ASC,
	[InstanceName] ASC,
	[DBName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_Application_ApplicationName]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_Application_ApplicationName] ON [automation].[Application]
(
	[ApplicationName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UX_ApplicationMapping_GroupAppId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_ApplicationMapping_GroupAppId] ON [automation].[ApplicationMapping]
(
	[GroupId] ASC,
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_ConfigTemplates_ConfigInstance]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_ConfigTemplates_ConfigInstance] ON [automation].[ConfigTemplates]
(
	[ConfigType] ASC,
	[InstanceType] ASC,
	[ConfigId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_Domain_DomainName]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_Domain_DomainName] ON [automation].[Domain]
(
	[DomainName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ_emailTemplate]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_emailTemplate] ON [automation].[emailTemplate]
(
	[TemplateName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_Entity]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_Entity] ON [automation].[Entity]
(
	[EntityName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_Group_GroupName]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_Group_GroupName] ON [automation].[Group]
(
	[GroupName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UX_GroupMapping_GroupUserId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_GroupMapping_GroupUserId] ON [automation].[GroupMapping]
(
	[GroupId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_Instance_InstanceServerId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_Instance_InstanceServerId] ON [automation].[Instance]
(
	[InstanceName] ASC,
	[ServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_LookupMaster_LookupTypeCode]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_LookupMaster_LookupTypeCode] ON [automation].[LookupMaster]
(
	[LookupType] ASC,
	[LookupCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ_NASShare_RegionId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_NASShare_RegionId] ON [automation].[NASShare]
(
	[RegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UX_ProcessGroupMapping_GroupProcessId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_ProcessGroupMapping_GroupProcessId] ON [automation].[ProcessGroupMapping]
(
	[GroupId] ASC,
	[ProcessId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_Region_RegionName]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_Region_RegionName] ON [automation].[Region]
(
	[RegionName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UK_Scheduled_Jobs_JobsName]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UK_Scheduled_Jobs_JobsName] ON [automation].[ScheduledJobs]
(
	[JobName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ScheduledJobsRunHistory_ScheduledJobid]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_ScheduledJobsRunHistory_ScheduledJobid] ON [automation].[ScheduledJobsRunHistory]
(
	[ScheduledJobid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ScheduledTasks_ProcessId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_ScheduledTasks_ProcessId] ON [automation].[ScheduledTasks]
(
	[ProcessId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ScheduledTasks_ScheduledUser]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_ScheduledTasks_ScheduledUser] ON [automation].[ScheduledTasks]
(
	[ScheduledUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ScheduledTasks_StatusId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_ScheduledTasks_StatusId] ON [automation].[ScheduledTasks]
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UX_ServerMapping_AppServerId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_ServerMapping_AppServerId] ON [automation].[ServerMapping]
(
	[ApplicationId] ASC,
	[ServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ServiceRequest_AlertId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_ServiceRequest_AlertId] ON [automation].[ServiceRequest]
(
	[AlertId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ServiceRequest_RequestedUser]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_ServiceRequest_RequestedUser] ON [automation].[ServiceRequest]
(
	[RequestedUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ServiceRequest_StatusId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_ServiceRequest_StatusId] ON [automation].[ServiceRequest]
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ServiceRequest_TypeId]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [IX_ServiceRequest_TypeId] ON [automation].[ServiceRequest]
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_User_UserName]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_User_UserName] ON [automation].[User]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [ix_vw_user_group_app_process_mapping_servername_userid]    Script Date: 6/12/2020 1:10:11 PM ******/
CREATE NONCLUSTERED INDEX [ix_vw_user_group_app_process_mapping_servername_userid] ON [automation].[vw_user_group_app_process_mapping]
(
	[ServerName] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [automation].[Alert] ADD  DEFAULT (getutcdate()) FOR [CreationDate]
GO
ALTER TABLE [automation].[AlertSuppression] ADD  DEFAULT (getutcdate()) FOR [CreationDate]
GO
ALTER TABLE [automation].[emailTemplate] ADD  DEFAULT (getutcdate()) FOR [CreationDate]
GO
ALTER TABLE [automation].[ScheduledJobs] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [automation].[ServiceRequest] ADD  DEFAULT (getutcdate()) FOR [CreationDate]
GO
ALTER TABLE [automation].[ApplicationMapping]  WITH CHECK ADD  CONSTRAINT [FK_APPLICATION_NAME_APPLICATIONMAPPING] FOREIGN KEY([ApplicationId])
REFERENCES [automation].[Application] ([Id])
GO
ALTER TABLE [automation].[ApplicationMapping] CHECK CONSTRAINT [FK_APPLICATION_NAME_APPLICATIONMAPPING]
GO
ALTER TABLE [automation].[ApplicationMapping]  WITH CHECK ADD  CONSTRAINT [FK_GROUP_NAME_APPLICATIONMAPPING] FOREIGN KEY([GroupId])
REFERENCES [automation].[Group] ([Id])
GO
ALTER TABLE [automation].[ApplicationMapping] CHECK CONSTRAINT [FK_GROUP_NAME_APPLICATIONMAPPING]
GO
ALTER TABLE [automation].[Event]  WITH CHECK ADD  CONSTRAINT [FK_PROCESS] FOREIGN KEY([ProcessId])
REFERENCES [automation].[Process] ([Id])
GO
ALTER TABLE [automation].[Event] CHECK CONSTRAINT [FK_PROCESS]
GO
ALTER TABLE [automation].[Event]  WITH CHECK ADD  CONSTRAINT [FK_SERVER_NAME_EVENT] FOREIGN KEY([ServerId])
REFERENCES [automation].[Server] ([Id])
GO
ALTER TABLE [automation].[Event] CHECK CONSTRAINT [FK_SERVER_NAME_EVENT]
GO
ALTER TABLE [automation].[Event]  WITH CHECK ADD  CONSTRAINT [FK_USER_NAME_AUTH_EVENT] FOREIGN KEY([UserId])
REFERENCES [automation].[User] ([Id])
GO
ALTER TABLE [automation].[Event] CHECK CONSTRAINT [FK_USER_NAME_AUTH_EVENT]
GO
ALTER TABLE [automation].[GroupMapping]  WITH CHECK ADD  CONSTRAINT [FK_GROUP_NAME_GROUPMAPPING] FOREIGN KEY([GroupId])
REFERENCES [automation].[Group] ([Id])
GO
ALTER TABLE [automation].[GroupMapping] CHECK CONSTRAINT [FK_GROUP_NAME_GROUPMAPPING]
GO
ALTER TABLE [automation].[GroupMapping]  WITH CHECK ADD  CONSTRAINT [FK_USER_NAME_GROUPMAPPING] FOREIGN KEY([UserId])
REFERENCES [automation].[User] ([Id])
GO
ALTER TABLE [automation].[GroupMapping] CHECK CONSTRAINT [FK_USER_NAME_GROUPMAPPING]
GO
ALTER TABLE [automation].[Instance]  WITH CHECK ADD  CONSTRAINT [FK_SERVER_NAME_INSTANCE] FOREIGN KEY([ServerId])
REFERENCES [automation].[Server] ([Id])
GO
ALTER TABLE [automation].[Instance] CHECK CONSTRAINT [FK_SERVER_NAME_INSTANCE]
GO
ALTER TABLE [automation].[Process]  WITH CHECK ADD  CONSTRAINT [FK_Process_Entity] FOREIGN KEY([EntityId])
REFERENCES [automation].[Entity] ([Id])
GO
ALTER TABLE [automation].[Process] CHECK CONSTRAINT [FK_Process_Entity]
GO
ALTER TABLE [automation].[ProcessGroupMapping]  WITH CHECK ADD  CONSTRAINT [FK_GROUP_NAME_ProcessGroupMapping] FOREIGN KEY([GroupId])
REFERENCES [automation].[Group] ([Id])
GO
ALTER TABLE [automation].[ProcessGroupMapping] CHECK CONSTRAINT [FK_GROUP_NAME_ProcessGroupMapping]
GO
ALTER TABLE [automation].[ProcessGroupMapping]  WITH CHECK ADD  CONSTRAINT [FK_PROCESSID_ProcessGroupMapping] FOREIGN KEY([ProcessId])
REFERENCES [automation].[Process] ([Id])
GO
ALTER TABLE [automation].[ProcessGroupMapping] CHECK CONSTRAINT [FK_PROCESSID_ProcessGroupMapping]
GO
ALTER TABLE [automation].[ScheduledJobsRunHistory]  WITH CHECK ADD  CONSTRAINT [FK_ScheduledJobsRunHistory_ScheduledJobid] FOREIGN KEY([ScheduledJobid])
REFERENCES [automation].[ScheduledJobs] ([Id])
GO
ALTER TABLE [automation].[ScheduledJobsRunHistory] CHECK CONSTRAINT [FK_ScheduledJobsRunHistory_ScheduledJobid]
GO
ALTER TABLE [automation].[ScheduledTasks]  WITH CHECK ADD  CONSTRAINT [FK_PROCESSID_ScheduledTasks] FOREIGN KEY([ProcessId])
REFERENCES [automation].[Process] ([Id])
GO
ALTER TABLE [automation].[ScheduledTasks] CHECK CONSTRAINT [FK_PROCESSID_ScheduledTasks]
GO
ALTER TABLE [automation].[ServerMapping]  WITH CHECK ADD  CONSTRAINT [FK_APPLICATION_NAME_SERVERMAPPING] FOREIGN KEY([ApplicationId])
REFERENCES [automation].[Application] ([Id])
GO
ALTER TABLE [automation].[ServerMapping] CHECK CONSTRAINT [FK_APPLICATION_NAME_SERVERMAPPING]
GO
ALTER TABLE [automation].[ServerMapping]  WITH CHECK ADD  CONSTRAINT [FK_SERVER_NAME_SERVERMAPPING] FOREIGN KEY([ServerId])
REFERENCES [automation].[Server] ([Id])
GO
ALTER TABLE [automation].[ServerMapping] CHECK CONSTRAINT [FK_SERVER_NAME_SERVERMAPPING]
GO
ALTER TABLE [automation].[ServiceRequest]  WITH CHECK ADD  CONSTRAINT [FK_ServiceRequest_AlertId] FOREIGN KEY([AlertId])
REFERENCES [automation].[Alert] ([Id])
GO
ALTER TABLE [automation].[ServiceRequest] CHECK CONSTRAINT [FK_ServiceRequest_AlertId]
GO
/****** Object:  StoredProcedure [automation].[Group_Permissions_Update]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [automation].[Group_Permissions_Update]
AS
    SET NOCOUNT ON;

	/* New Groups for READ ONLY */
	INSERT INTO [automation].[Group] (GroupName, Description, EmailGroup, StatusId, CreationDate)
	SELECT CONCAT(A.ApplicationName,'-ReadOnly'),CONCAT(A.ApplicationName,'-ReadOnly'),
	A.ApplicationName,A.StatusId, GETUTCDATE()
	FROM [automation].[Application] A
	WHERE A.StatusId = 'ACTIVE'
	and A.ApplicationName not in
		(select EmailGroup 
		from [automation].[Group] G 
		where G.GroupName = CONCAT(A.ApplicationName,'-ReadOnly' ));

	/* New Group for EXEC */
	INSERT INTO [automation].[Group] (GroupName, Description, EmailGroup, StatusId, CreationDate)
	SELECT CONCAT(A.ApplicationName,'-Exec'),CONCAT(A.ApplicationName,'-Exec'),
	A.ApplicationName, A.StatusId,GETUTCDATE()
	FROM [automation].[Application] A
	WHERE A.StatusId = 'ACTIVE'
	and A.ApplicationName not in
		(select EmailGroup 
		from [automation].[Group] G 
		where G.GroupName = CONCAT(A.ApplicationName,'-Exec' ));

	/* Insert Application Mapping for missing entries
	-- Ignore duplicate inserts if any */
	INSERT INTO [automation].[ApplicationMapping]  (ApplicationId, GroupId,CreationDate)
	SELECT A.id, G.id, GETUTCDATE()
	FROM [automation].[Application] A, [automation].[Group] G
	WHERE A.ApplicationName = G.EmailGroup
	AND A.Id not in (SELECT AM.ApplicationId  FROM [automation].[ApplicationMapping] AM)


	/* Add to the Exec GROUP */
	INSERT INTO [automation].[ProcessGroupMapping] (GroupId,ProcessId,CreationDate)
	SELECT g.Id, p.Id, GETUTCDATE()
	FROM [automation].[Process] p, [automation].[Group] g
	WHERE p.Action IN ('BACKUP_NOW', 'REBALANCE_NOW', 'SCHEDULE_REBALANCE')  
	and g.GroupName LIKE '%Exec'
	and g.Id not in (select PG.GroupId from [automation].[ProcessGroupMapping] PG);


GO
/****** Object:  StoredProcedure [automation].[Import_Application_Data]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [automation].[Import_Application_Data]
AS
    SET NOCOUNT ON;
	-- step 1 Insert new Applications
 DECLARE
 	@ACTIVE_STATUS int

 	SELECT @ACTIVE_STATUS = s.LookupCode from automation.LookupMaster s WHERE s.LookupCode = 'ACTIVE' AND s.LookupType = 'GENERAL_STATUS'

    INSERT INTO automation.application (ApplicationName, Manageable, Description, CreationDate, StatusId  )
        SELECT   DISTINCT AppName, 1, 'Description for '+AppName, getutcdate(), @ACTIVE_STATUS
        FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].[vInstancesList] as SRC
        WHERE SRC.[AppName] not in (
            SELECT [automation].[application].[ApplicationName]
            FROM [automation].[application]
        ) ORDER BY 1 DESC

GO
/****** Object:  StoredProcedure [automation].[Import_Instance_Data]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [automation].[Import_Instance_Data]
AS
    SET NOCOUNT ON;

   DECLARE
 	@ACTIVE_STATUS VARCHAR(64)

 	SELECT @ACTIVE_STATUS = s.LookupCode  from automation.LookupMaster s WHERE s.LookupType = 'GENERAL_STATUS' AND s.LookupCode = 'ACTIVE'
	INSERT INTO [automation].[instance] (InstanceName, Description, ServerId, StatusId, CreationDate)
    SELECT DISTINCT InstanceName, 'Description for' +InstanceName, SRV.Id , @ACTIVE_STATUS , getutcdate()
    FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].[vInstancesList] as SRC,
    	 [automation].[server] SRV
	WHERE SRC.[instanceName] not in (
		SELECT [automation].[instance].[instanceName]
		FROM [automation].[instance]
	)
	AND SRC.ServerName = SRV.ServerName
	AND SRC.Type = 'SQL'
	ORDER BY 1 DESC

GO
/****** Object:  StoredProcedure [automation].[Import_Server_Data]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [automation].[Import_Server_Data]
AS
    SET NOCOUNT ON;

   DECLARE
 	@ACTIVE_STATUS VARCHAR(64)

 	SELECT @ACTIVE_STATUS = s.LookupCode from automation.LookupMaster s WHERE s.LookupType = 'GENERAL_STATUS' AND s.LookupCode = 'ACTIVE'

-- step Insert new servers


	INSERT INTO [automation].[server] (ServerName, DomainName , RegionName , Description, StatusId)
    SELECT DISTINCT SRC.ServerName, SRC.[Domain], SRC.[Location] , SRC.Description, @ACTIVE_STATUS
    FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].[vInstancesList] as SRC
	WHERE SRC.[ServerName] not in (
		SELECT [automation].[server].[servername]
		FROM [automation].[server] )
	AND SRC.[Domain] IS NOT NULL
	AND SRC.[Location] IS NOT NULL
	ORDER BY 1 DESC
GO
/****** Object:  StoredProcedure [automation].[Mapping_Application_server]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [automation].[Mapping_Application_server]
AS
    SET NOCOUNT ON;

   DECLARE
 	@ACTIVE_STATUS int

 	SELECT @ACTIVE_STATUS = s.id from automation.LookupMaster s WHERE s.LookupCode = 'ACTIVE' AND s.LookupType = 'GENERAL_STATUS'

    -- step 1 Applications and Server Mapping
    INSERT INTO [automation].[ServerMapping] (ApplicationId, ServerId,  Description,CreationDate)
    SELECT distinct [automation].[application].[id] as ApplicationId, [automation].[server].[id] as ServerId, INV.AppName + ' have ' + INV.ServerName, getutcdate()
    FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].[vInstancesList]  as INV
    INNER JOIN [automation].[application] ON  [automation].[application].[ApplicationName] = INV.AppName
    INNER JOIN [automation].[server] ON  [automation].[server].[ServerName] = INV.ServerName
GO
/****** Object:  StoredProcedure [automation].[sp_DismissAlert]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Bharat Chavada>
-- Create date: 6/4/2020
-- =============================================
CREATE PROCEDURE [automation].[sp_DismissAlert]

@AlertId int
AS
BEGIN
declare @ServerName nvarchar(50),@InstanceName nvarchar(50),@DatabaseName nvarchar(50) ,@ExistingCount as int
  update [automation].Alert
  set StatusId = 'DISMISSED'
  where Id = @AlertId

  SELECT  @ServerName=a.ServerName,@InstanceName = a.InstanceName,@DatabaseName=a.DBName
        FROM [automation].Alert as a
        where Id = @AlertId

  --insrt into AlertSuppression for same record
  set  @ExistingCount = (Select count(*) from [automation].[AlertSuppression]
  where ServerName =@ServerName
and InstanceName = @InstanceName
and   DBName =@DatabaseName)

if (@ExistingCount =0)
begin
   INSERT INTO [automation].[AlertSuppression]
	             ([AlertType]
           ,[ServerName]
           ,[InstanceName]
           ,[DBName]
           ,[ConfigKeys]
           ,[CreationDate])
        SELECT   a.TypeId,a.ServerName,a.InstanceName,a.DBName,null, getutcdate()
        FROM [automation].Alert as a
        where Id = @AlertId
		return @AlertId
		end
return @AlertId		
end
GO
/****** Object:  StoredProcedure [automation].[sp_GetDashboardCounts]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Bharat Chavada>
-- Create date: 6/4/2020
-- Description:	<Get Dashboard Counts>
-- =============================================
CREATE PROCEDURE [automation].[sp_GetDashboardCounts]
@IsAdmin bit,
@UserId int,
@UserName nvarchar(50)
AS
BEGIN
declare @OpenAlertsCount as int,@OpenRequestsCount as int ,@FailedJobsCount as int,@FailedTasksCount as int 
if(@IsAdmin =1)
begin
set  @OpenAlertsCount = (select count(*) from [automation].[Alert]  where StatusId='CREATED')
set  @OpenRequestsCount =(select count(*)  from [automation].[ServiceRequest]  where StatusId='CREATED')
set  @FailedJobsCount =(select Count(*)  from [automation].[ScheduledJobsRunHistory] where RunStatus = 'FAILED' and RunEndtime >= DATEADD(day, -1, GETUTCDATE()))
set  @FailedTasksCount =(select Count(*) from [automation].[ScheduledTasks] where StatusId = 'FAILED' and [Last_Modified_Date] >= DATEADD(day, -1, GETUTCDATE()))

select @OpenAlertsCount as OpenAlertsCount,@OpenRequestsCount as OpenRequestsCount,@FailedJobsCount as FailedJobsCount , @FailedTasksCount as FailedTasksCount
END
else
begin
set  @OpenAlertsCount = (SELECT count(*)
  FROM [EYDBSRE_UAT].[automation].[Alert] where ServerNoDomain in (SELECT ServerName
  FROM [EYDBSRE_UAT].[automation].vw_user_group_app_process_mapping where UserId = @UserId)
  and StatusId='CREATED' )
set  @OpenRequestsCount =(SELECT count(*)
  FROM [EYDBSRE_UAT].[automation].[ServiceRequest] where ServerNoDomain in (SELECT ServerName
  FROM [EYDBSRE_UAT].[automation].vw_user_group_app_process_mapping where UserId = @UserId)
  and StatusId='CREATED')
set  @FailedJobsCount =0
set  @FailedTasksCount =(select Count(*) from [automation].[ScheduledTasks] where LOWER(ScheduledUser) = LOWER(@UserName) and StatusId = 'FAILED' and [Last_Modified_Date] >= DATEADD(day, -1, GETUTCDATE()))
select @OpenAlertsCount as OpenAlertsCount,@OpenRequestsCount as OpenRequestsCount,@FailedJobsCount as FailedJobsCount , @FailedTasksCount as FailedTasksCount
end
end
GO
/****** Object:  StoredProcedure [automation].[sp_IsScheduledTaskExist]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [automation].[sp_IsScheduledTaskExist]
      @ServerName nvarchar(50),
	  @ProcessId int,
	  @InstanceName nvarchar(50),
	  @DatabaseName nvarchar(50) =null
	  --@ScheduledTime DATETIME OUTPUT
AS
BEGIN
      If (@ProcessId = 3 OR @ProcessId = 6)
	  begin
	   SELECT TOP 1 * FROM [automation].[ScheduledTasks] as st WHERE st.ServerNoDomain = @ServerName AND st.ProcessId = @ProcessId and StatusId = 'SCHEDULED' and InstanceName = @InstanceName and DBName= @DatabaseName order by Id desc
	  end
	  else
	  begin
	  SELECT TOP 1 * FROM [automation].[ScheduledTasks] WHERE ServerNoDomain = @ServerName AND ProcessId = @ProcessId and StatusId = 'SCHEDULED' and InstanceName = @InstanceName order by Id desc
	  end
END

GO
/****** Object:  DdlTrigger [ddl_database_audit]    Script Date: 6/12/2020 1:10:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
CREATE TRIGGER  [ddl_database_audit] 
ON  DATABASE
FOR DDL_DATABASE_LEVEL_EVENTS 
AS
BEGIN

SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON

-- Check that Statistics_DB database is available
IF NOT EXISTS(
	SELECT TOP 1 1 FROM master.sys.databases WHERE [name] = 'Statistics_DB' and state = 0 and is_read_only = 0 and user_access = 0
)
	RETURN

-- Check for the existence of the Audit table
DECLARE @check int;
EXEC sp_executesql 
	N'SET @check = (SELECT TOP 1 1 FROM [Statistics_DB].sys.tables WHERE [name] = ''Audit'')',
	N'@check int OUTPUT',
	@check OUTPUT

IF @check IS NULL
	RETURN

-- Attempt to log the event
BEGIN TRY
	declare @database nvarchar(100)
	declare @tsql nvarchar(max)
	declare @login nvarchar(100)
	declare @user nvarchar(100)
	declare @time datetime
	declare @object nvarchar(200)
	declare @eventType nvarchar(100)
	declare @trigger nvarchar(100)
	declare @xmlEventData XML 

	SET @trigger = 'ddl_database_audit'
	SET @xmlEventData = EVENTDATA()
	SET @eventType = CONVERT(NVARCHAR(100), @xmlEventData.query('data(/EVENT_INSTANCE/EventType)'))
	SET @database = CONVERT(NVARCHAR(100), @xmlEventData.query('data(/EVENT_INSTANCE/DatabaseName)'))
	SET @tsql = CONVERT(NVARCHAR(MAX), @xmlEventData.query('data(/EVENT_INSTANCE/TSQLCommand/CommandText)'))
	SET @login = CONVERT(NVARCHAR(100), @xmlEventData.query('data(/EVENT_INSTANCE/LoginName)'))
	SET @user = CONVERT(NVARCHAR(100), @xmlEventData.query('data(/EVENT_INSTANCE/UserName)'))
	SET @time = REPLACE(CONVERT(NVARCHAR(50), @xmlEventData.query('data(/EVENT_INSTANCE/PostTime)')),'T', ' ')
	SET @object = CONVERT(NVARCHAR(200), @xmlEventData.query('data(/EVENT_INSTANCE/ObjectName)'))

	EXEC sp_executesql N'
		INSERT [Statistics_DB].dbo.Audit(
			[Database], [TSql], [Login], [User], [Time], [Object], [EventData], [EventType], [Trigger]
		) VALUES (
			@database, @tsql, @login, @user, @time, @object, @eventdata, @eventType, @trigger
		) 
	',
	N'
	@database nvarchar(100), 
	@tsql nvarchar(max), 
	@login nvarchar(100), 
	@user nvarchar(100), 
	@time datetime, 
	@object nvarchar(200), 
	@eventData XML, 
	@eventType nvarchar(100), 
	@trigger nvarchar(100)
	', 
	@database,
	@tsql,
	@login,
	@user,
	@time,
	@object,
	@xmlEventData,
	@eventType,
	@trigger

END TRY
BEGIN CATCH
	declare @error_message varchar(max);
	declare @error_severity int;
	declare @error_state int;

	set @error_message = error_message()
	set @error_severity = error_severity()
	set @error_state = error_state()

	set @error_message = 'ddl_database_audit: ' +  error_message() + '. Severity: ' + 
		cast(@error_severity as varchar(7)) + '. State: ' + cast(@error_state as varchar(7))
	RAISERROR ('%s',16,1,@error_message) with log;
END CATCH

END
GO
ENABLE TRIGGER [ddl_database_audit] ON DATABASE
GO
USE [master]
GO
ALTER DATABASE [EYDBSRE_UAT] SET  READ_WRITE 
GO
