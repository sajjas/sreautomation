param(
	[String]$JOB_INPUT,
    [String]$SERVERNAME,
	[String]$INSTANCENAME
)

$scriptDir = Get-Location

# -----------------------------------------------------------------------------
#          Global Lists Definition
# -----------------------------------------------------------------------------
$SMTP = "smtp.ey.net"
$IIS_HOST = "USSECVMPMGMSQ01.EY.NET:8000"

$json_data = $JOB_INPUT | ConvertFrom-Json 
$SCHEDULED_USER = $json_data.email
$SCHEDULED_JOB_ID = $json_data.scheduled_job_id
$OVERDUE_DAYS = $json_data.overdue_backup_days
$BACKUP_TYPE = $json_data.backup_type

$EMAIL_DATA_COLLECTION = New-Object System.Collections.ArrayList
$TABLE_DATA_COLLECTION = New-Object System.Collections.ArrayList

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"


# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# -----------------------------------------------------------------------------
# @function get_email_template:  a) pull data from givven email template name
#                                b) print to log the pulled db info
# @return:   - Dictionary which encapsules the EmailTemaplate data
#            - null in case of any error.
#
# @param TemplateName:   Name of Email Template same as stored in DB
#                        table "EYDBSRE.automation.emailTemplate"
function get_email_template([String]$TemplateName)
{
	# -----------------------------------------------------------------------------
	$QRY_GET_EMAIL_TEMPLATE = "SELECT subject,TopBody,BottomBody,[From],[To],[CC],[BCC],'Body' as Body FROM automation.emailTemplate WHERE TemplateName = @TemplateName"
	# -----------------------------------------------------------------------------

    log("  *********************************get_email_template starts ********************************************")
    $emailTemplateInstance = $null
    try {
        if ($null -ne $TemplateName) {
            $emailTemplateInstance = executeSelectQuery -select_qry $QRY_GET_EMAIL_TEMPLATE -parameters @{ TemplateName = $TemplateName }
            if ($null -ne $emailTemplateInstance -and $emailTemplateInstance) {
                log("Return Email Template Instance")
                foreach($itemEmail in $emailTemplateInstance.keys)
                {
                    $value =  $emailTemplateInstance.item($itemEmail)
                    log(" $itemEmail --> $value ")
                }
            }
        }
    }
    catch{
        log("------------ get_email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************get_email_template ends  ********************************************")
    return $emailTemplateInstance
}

# -----------------------------------------------------------------------------
# @function send_Email_template: a) Validate for required Fields
#                                b) Contenate Top, Middle and Bottom Body
#                                c) Create Dictionay with message Parameters
#                                d) Send Email
# @return:   False - if any error during email creation or email is not sent.
#            True  - Email sent
#
# @param emailTemplateInstance:  Dictionary which encapsules the EmailTemaplate
#                                Data, Body is 'Body' String as default.
# @TODO: It could be greate to have SMTP value as DB Lookup configuration.
function send_Email_template($emailTemplateInstance)
{
    log("  *********************************send_Email_template starts ********************************************")
    $returnStatus = $false
    try
    {
        if ($null -ne $emailTemplateInstance) {
            # -- Validate for required fields are in dicc starts
            $daRequiredFields = @('subject','TopBody','Body','BottomBody','From','To')
            $emailContinue = $true
            foreach( $itemRequired in $daRequiredFields)
            {
                $fieldKey = $itemRequired
                $fieldValue = $emailTemplateInstance.item("$fieldKey")
                if ($null -eq $fieldValue -and $fieldValue.Length -eq 0){
                    log("$fieldKey is missing")
                    $emailContinue = $false
                    break
                }
            }
            # -- Validate for required fields are in dicc ends
            #------------------------------------------------------------------
            # Continue only if required values are set
            if ($emailContinue)
            {
                # I know this is redundant but is the way how PS works   --- starts
                $subject = $emailTemplateInstance.subject
                $TopBody = $emailTemplateInstance.TopBody
                $Body = $emailTemplateInstance.Body
                $BottomBody = $emailTemplateInstance.BottomBody
                $From = $emailTemplateInstance.From
                $To = $emailTemplateInstance.To
                # I know this is redundant but is the way how PS works   --- ends
                #------------------------------------------------------------------
                # Create Body starts
                log(" Creating Body start ---------")
                $Full_Body = New-Object Collections.Generic.List[string]
                $Full_Body.add($TopBody)
                $Full_Body.add($Body)
                $Full_Body.add($BottomBody)
                log(" Email Body --> $Full_Body")
                log(" Creating Body ends ---------")
                # Create Body Ends
                #------------------------------------------------------------------
                # Assign Values to message dic starts
                $messageParameters = @{
                Subject = "$subject"
                Body = "$Full_Body"
                From = "$From"
                To = $To.split(',')
                SmtpServer = "$SMTP"   # This value is taken from global configuration variables
                }
                # Add CC to dictionary if not null
                if ($null -ne $emailTemplateInstance.CC -and $emailTemplateInstance.CC){
                    $CC = $emailTemplateInstance.CC
                    $messageParameters.Cc = $CC.split(',')
                }
                # Add BCC to dictionary if not null
                if ($null -ne $emailTemplateInstance.BCC -and $emailTemplateInstance.BCC){
                    $BCC = $emailTemplateInstance.BCC
                    $messageParameters.Bcc = $BCC.split(',')
                }
                # Assign Values to message dic ends
                #------------------------------------------------------------------
                Send-MailMessage @messageParameters -BodyAsHtml
                $returnStatus = $True
                log( "----> Email sent From: $From To: $To  using smtp server: $SMTP")
            }
        }
    }
    catch{
        log("------------ send_Email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************send_Email_template ends  ********************************************")
    return $returnStatus
}

# -----------------------------------------------------------------------------
function executeSelectQuery
{
    param ([String]$select_qry, $parameters = @{ })
    log( "<----------- Pulling Data from DB Starts -------->")
    if ($null -ne $select_qry -and $select_qry)
    {
        $query_final = $select_qry
        $dataset_cursor = New-Object System.Data.DataTable
        $sqlSelectConnection = getConnection
        $return_dic = $null
        if ($null -ne $sqlSelectConnection -and $sqlSelectConnection)
        {
            try
            {
                $sqlSelectConnection.Open()
                #SQL QUERY Execution
                $sqlCommand = New-Object System.Data.SqlClient.SqlCommand;
                $sqlCommand.Connection = $sqlSelectConnection;
                $sqlCommand.CommandText = $query_final;
                #SQL Parameters
                if ($null -ne $parameters -and $parameters)
                {
                    foreach ($p in $parameters.Keys)
                    {
                        [Void] $sqlCommand.Parameters.AddWithValue("@$p", $parameters[$p])
                        $param_text = $parameters[$p]
                        log("parameter $p  --> $param_text ")
                    }
                }
                #SQL Return Values
                $READER_DATA = $sqlCommand.ExecuteReader()
                $dataset_cursor.Load($READER_DATA)
                $sqlSelectConnection.Close()
                if ($null -ne $dataset_cursor)
                {
                    $return_dic = @{ }
                    foreach ($tupla in $dataset_cursor)
                    {
                        foreach ($columna in $dataset_cursor.Columns)
                        {
                            $value_x = $tupla.item($columna)
                            $columna_x = $columna
                            $return_dic.Add("$columna_x", "$value_x")
                            log("[$columna_x]=$value_x")
                        }
                    }
                }
            }
            catch
            {
                $return_dic = $null
                log($ERROR[0])
                log("Not Able to execute Select Query - [ $select_qry ]")
            }
        }

    }
    log( "<----------- Pulling Data from DB Ends -------->")
    return $return_dic
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function getConnection
{
    try
    {
        $connectionString = "server=$VAR_SERVERINSTANCE;database='$VAR_DATABASE';user id=$VAR_USERNAME;password=$VAR_PASSWORD";
        #SQL Connection - connection to SQL server
        $sqlConnection = new-object System.Data.SqlClient.SqlConnection;
        $sqlConnection.ConnectionString = $connectionString;
    }
    catch
    {
        log($ERROR[0])
        $sqlConnection = $null
    }
    return $sqlConnection
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function sendEmailAlert
{
    param(
		$data, 
		[string]$email_template_type
	)

	# -------------------------------------------------------------------------------------
	# Format data into HTML Table
	$TABLE_DATA_COLLECTION = tableData($data)
	
	# -------------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User before DB Backup Start.
	$emailTemplateInstance = get_email_template($email_template_type)
	$emailTemplateInstance.To = $SCHEDULED_USER
	$emailTemplateInstance.Body = $TABLE_DATA_COLLECTION
	send_Email_template($emailTemplateInstance)
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function tableData($data) {
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
	$new_data = $data | ConvertFrom-Json
    
    if ($new_data.PSobject.Properties.Name -contains "ScheduledJobId") {
		$htmlTableOutput = $new_data | select-object -property @{N='Scheduled Job Id';E={$ScheduledJobId}}, @{N='Scheduled Job Run History Id';E={$ScheduledJobRunHistoryId}}, @{N='Error Message';E={$_.ErrorMessage}} | ConvertTo-Html -Head $style
	} else {
	    $htmlTableOutput = $new_data | select-object -property @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, @{N='DBName';E={$_.DBName}}, @{N='Backup Type';E={$_.BackupType}}, @{Label="Action";Expression={"<a href='$($_.Action)'>Manage Alert</a>"}} | ConvertTo-Html -Head $style
    }
    
    Add-Type -AssemblyName System.Web
    $tableOutput = [System.Web.HttpUtility]::HtmlDecode($htmlTableOutput)
	
	return $tableOutput
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function getOverdueBackupDBS {
	# ----------- Overdue backup database return list, insert into Alert table and insert into ScheduledJobRunHistory table. -----------
	if ($BACKUP_TYPE -eq "FULL"){
		$BACKUP_TYPE_ID = "D"
	} elseif ($BACKUP_TYPE -eq "DIFF"){
		$BACKUP_TYPE_ID = "I"
	} elseif ($BACKUP_TYPE -eq "T-LOG"){
		$BACKUP_TYPE_ID = "L"
	}

	# -----------------------------------------------------------------------------
	log("SERVERNAME:	$SERVERNAME ")
	log("INSTANCENAME:	$INSTANCENAME ")
	log("SCHEDULED_JOB_ID:	$SCHEDULED_JOB_ID ")
	log("OVERDUE_BACKUP_DAYS:	$OVERDUE_DAYS ")
	log("BACKUP_TYPE:	$BACKUP_TYPE ")
    log("SCHEDULED_USER: $SCHEDULED_USER ")
	
	# -----------------------------------------------------------------------------
	# Fetching List of Overdue Backup Databases List
	$overdue_db_backup_queryString = "
		SELECT d.name AS database_name,
		CONVERT(VARCHAR(16), MAX(b.backup_finish_date), 120) AS LastBackup
		FROM sys.databases d
		LEFT OUTER JOIN msdb.dbo.backupset b ON d.name = b.database_name
		WHERE d.name NOT IN ('tempdb', 'model', 'msdb','master','Statistics_DB') and b.[type] = '$BACKUP_TYPE_ID'
		GROUP BY d.database_id, d.name
		HAVING MAX(b.backup_finish_date) < GETDATE() - $OVERDUE_DAYS
		ORDER BY CASE WHEN d.database_id <= 4 THEN 0 ELSE 1 END, d.name
		"
	try {
		$databases = (invoke-sqlcmd -serverInstance $INSTANCENAME -query $overdue_db_backup_queryString).database_name
		$get_overdue_backup_dbs_query_output_data = @{output = $true; Success_Message = $databases} | ConvertTo-Json
	} catch {
		$get_overdue_backup_dbs_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
    
    return $get_overdue_backup_dbs_query_output_data
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function scheduleJobHistoryRecord($PARAM=@{})
{
    $INSERT_ID = $PARAM.InsertId
    $RUN_START_TIME = $PARAM.RunStartTime
    $RUN_END_TIME = $PARAM.RunEndTime
    $RUN_STATUS = $PARAM.RunStatus
    $RUN_OUTPUT = $PARAM.RunOutput
     
    # -----------------------------------------------------------------------------
	if($param.query_type -eq "INSERT") {
		$queryString = "
		INSERT INTO [automation].[ScheduledJobsRunHistory]
			   ([ScheduledJobid]
			   ,[ServerName]
			   ,[InstanceName]
			   ,[DBName]
			   ,[RunStarttime]
			   ,[RunEndtime]
			   ,[RunStatus]
			   ,[RunOutput])
			VALUES
			   ('$SCHEDULED_JOB_ID'
			   ,'$SERVERNAME'
			   ,'$INSTANCENAME'
			   ,'NULL'
			   ,'$RUN_START_TIME'
			   ,'$RUN_START_TIME'
			   ,'$RUN_STATUS'
			   ,'$RUN_OUTPUT');
			   SELECT SCOPE_IDENTITY();
		"
	} elseif($param.query_type -eq "UPDATE") {
		$queryString=" 
		UPDATE [automation].[ScheduledJobsRunHistory] SET
			RunEndtime='$RUN_END_TIME', 
			RunStatus='$RUN_STATUS',
            RunOutput='$RUN_OUTPUT'
		WHERE Id='$INSERT_ID'; 
		SELECT SCOPE_IDENTITY();
		"
	}
	
	try {
        $Schedulejob_Run_History_Query_Output = invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
        $Schedulejob_Run_History_Query_Output_Json = @{output = $true; Success_Message = $Schedulejob_Run_History_Query_Output.Column1} | ConvertTo-Json
	} catch {
		$Schedulejob_Run_History_Query_Output_Json = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	
	return $Schedulejob_Run_History_Query_Output_Json
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# @function AlertInsertOrSuppressCheck:
#   In the following Cases, the alerts creation to be Suppressed. 
#   a) Cluster Unbalance Alert: When for the Same Server/another Server in the Cluster an already exists in Created/New State.
#   b) DB Backup Overdue Alert: When for the Same Server, InstanceName and DBName if alert already exists in Created/New State.
#   c) Non Standard Instance: When for the same server and Instance if alert already exists in Created/New State.
#   d) non Standard Database: When for the same server, Instance and DBName if alert is already exists in Created/New State.
# @return:   Existing Alert Id or New Alert Id and Suppression Flag in Json Format
#            
# @param emailTemplateInstance:  Dictionary which encapsules the AlertInfo
#
# The Check is based on AlertType + Status + serverNamenodomain + InstanceName + DBName and based on applicable criteria.
#
# -----------------------------------------------------------------------------
Function AlertInsertOrSuppressCheck ($PARAM=@{}) 
{
    #$ALERT_STATUS = $PARAM.Status
    $ALERT_TYPE = $PARAM.Type
    #$ALERT_INFO = $PARAM.Info
    $SERVERNAME = $PARAM.ServerName
    $INSTANCENAME = $PARAM.InstanceName
    $DBNAME = $PARAM.Database
    #$CLUSTER_INSTANCES = $PARAM.ClusterInstances
    
    # -----------------------------------------------------------------------------
    # First Check in AlertSuppression Table
    if ($ALERT_TYPE -eq "CLUSTER_UNBALANCE") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_INFO = "SELECT Id FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "OVERDUE_BACKUP") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_INFO = "SELECT Id FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and DBName='$DBNAME'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_INSTANCE") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_INFO = "SELECT Id FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_DATABASE") {
         # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_INFO = "SELECT Id FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and DBName='$DBNAME'"
        # -----------------------------------------------------------------------------
    }

    log($QRY_GET_ALERT_SUPPRESSION_INFO)

    $ALERT_SUPPRESSION_ID = executeSelectQuery -select_qry $QRY_GET_ALERT_SUPPRESSION_INFO -parameters @{}
    
    if ($ALERT_SUPPRESSION_ID.Count -eq 0) {
        if ($ALERT_TYPE -eq "CLUSTER_UNBALANCE") {
            $ALERT_ID_JSON_DATA = checkAlertTable $ALERT_TYPE $PARAM
        } elseif ($ALERT_TYPE -eq "OVERDUE_BACKUP") {
            $ALERT_ID_JSON_DATA = checkAlertTable $ALERT_TYPE $PARAM
        } elseif ($ALERT_TYPE -eq "NON_STANDARD_INSTANCE") {
            $ALERT_ID_JSON_DATA = checkAlertTable $ALERT_TYPE $PARAM
        } elseif ($ALERT_TYPE -eq "NON_STANDARD_DATABASE") {
            $ALERT_ID_JSON_DATA = checkAlertTable $ALERT_TYPE $PARAM
        }
    } else {
        $ALERT_ID_JSON_DATA = suppressedAlertInfo $ALERT_TYPE $PARAM
    }

    return $ALERT_ID_JSON_DATA
}

Function suppressedAlertInfo
{
    param (
        $ALERT_TYPE,
        $PARAM
    )

    if ($ALERT_TYPE -eq "CLUSTER_UNBALANCE") {
        
        $MSG = "This Alert is Suppressed. Hence Skipping Raising an Alert!"

        log($MSG)
        
        $ALERT_ID_JSON = @{output = $true; Success_Message = $MSG; Alert_Suppression = $true} | ConvertTo-Json
    
    } elseif ($ALERT_TYPE -eq "OVERDUE_BACKUP") {
        
        $MSG = "This Alert is Suppressed. Hence Skipping Raising an Alert!"

        log($MSG)
        
        $ALERT_ID_JSON = @{output = $true; Success_Message = $MSG; Alert_Suppression = $true} | ConvertTo-Json
    
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_DATABASE") {
        
        $MSG = "This Alert is Suppressed. Hence Skipping Raising an Alert!"

        log($MSG)
        
        $ALERT_ID_JSON = @{output = $true; Success_Message = $MSG; Alert_Suppression = $true} | ConvertTo-Json
    
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_INSTANCE") {

        $ALERT_INFO_JSON = New-Object System.Collections.ArrayList

        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_CONFIG_KEYS = "SELECT ConfigKeys FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME'"
        # -----------------------------------------------------------------------------
        
        log($QRY_GET_ALERT_SUPPRESSION_CONFIG_KEYS)

        $CONFIG_KEYS = executeSelectQuery -select_qry $QRY_GET_ALERT_SUPPRESSION_CONFIG_KEYS -parameters @{}
        $CONFIG_KEYS =  $CONFIG_KEYS.ConfigKeys.split(',')
        
        $ALERT_INFO_FROM_TABLE = $PARAM.Info | ConvertFrom-Json

        foreach ($i in $ALERT_INFO_FROM_TABLE.NonStandardInstanceConfig) { 
            if ($CONFIG_KEYS -NotContains $i.ConfigId) { 
                $ALERT_INFO_JSON.add(
                    @{configuration_id=$i.configuration_id; 
                        name=$i.name; 
                        value=$i.value;
                        maximum=$i.maximum; 
                        value_in_use=$i.value_in_use; 
                        description=$i.description; 
                        ConfigId=$i.ConfigId; 
                        ValueType=$i.ValueType; 
                        StdValue=$i.StdValue
                    }
                ) 
            } 
        }

        $NON_STD_INSTANCE_CONFIG_DATA = $ALERT_INFO_JSON | ConvertTo-Json

        if($ALERT_INFO_JSON.Count -le 1) {
            $INFO = @"
{
    "NonStandardInstanceConfig": [$NON_STD_INSTANCE_CONFIG_DATA]
}
"@
        } else {
            $INFO = @"
{
    "NonStandardInstanceConfig": $NON_STD_INSTANCE_CONFIG_DATA
}
"@
        }

        $PARAM.Info = $INFO

        $ALERT_ID_JSON = checkAlertTable $ALERT_TYPE $PARAM

    }

    return $ALERT_ID_JSON
}

Function checkAlertTable 
{
    param (
        $ALERT_TYPE, 
        $PARAM=@{}
    )

    $ALERT_STATUS = $PARAM.Status
    #$ALERT_INFO = $PARAM.Info
    $SERVERNAME = $PARAM.ServerName
    $INSTANCENAME = $PARAM.InstanceName
    $DBNAME = $PARAM.Database
    #$CLUSTER_INSTANCES = $PARAM.ClusterInstances

    # -----------------------------------------------------------------------------
    # Second Check in Alert Table
    if ($ALERT_TYPE -eq "CLUSTER_UNBALANCE") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_ID = "SELECT Id FROM automation.Alert WHERE TypeId='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and StatusId='$ALERT_STATUS'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "OVERDUE_BACKUP") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_ID = "SELECT Id FROM automation.Alert WHERE TypeId='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and DBName='$DBNAME' and StatusId='$ALERT_STATUS'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_INSTANCE") {
         # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_ID = "SELECT Id FROM automation.Alert WHERE TypeId='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and StatusId='$ALERT_STATUS'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_DATABASE") {
         # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_ID = "SELECT Id FROM automation.Alert WHERE TypeId='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and DBName='$DBNAME' and StatusId='$ALERT_STATUS'"
        # -----------------------------------------------------------------------------
    }

    log($QRY_GET_ALERT_ID)
    
    $ALERT_ID = executeSelectQuery -select_qry $QRY_GET_ALERT_ID -parameters @{}
    
    if ($ALERT_ID.Count -eq 0) {
        $ALERT_ID_JSON = AlertRecordInsert($PARAM)
    } else {    
        log("The AlertId is Already Present in the Database!")
        $ALERT_ID_JSON = @{output = $true; Success_Message = $ALERT_ID; Alert_Suppression = $true} | ConvertTo-Json
    }

    return $ALERT_ID_JSON
}

Function AlertRecordInsert($PARAM=@{})
{
    $ALERT_STATUS = $PARAM.Status
    $ALERT_TYPE = $PARAM.Type
    $INFO = $PARAM.Info
    $SERVERNAME = $PARAM.ServerName
    $INSTANCENAME = $PARAM.InstanceName
    $DBNAME = $PARAM.Database

	$queryString = "
	INSERT INTO [automation].[Alert]
		   ([TypeId]
		   ,[CreationDate]
		   ,[StatusId]
		   ,[Info]
		   ,[Last_Modified_Date]
		   ,[ServerName]
		   ,[InstanceName]
		   ,[DBName])
		VALUES
            ('$ALERT_TYPE'
            ,getutcdate()
            ,'$ALERT_STATUS'
            ,'$INFO'
            ,getutcdate()
            ,'$SERVERNAME'
            ,'$INSTANCENAME'
            ,'$DBNAME');
		SELECT SCOPE_IDENTITY();
	"
	try {
		$ALERT_ID = (invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD").Column1
		$ALERT_ID_JSON_DATA = @{output = $true; Success_Message = $ALERT_ID; Alert_Suppression = $false} | ConvertTo-Json
	} catch {
		$ALERT_ID_JSON_DATA = @{output = $false; Fail_Message = $_.Exception.Message; Alert_Suppression = $false} | ConvertTo-Json
	}
	
	return $ALERT_ID_JSON_DATA
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function log ($Message) 
{
    Push-Location -Path $scriptDir
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/overdue_backup_databases.log
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function mainFunction
{
	# ----------- Starting with Creating an Entry in ScheduledJobRunHistory Table -----------
	log("=========== Starting with Creating an Entry in ScheduledJobRunHistory Table ===========")
	
	$RUN_START_TIME = Get-Date -format "yyyy-MM-dd HH:mm:ss"

	$RUN_STATUS = "STARTED"
	
	$Schedule_Job_Run_History_Data = scheduleJobHistoryRecord(@{query_type="INSERT"; RunStartTime=$RUN_START_TIME; RunEndTime=$RUN_START_TIME; RunStatus=$RUN_STATUS; RunOutput="NULL"})

	$Schedule_Job_Run_History_Data_Parsed = $Schedule_Job_Run_History_Data | ConvertFrom-Json

	if($schedule_Job_Run_History_Data_Parsed.output -eq $false) {
		log($Schedule_Job_Run_History_Data_Parsed.Fail_Message)

	} elseif($Schedule_Job_Run_History_Data_Parsed.output -eq $true -And [String]::IsNullOrEmpty($Schedule_Job_Run_History_Data_Parsed.Success_Message) -eq $true) {
		log("ERROR: Schedule Job Eun History InsertID missing")
        Exit

	} else {
		$Schedule_Job_Run_History_InsertID = $Schedule_Job_Run_History_Data_Parsed.Success_Message
		log("ScheduledJobRunHistory Insert ID:	$Schedule_Job_Run_History_InsertID")
	}

    try {
        # ----------- Fetching Overdue Bakcup Databases list -----------
        log("=========== Fetching the Overdue Bakcup Databases list ===========")

        $LIST_OF_OVERDUE_DATABASES = getOverdueBackupDBS

        $JSON_PARSED_DATA = $LIST_OF_OVERDUE_DATABASES | ConvertFrom-Json

        if ($JSON_PARSED_DATA.output -eq $true) {

            $DATABASES = $JSON_PARSED_DATA.Success_Message

            $JSON_BACKUP_TYPE = @{BackupType = $BACKUP_TYPE} | ConvertTo-Json
            
            if ([String]::IsNullOrEmpty($DATABASES) -eq $false) {
                
                foreach ($DATABASE in $DATABASES) {
                    # ----------- Inserting Entry with Overdue Backup Database Name in Alert Table -----------
                    log("=========== Inserting Entry with Overdue Backup Database ""$DATABASE"" in Alert Table ===========")
                    $ALERT_ENTRY = AlertInsertOrSuppressCheck(@{Status="CREATED"; Type="OVERDUE_BACKUP"; Info=$JSON_BACKUP_TYPE; ServerName=$SERVERNAME; InstanceName=$INSTANCENAME; Database=$DATABASE; ClusterInstances="NULL";})
                    $ALERT_ENTRY_JSON = $ALERT_ENTRY | ConvertFrom-Json

                    if ($ALERT_ENTRY_JSON.output -eq $false) {
                        $FAIL_MSG = $ALERT_ENTRY_JSON.Fail_Message
                        log("ERROR WITH ALERT RECORD INSERT:   $FAIL_MSG")
                        return
                    } else {
                        $ALERT_INSERT_ID = $ALERT_ENTRY_JSON.Success_Message
                        $ALERT_SUPPRESSION_FLAG = $ALERT_ENTRY_JSON.Alert_Suppression
                        if($true -eq $ALERT_SUPPRESSION_FLAG) {
                            log("ALERT RECORD ALREADY PRESENT:   $ALERT_INSERT_ID")
                        } else {
                            log("NEW ALERT RECORD CREATED:   $ALERT_INSERT_ID")
                            $data = @{ServerName= $SERVERNAME; InstanceName= $INSTANCENAME; DBName= $DATABASE | out-string; BackupType= $BACKUP_TYPE; Action="http://$IIS_HOST/EmailAlert/GetAlerts?alertId=$ALERT_INSERT_ID&alertType=OVERDUE_BACKUP"} | ConvertTo-Json
                            $EMAIL_DATA_COLLECTION.add($data) | Out-Null
                        }
                    }
                }
                
                # ----------- Sending an Email to Scheduled user with Overdue Backup Database Names -----------
                log("=========== Sending an Email with Overdue Backup Database Names ===========")
                if ([String]::IsNullOrEmpty($EMAIL_DATA_COLLECTION) -eq $false) {			
                    sendEmailAlert $EMAIL_DATA_COLLECTION "OVERDUE_BACKUP_ALERT"
                }
            
            } else {
                log("=========== No OverDue Databases Found! ===========")
            }
            
            $RUN_OUTPUT = @{OverdueBackupDatabases=$DATABASES | Out-String} | ConvertTo-Json
            
            $RUN_STATUS = "COMPLETED"

        } elseif ($JSON_PARSED_DATA.output -eq $false) {
            log($JSON_PARSED_DATA.Fail_Message)
            
            $RUN_OUTPUT = $JSON_PARSED_DATA.Fail_Message
            
            $RUN_STATUS = "FAILED"

        }
    } catch {
        $RUN_OUTPUT = $JSON_PARSED_DATA.Fail_Message
        
        $RUN_STATUS = "FAILED"
 
        $SCHEDLED_JOB_FAILED_EMAIL = @{ScheduledJobId=$SCHEDULED_JOB_ID; ScheduledJobRunHistoryId=$Schedule_Job_Run_History_InsertID; ErrorMessage=$RUN_OUTPUT} | ConvertTo-Json
        
        sendEmailAlert $SCHEDLED_JOB_FAILED_EMAIL "OVERDUE_BACKUP_JOB_FAILED"
    }

	# ----------- Finishing with Creating an Entry in ScheduledJobRunHistory Table -----------
	log("=========== Finishing with Creating an Entry in ScheduledJobRunHistory Table ===========")

	$RUN_END_TIME = Get-Date -format "yyyy-MM-dd HH:mm:ss"

    $Schedule_Job_Run_History_Data = scheduleJobHistoryRecord(@{query_type="UPDATE"; InsertId=$Schedule_Job_Run_History_InsertID; RunStartTime=$RUN_START_TIME; RunEndTime=$RUN_END_TIME; RunStatus=$RUN_STATUS; RunOutput=$RUN_OUTPUT})
    
    $Schedule_Job_Run_History_Data_Parsed = $Schedule_Job_Run_History_Data | ConvertFrom-Json

	if($schedule_Job_Run_History_Data_Parsed.output -eq $false) {
		log($Schedule_Job_Run_History_Data_Parsed.Fail_Message)

	} else {
		log($Schedule_Job_Run_History_Data_Parsed.Success_Message)
	}
}
# -----------------------------------------------------------------------------

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# Calling Main Function
log("+++++++++++++ START GETTING OVERDUE BACKUP DATABASES ++++++++++++++")
mainFunction
log("+++++++++++++ END GETTING OVERDUE BACKUP DATABASES ++++++++++++++")

#.\get_DBBackupOverdue.ps1 -JOB_INPUT '{"email":"sudhakara.rao.sajja@ey.com","overdue_backup_days":"7","backup_type":"DIFF","scheduled_job_id":335,"taskprefix":"WEEKLY_335"}' -SERVERNAME "DEFRAVMQMISSQ01.eyqa.net" -INSTANCENAME "DEFRAVMQMISSQ01.eyqa.net\INST1"