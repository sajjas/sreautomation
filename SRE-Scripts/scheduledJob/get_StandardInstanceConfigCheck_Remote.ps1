param(
	[String]$JOB_INPUT,
	[String]$SERVERNAME,
	[String]$INSTANCENAME
)

# -----------------------------------------------------------------------------
#          Global Definition
# -----------------------------------------------------------------------------
$SMTP = "smtp.ey.net"
$FROM = "standard.config.check@ey.com"

$JSON_DATA = $JOB_INPUT | ConvertFrom-Json 
$SCHEDULED_JOB_ID = $JSON_DATA.scheduled_job_id
$SCHEDULED_USER = $JSON_DATA.email

$NON_STANDARD_CONFIG_LIST = New-Object System.Collections.ArrayList
$TABLE_DATA_COLLECTION = New-Object System.Collections.ArrayList

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# -----------------------------------------------------------------------------
# @function get_email_template:  a) pull data from givven email template name
#                                b) print to log the pulled db info
# @return:   - Dictionary which encapsules the EmailTemaplate data
#            - null in case of any error.
#
# @param TemplateName:   Name of Email Template same as stored in DB
#                        table "EYDBSRE.automation.emailTemplate"
function get_email_template([String]$TemplateName)
{
	# -----------------------------------------------------------------------------
	$QRY_GET_EMAIL_TEMPLATE = "SELECT subject,TopBody,BottomBody,[From],[To],[CC],[BCC],'Body' as Body FROM automation.emailTemplate WHERE TemplateName = @TemplateName"
	# -----------------------------------------------------------------------------

    log("  *********************************get_email_template starts ********************************************")
    $emailTemplateInstance = $null
    try {
        if ($null -ne $TemplateName) {
            $emailTemplateInstance = executeSelectQuery -select_qry $QRY_GET_EMAIL_TEMPLATE -parameters @{ TemplateName = $TemplateName }
            if ($null -ne $emailTemplateInstance -and $emailTemplateInstance) {
                log("Return Email Template Instance")
                foreach($itemEmail in $emailTemplateInstance.keys)
                {
                    $value =  $emailTemplateInstance.item($itemEmail)
                    log(" $itemEmail --> $value ")
                }
            }
        }
    }
    catch{
        log("------------ get_email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************get_email_template ends  ********************************************")
    return $emailTemplateInstance
}

# -----------------------------------------------------------------------------
# @function send_Email_template: a) Validate for required Fields
#                                b) Contenate Top, Middle and Bottom Body
#                                c) Create Dictionay with message Parameters
#                                d) Send Email
# @return:   False - if any error during email creation or email is not sent.
#            True  - Email sent
#
# @param emailTemplateInstance:  Dictionary which encapsules the EmailTemaplate
#                                Data, Body is 'Body' String as default.
# @TODO: It could be greate to have SMTP value as DB Lookup configuration.
function send_Email_template($emailTemplateInstance)
{
    log("  *********************************send_Email_template starts ********************************************")
    $returnStatus = $false
    try
    {
        if ($null -ne $emailTemplateInstance) {
            # -- Validate for required fields are in dicc starts
            $daRequiredFields = @('subject','TopBody','Body','BottomBody','From','To')
            $emailContinue = $true
            foreach( $itemRequired in $daRequiredFields)
            {
                $fieldKey = $itemRequired
                $fieldValue = $emailTemplateInstance.item("$fieldKey")
                if ($null -eq $fieldValue -and $fieldValue.Length -eq 0){
                    log("$fieldKey is missing")
                    $emailContinue = $false
                    break
                }
            }
            # -- Validate for required fields are in dicc ends
            #------------------------------------------------------------------
            # Continue only if required values are set
            if ($emailContinue)
            {
                # I know this is redundant but is the way how PS works   --- starts
                $subject = $emailTemplateInstance.subject
                $TopBody = $emailTemplateInstance.TopBody
                $Body = $emailTemplateInstance.Body
                $BottomBody = $emailTemplateInstance.BottomBody
                $From = $emailTemplateInstance.From
                $To = $emailTemplateInstance.To
                # I know this is redundant but is the way how PS works   --- ends
                #------------------------------------------------------------------
                # Create Body starts
                log(" Creating Body start ---------")
                $Full_Body = New-Object Collections.Generic.List[string]
                $Full_Body.add($TopBody)
                $Full_Body.add($Body)
                $Full_Body.add($BottomBody)
                log(" Email Body --> $Full_Body")
                log(" Creating Body ends ---------")
                # Create Body Ends
                #------------------------------------------------------------------
                # Assign Values to message dic starts
                $messageParameters = @{
                Subject = "$subject"
                Body = "$Full_Body"
                From = "$From"
                To = $To.split(',')
                SmtpServer = "$SMTP"   # This value is taken from global configuration variables
                }
                # Add CC to dictionary if not null
                if ($null -ne $emailTemplateInstance.CC -and $emailTemplateInstance.CC){
                    $CC = $emailTemplateInstance.CC
                    $messageParameters.Cc = $CC.split(',')
                }
                # Add BCC to dictionary if not null
                if ($null -ne $emailTemplateInstance.BCC -and $emailTemplateInstance.BCC){
                    $BCC = $emailTemplateInstance.BCC
                    $messageParameters.Bcc = $BCC.split(',')
                }
                # Assign Values to message dic ends
                #------------------------------------------------------------------
                Send-MailMessage @messageParameters -BodyAsHtml
                $returnStatus = $True
                log( "----> Email sent From: $From To: $To  using smtp server: $SMTP")
            }
        }
    }
    catch{
        log("------------ send_Email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************send_Email_template ends  ********************************************")
    return $returnStatus
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function executeSelectQuery
{
    param ([String]$select_qry, $parameters = @{ })
    log( "<----------- Pulling Data from DB Starts -------->")
    if ($null -ne $select_qry -and $select_qry)
    {
        $query_final = $select_qry
        $dataset_cursor = New-Object System.Data.DataTable
        $sqlSelectConnection = getConnection
        $return_dic = $null
        if ($null -ne $sqlSelectConnection -and $sqlSelectConnection)
        {
            try
            {
                $sqlSelectConnection.Open()
                #SQL QUERY Execution
                $sqlCommand = New-Object System.Data.SqlClient.SqlCommand;
                $sqlCommand.Connection = $sqlSelectConnection;
                $sqlCommand.CommandText = $query_final;
                #SQL Parameters
                if ($null -ne $parameters -and $parameters)
                {
                    foreach ($p in $parameters.Keys)
                    {
                        [Void] $sqlCommand.Parameters.AddWithValue("@$p", $parameters[$p])
                        $param_text = $parameters[$p]
                        log("parameter $p  --> $param_text ")
                    }
                }
                #SQL Return Values
                $READER_DATA = $sqlCommand.ExecuteReader()
                $dataset_cursor.Load($READER_DATA)
                $sqlSelectConnection.Close()
                if ($null -ne $dataset_cursor)
                {
                    $return_dic = @{ }
                    foreach ($tupla in $dataset_cursor)
                    {
                        foreach ($columna in $dataset_cursor.Columns)
                        {
                            $value_x = $tupla.item($columna)
                            $columna_x = $columna
                            $return_dic.Add("$columna_x", "$value_x")
                            log("[$columna_x]=$value_x")
                        }
                    }
                }
            }
            catch
            {
                $return_dic = $null
                log($ERROR[0])
                log("Not Able to execute Select Query - [ $select_qry ]")
            }
        }

    }
    log( "<----------- Pulling Data from DB Ends -------->")
    return $return_dic
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function getConnection
{
    try
    {
        $connectionString = "server=$VAR_SERVERINSTANCE;database='$VAR_DATABASE';user id=$VAR_USERNAME;password=$VAR_PASSWORD";
        #SQL Connection - connection to SQL server
        $sqlConnection = new-object System.Data.SqlClient.SqlConnection;
        $sqlConnection.ConnectionString = $connectionString;
    }
    catch
    {
        log($ERROR[0])
        $sqlConnection = $null
    }
    return $sqlConnection
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function sendEmailAlert
{
	param(
		$data, 
		[string]$email_template_type
	)

	# -------------------------------------------------------------------------------------
	# Format data into HTML Table
	$TABLE_DATA_COLLECTION = tableData($data)
	
	# -------------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User before DB Backup Start.
	$emailTemplateInstance = get_email_template($email_template_type)
	$emailTemplateInstance.To = $SCHEDULED_USER
	$emailTemplateInstance.Body = $TABLE_DATA_COLLECTION
	send_Email_template($emailTemplateInstance)
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function tableData($data) 
{
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
    $new_data = $data | ConvertFrom-Json
	$htmlTableOutput = $new_data | select-object -property @{N='Server Name';E={$SERVERNAME}}, @{N='Instance Name';E={$INSTANCENAME}}, @{N='Configuration Id';E={$_.configuration_id}}, @{N='Name';E={$_.name}}, @{N='Maximum';E={$_.maximum}}, @{N='Value In Use';E={$_.value_in_use}}, @{N='Description';E={$_.description}}, @{N='Value Type';E={$_.ValueType}}, @{N='Std Value';E={$_.StdValue}} | ConvertTo-Html -Head $style
    
    Add-Type -AssemblyName System.Web
    $tableOutput = [System.Web.HttpUtility]::HtmlDecode($htmlTableOutput)
	
    return $tableOutput
}

# -----------------------------------------------------------------------------
function nonStandardInstanceConfigCheck
{
    param (
        $LIST_OF_RESULTS_TO_COMPARE, 
        $MEMORY_IN_BYTES    
    )
    
	foreach ($OBJ in $LIST_OF_RESULTS_TO_COMPARE) {
		# Simple
		if($OBJ.ValueType -eq "Simple") {
			if($OBJ.value_in_use -ne $OBJ.StdValue) {
				$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
			}
		# Range
		} elseif($OBJ.ValueType -eq "Range") {
			if ($OBJ.StdValue -Match "-") {
				$range = $OBJ.StdValue.split("-")
				$min = $range[0]
				$max = $range[1]
				if($min -gt $OBJ.value_in_use -And $OBJ.value_in_use -gt $max) {
					$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
				}
			} else {
				if($OBJ.value_in_use -lt $OBJ.StdValue) {
					$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
				}
			}
		# Complex
		} elseif($OBJ.ValueType -eq "Complex" -And $OBJ.ConfigId -eq 1544) {
			$80_PERC_VALUE = $MEMORY_IN_BYTES * (80/100)
			if($OBJ.value_in_use -gt $80_PERC_VALUE) {
				$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
			}
		}
	}
	
	return $NON_STANDARD_CONFIG_LIST
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function instanceConfig
{
	log("SERVERNAME:	$SERVERNAME")
	LOG("INSTANCENAME:	$INSTANCENAME")
	
	$r1_result = processSqlQuery $INSTANCENAME
	$memory_in_bytes = (Get-WmiObject -class "cim_physicalmemory" | Measure-Object -Property Capacity -Sum).Sum

	if ([String]::IsNullOrEmpty($r1_result) -eq $true) {
        log("No Output Returned from the Instance")
        Exit
	}
	
	$r1_display = $r1_result | Format-Table | Out-String 
    log("Result R1:	$r1_display")
	
	#Get data below query result in r2 from SREDB
	$getStandardValuesFromToolDBQuery = "SELECT B.ConfigId, B.ValueType, B.StdValue 
	 FROM automation.ConfigTemplates B 
	 Where InstanceType = 'Type1'
	 ORDER BY B.ConfigId
	"
	$r2_result = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query $getStandardValuesFromToolDBQuery -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	$r2_display = $r2_result | Format-Table | Out-String 
    log("Result R2:	$r2_display")
	
	#Refer this: https://devblogs.microsoft.com/powershell/joining-multiple-tables-grouping-and-evaluating-totals/
	#https://stackoverflow.com/questions/1848821/in-powershell-whats-the-best-way-to-join-two-tables-into-one
	try {
		$FinalContent = $r1_result | Join-Object $r2_result -On configuration_id -Equals ConfigId | Select-Object configuration_id, name, value, maximum, value_in_use, description, ConfigId, ValueType, StdValue
		return @{output=$true; Success_Message=$FinalContent; Memory_In_Bytes=$memory_in_bytes} | ConvertTo-Json
	} catch {
		return @{output=$false; Fail_Message=$_.Exception.Message} | ConvertTo-Json
	}
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function processSqlQuery
{
	param(
		$INSTANCENAME
	)
	
	#Get data below query result in r1 from remote Server
	$queryString = "SELECT A.configuration_id, A.name, A.value, A.maximum, A.value_in_use, A.description
	FROM sys.configurations A
	ORDER BY A.configuration_id
	"
	try {
		$result = invoke-sqlcmd -serverInstance $INSTANCENAME -query $queryString
	} catch {
		log("ERROR:	$_Exception.Message")
		Exit
	}
	
	return $result
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function log($Message)
{
	#$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir\non-standard-instance-config-check-output.log
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function Join-Object {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseLiteralInitializerForHashtable', '', Scope='Function')]
	[CmdletBinding(DefaultParameterSetName='Default')][OutputType([Object[]])]Param (

		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Default')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'On')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Expression')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Property')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Discern')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'OnProperty')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'OnDiscern')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'ExpressionProperty')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'ExpressionDiscern')]
		$LeftObject,

		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Default')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'On')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Expression')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Property')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Discern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'OnProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'OnDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'ExpressionProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'ExpressionDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Self')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfOn')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfExpression')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfOnProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfOnDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfExpressionProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfExpressionDiscern')]
		$RightObject,

		[Parameter(Position = 1, ParameterSetName = 'On', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'OnProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'OnDiscern', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfOn', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfOnProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfOnDiscern', Mandatory = $True)]
		[Alias("Using")][String[]]$On,

		[Parameter(Position = 1, ParameterSetName = 'Expression', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'ExpressionProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'ExpressionDiscern', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfExpression', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfExpressionProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfExpressionDiscern', Mandatory = $True)]
		[Alias("UsingExpression")][ScriptBlock]$OnExpression,

		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[String[]]$Equals,

		[Parameter(Position = 2, ParameterSetName = 'Discern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'OnDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'ExpressionDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'SelfDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'SelfOnDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'SelfExpressionDiscern', Mandatory = $True)]
		[AllowEmptyString()][String[]]$Discern,

		[Parameter(ParameterSetName = 'Property', Mandatory = $True)]
		[Parameter(ParameterSetName = 'OnProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'ExpressionProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'SelfProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'SelfOnProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'SelfExpressionProperty', Mandatory = $True)]
		$Property,

		[Parameter(Position = 3, ParameterSetName = 'Default')]
		[Parameter(Position = 3, ParameterSetName = 'On')]
		[Parameter(Position = 3, ParameterSetName = 'Expression')]
		[Parameter(Position = 3, ParameterSetName = 'Property')]
		[Parameter(Position = 3, ParameterSetName = 'Discern')]
		[Parameter(Position = 3, ParameterSetName = 'OnProperty')]
		[Parameter(Position = 3, ParameterSetName = 'OnDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'ExpressionProperty')]
		[Parameter(Position = 3, ParameterSetName = 'ExpressionDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'Self')]
		[Parameter(Position = 3, ParameterSetName = 'SelfOn')]
		[Parameter(Position = 3, ParameterSetName = 'SelfExpression')]
		[Parameter(Position = 3, ParameterSetName = 'SelfProperty')]
		[Parameter(Position = 3, ParameterSetName = 'SelfDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'SelfOnProperty')]
		[Parameter(Position = 3, ParameterSetName = 'SelfOnDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'SelfExpressionProperty')]
		[Parameter(Position = 3, ParameterSetName = 'SelfExpressionDiscern')]
		[ScriptBlock]$Where = {$True},

		[Parameter(ParameterSetName = 'Default')]
		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'Expression')]
		[Parameter(ParameterSetName = 'Property')]
		[Parameter(ParameterSetName = 'Discern')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'ExpressionProperty')]
		[Parameter(ParameterSetName = 'ExpressionDiscern')]
		[Parameter(ParameterSetName = 'Self')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfExpression')]
		[Parameter(ParameterSetName = 'SelfProperty')]
		[Parameter(ParameterSetName = 'SelfDiscern')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[Parameter(ParameterSetName = 'SelfExpressionProperty')]
		[Parameter(ParameterSetName = 'SelfExpressionDiscern')]
		[ValidateSet('Inner', 'Left', 'Right', 'Full', 'Cross')]$JoinType = 'Inner',

		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[Switch]$Strict,

		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[Alias("CaseSensitive")][Switch]$MatchCase
	)
	Begin {
		$HashTable = $Null; $Esc = [Char]27; $EscSeparator = $Esc + ','
		$Expression = [Ordered]@{}; $PropertyList = [Ordered]@{}; $Related = @()
		If ($RightObject -isnot [Array] -and $RightObject -isnot [Data.DataTable]) {$RightObject = @($RightObject)}
		$RightKeys = @(
			If ($RightObject -is [Data.DataTable]) {$RightObject.Columns | Select-Object -ExpandProperty 'ColumnName'}
			Else {
				$First = $RightObject | Select-Object -First 1
				If ($First -is [System.Collections.IDictionary]) {$First.Get_Keys()}
				Else {$First.PSObject.Properties | Select-Object -ExpandProperty 'Name'}
			}
		)
		$RightProperties = @{}; ForEach ($Key in $RightKeys) {$RightProperties.$Key = $Null}
		$RightVoid = New-Object PSCustomObject -Property $RightProperties
		$RightLength = @($RightObject).Length; $LeftIndex = 0; $InnerRight = @($False) * $RightLength
		Function OutObject($LeftIndex, $RightIndex, $Left = $LeftVoid, $Right = $RightVoid) {
			If (&$Where) {
				ForEach ($_ in $Expression.Get_Keys()) {$PropertyList.$_ = &$Expression.$_}
				New-Object PSCustomObject -Property $PropertyList
			}
		}
		Function SetExpression([String]$Key, [ScriptBlock]$ScriptBlock) {
			If ($Key -eq '*') {$Key = $Null}
			If ($Key -and $ScriptBlock) {$Expression.$Key = $ScriptBlock}
			Else {
				$Keys = If ($Key) {@($Key)} Else {$LeftKeys + $RightKeys}
				ForEach ($Key in $Keys) {
					If (!$Expression.Contains($Key)) {
						$InLeft  = $LeftKeys  -Contains $Key
						$InRight = $RightKeys -Contains $Key
						If ($InLeft -and $InRight) {
							$Expression.$Key = If ($ScriptBlock) {$ScriptBlock}
								ElseIf ($Related -NotContains $Key) {{$Left.$_, $Right.$_}}
								Else {{If ($Null -ne $LeftIndex) {$Left.$_} Else {$Right.$_}}}
						}
						ElseIf ($InLeft)  {$Expression.$Key = {$Left.$_}}
						ElseIf ($InRight) {$Expression.$Key = {$Right.$_}}
						Else {Throw [ArgumentException]"The property '$Key' cannot be found on the left or right object."}
					}
				}
			}
		}
	}
	Process {
		Try {
			$SelfJoin = !$PSBoundParameters.ContainsKey('LeftObject'); If ($SelfJoin) {$LeftObject = $RightObject}
			ForEach ($Left in @($LeftObject)) {
				$InnerLeft = $Null
				If (!$LeftIndex) {
					$LeftKeys = @(
						If ($Left -is [Data.DataRow]) {$Left.Table.Columns | Select-Object -ExpandProperty 'ColumnName'}
						ElseIf ($Left -is [System.Collections.IDictionary]) {$Left.Get_Keys()}
						Else {$Left.PSObject.Properties | Select-Object -ExpandProperty 'Name'}
					)
					$LeftProperties = @{}; ForEach ($Key in $LeftKeys) {$LeftProperties.$Key = $Null}
					$LeftVoid = New-Object PSCustomObject -Property $LeftProperties
					If ($Null -ne $On -or $Null -ne $Equals) {
						$On = If ($On) {,@($On)} Else {,@()}; $Equals = If ($Equals) {,@($Equals)} Else {,@()}
						For ($i = 0; $i -lt [Math]::Max($On.Length, $Equals.Length); $i++) {
							If ($i -ge $On.Length) {$On += $Equals[$i]}
							If ($LeftKeys -NotContains $On[$i]) {Throw [ArgumentException]"The property '$($On[$i])' cannot be found on the left object."}
							If ($i -ge $Equals.Length) {$Equals += $On[$i]}
							If ($RightKeys -NotContains $Equals[$i]) {Throw [ArgumentException]"The property '$($Equals[$i])' cannot be found on the right object."}
							If ($On[$i] -eq $Equals[$i]) {$Related += $On[$i]}
						}
						$HashTable = If ($MatchCase) {[HashTable]::New(0, [StringComparer]::Ordinal)} Else {@{}}
						$RightIndex = 0; ForEach ($Right in $RightObject) {
							$Keys = ForEach ($Name in @($Equals)) {$Right.$Name}
							$HashKey = If (!$Strict) {[String]::Join($EscSeparator, @($Keys))}
									   Else {[System.Management.Automation.PSSerializer]::Serialize($Keys)}
							[Array]$HashTable[$HashKey] += $RightIndex++
						}
					}
					If ($Discern) {
						If (@($Discern).Count -le 1) {$Discern = @($Discern) + ''}
						ForEach ($Key in $LeftKeys) {
							If ($RightKeys -Contains $Key) {
								If ($Related -Contains $Key) {
									$Expression[$Key] = {If ($Null -ne $LeftIndex) {$Left.$_} Else {$Right.$_}}
								} Else {
									$Name = If ($Discern[0].Contains('*')) {([Regex]"\*").Replace($Discern[0], $Key, 1)} Else {$Discern[0] + $Key}
									$Expression[$Name] = [ScriptBlock]::Create("`$Left.'$Key'")
								}
							} Else {$Expression[$Key] = {$Left.$_}}
						}
						ForEach ($Key in $RightKeys) {
							If ($LeftKeys -Contains $Key) {
								If ($Related -NotContains $Key) {
									$Name = If ($Discern[1].Contains('*')) {([Regex]"\*").Replace($Discern[1], $Key, 1)} Else {$Discern[1] + $Key}
									$Expression[$Name] = [ScriptBlock]::Create("`$Right.'$Key'")
								}
							} Else {$Expression[$Key] = {$Right.$_}}
						}
					} ElseIf ($Property) {
						ForEach ($Item in @($Property)) {
							If ($Item -is [ScriptBlock]) {SetExpression $Null $Item}
							ElseIf ($Item -is [System.Collections.IDictionary]) {ForEach ($Key in $Item.Get_Keys()) {SetExpression $Key $Item.$Key}}
							Else {SetExpression $Item}
						}
					} Else {SetExpression}
				}
				$RightList = `
					If ($On) {
						If ($JoinType -eq "Cross") {Throw [ArgumentException]"The On parameter cannot be used on a cross join."}
						$Keys = ForEach ($Name in @($On)) {$Left.$Name}
						$HashKey = If (!$Strict) {[String]::Join($EscSeparator, @($Keys))}
								   Else {[System.Management.Automation.PSSerializer]::Serialize($Keys)}
						$HashTable[$HashKey]
					} ElseIf ($OnExpression) {
						If ($JoinType -eq "Cross") {Throw [ArgumentException]"The OnExpression parameter cannot be used on a cross join."}
						For ($RightIndex = 0; $RightIndex -lt $RightLength; $RightIndex++) {
							$Right = $RightObject[$RightIndex]; If (&$OnExpression) {$RightIndex}
						}
					}
					ElseIf ($JoinType -eq "Cross") {0..($RightObject.Length - 1)}
					ElseIf ($LeftIndex -lt $RightLength) {$LeftIndex} Else {$Null}
				ForEach ($RightIndex in $RightList) {
					$Right = If ($RightObject -is [Data.DataTable]) {$RightObject.Rows[$RightIndex]} Else {$RightObject[$RightIndex]}
						$OutObject = OutObject -LeftIndex $LeftIndex -RightIndex $RightIndex -Left $Left -Right $Right
						If ($Null -ne $OutObject) {$OutObject; $InnerLeft = $True; $InnerRight[$RightIndex] = $True}
				}
				If (!$InnerLeft -and ($JoinType -eq "Left" -or $JoinType -eq "Full")) {OutObject -LeftIndex $LeftIndex -Left $Left}
				$LeftIndex++
			}
		} Catch [ArgumentException] {
			$PSCmdlet.ThrowTerminatingError($_)
		}
	}
	End {
		If ($JoinType -eq "Right" -or $JoinType -eq "Full") {$Left = $Null
			$RightIndex = 0; ForEach ($Right in $RightObject) {
				If (!$InnerRight[$RightIndex]) {OutObject -RightIndex $RightIndex -Right $Right}
				$RightIndex++
			}
		}
	}
}; Set-Alias Join Join-Object

# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function scheduleJobHistoryRecord($PARAM=@{})
{
    $INSERT_ID = $PARAM.InsertId
    $RUN_START_TIME = $PARAM.RunStartTime
    $RUN_END_TIME = $PARAM.RunEndTime
    $RUN_STATUS = $PARAM.RunStatus
	$RUN_OUTPUT = $PARAM.RunOutput

    # -------------------------------------------------------------------------
	if($param.query_type -eq "INSERT") {
		$queryString = "
		INSERT INTO [automation].[ScheduledJobsRunHistory]
			   ([ScheduledJobid]
			   ,[ServerName]
			   ,[InstanceName]
			   ,[DBName]
			   ,[RunStarttime]
			   ,[RunEndtime]
			   ,[RunStatus]
			   ,[RunOutput])
			VALUES
			   ('$SCHEDULED_JOB_ID'
			   ,'$SERVERNAME'
			   ,'$INSTANCENAME'
			   ,'NULL'
			   ,'$RUN_START_TIME'
			   ,'$RUN_START_TIME'
			   ,'$RUN_STATUS'
			   ,'NULL');
			   SELECT SCOPE_IDENTITY();
		"
	} elseif($param.query_type -eq "UPDATE") {
		$queryString=" 
		UPDATE [automation].[ScheduledJobsRunHistory] SET
			RunEndtime='$RUN_END_TIME', 
			RunStatus='$RUN_STATUS',
            RunOutput='$RUN_OUTPUT'
		WHERE Id='$INSERT_ID'; 
		SELECT SCOPE_IDENTITY();
		"
	}
	
	try {
        $Schedulejob_Run_History_Query_Output = invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
        $Schedulejob_Run_History_Query_Output_Json = @{output = $true; Success_Message = $Schedulejob_Run_History_Query_Output.Column1} | ConvertTo-Json
	} catch {
		$Schedulejob_Run_History_Query_Output_Json = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	
	return $Schedulejob_Run_History_Query_Output_Json
}

# -----------------------------------------------------------------------------
Function AlertRecord($PARAM=@{})
{
	$ALERT_STATUS = $PARAM.Status
    $ALERT_TYPE = $PARAM.Type
    $INFO = $PARAM.Info
	$DATABASE = $PARAM.Database

	$queryString = "
	INSERT INTO [automation].[Alert]
		   ([TypeId]
		   ,[CreationDate]
		   ,[StatusId]
		   ,[Info]
		   ,[Last_Modified_Date]
		   ,[ServerName]
		   ,[InstanceName]
		   ,[DBName])
		VALUES
			('$ALERT_TYPE'
			,getdate()
			,'$ALERT_STATUS'
			,'$INFO'
			,getdate()
			,'$SERVERNAME'
			,'$INSTANCENAME'
			,'$DATABASE');
		SELECT SCOPE_IDENTITY();
	"
	try {
		$Alert_Insert_Query_Output = (invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD").Column1
		$Alert_Insert_Query_Output_Json = @{output = $true; Success_Message = $Alert_Insert_Query_Output} | ConvertTo-Json
	} catch {
		$Alert_Insert_Query_Output_Json = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	
	return $Alert_Insert_Query_Output_Json
}

# -----------------------------------------------------------------------------
Function mainFunction
{
	# ----------- Starting with Creating an Entry in ScheduledJobRunHistory Table -----------
	log("=========== Starting with Creating an Entry in ScheduledJobRunHistory Table ===========")
	
	$RUN_START_TIME = Get-Date -format "yyyy-MM-dd HH:mm:ss"

	$RUN_STATUS = "STARTED"
	
	$Schedule_Job_Run_History_Data = scheduleJobHistoryRecord(@{query_type="INSERT"; RunStartTime=$RUN_START_TIME; RunEndTime=$RUN_START_TIME; RunStatus=$RUN_STATUS; RunOutput="NULL"})

	$Schedule_Job_Run_History_Data_Parsed = $Schedule_Job_Run_History_Data | ConvertFrom-Json

	if($schedule_Job_Run_History_Data_Parsed.output -eq $false) {
		log($Schedule_Job_Run_History_Data_Parsed.Fail_Message)
		Exit

	} elseif($Schedule_Job_Run_History_Data_Parsed.output -eq $true -And [String]::IsNullOrEmpty($Schedule_Job_Run_History_Data_Parsed.Success_Message) -eq $true) {
		log("ERROR: Schedule Job Eun History InsertID missing")
		Exit

	} else {
		$Schedule_Job_Run_History_InsertID = $Schedule_Job_Run_History_Data_Parsed.Success_Message
		log("ScheduledJobRunHistory Insert ID:	$Schedule_Job_Run_History_InsertID")
	}

	# ----------- Fetching the Sql Instance Configurations -----------
	log("=========== Fetching the Sql Instance Confiugrations ===========")

	$LIST_OF_RESULTS_TO_COMPARE = instanceConfig

	$JSON_PARSED_DATA = $LIST_OF_RESULTS_TO_COMPARE | ConvertFrom-Json

	if ($JSON_PARSED_DATA.output -eq $true) {
		# ----------- Checking For Non Standard Sql Instance Configurations -----------
		log("=========== Checking For Non Standard Sql Instance Configurations ===========")

		$NON_STD_INSTANCE_CONFIG_DATA = nonStandardInstanceConfigCheck $JSON_PARSED_DATA.Success_Message $JSON_PARSED_DATA.Memory_In_Bytes.value | ConvertTo-Json
		
		if($NON_STD_INSTANCE_CONFIG_DATA.Count -le 1) {
			$INFO = @"
{
		"NonStandardInstanceConfig": [$NON_STD_INSTANCE_CONFIG_DATA]
}
"@
		} else {
			$INFO = @"
{
		"NonStandardInstanceConfig": $NON_STD_INSTANCE_CONFIG_DATA
}
"@
		}
		
		if ([String]::IsNullOrEmpty($NON_STD_INSTANCE_CONFIG_DATA) -eq $false) {

			# ----------- Inserting Entry with Non Standard Sql Instance Configurations in Alert Table -----------
			log("=========== Inserting Entry with Non Standard Sql Instance Configurations in Alert Table ===========")
			$ALERT_ENTRY = AlertRecord(@{Status="CREATED"; Type="NON_STANDARD_INSTANCE"; Info=$INFO; Database="NULL"})
            $ALERT_ENTRY_JSON = $ALERT_ENTRY | ConvertFrom-Json
            
            if ($ALERT_ENTRY_JSON.output -eq $false) {
                log("ERROR:   $ALERT_ENTRY_JSON")
                Exit
            } else {
                $ALERT_INSERT_ID = $ALERT_ENTRY_JSON.Success_Message
                log("Alert Enter Insert ID:   $ALERT_INSERT_ID")
            }
			
			# ----------- Sending an Email to Scheduled user with Non Standard Sql Instance Configurations -----------
			log("=========== Sending an Email with Non Standard Sql Instance Configurations ===========")
			sendEmailAlert $NON_STD_INSTANCE_CONFIG_DATA "NON_STANDARD_INSTANCE_ALERT"
		
		} else {
			log("=========== No Non Standard Sql Instance Configurations Found! ===========")
		}
		
		$RUN_OUTPUT = $INFO
		
		$RUN_STATUS = "COMPLETED"

	} elseif ($JSON_PARSED_DATA.output -eq $false) {
		log($JSON_PARSED_DATA.Fail_Message)
		
		$RUN_OUTPUT = $JSON_PARSED_DATA.Fail_Message
		
		$RUN_STATUS = "FAILED"
	}

	# ----------- Finishing with Creating an Entry in ScheduledJobRunHistory Table -----------
	log("=========== Finishing with Creating an Entry in ScheduledJobRunHistory Table ===========")

	$RUN_END_TIME = Get-Date -format "yyyy-MM-dd HH:mm:ss"

	$Schedule_Job_Run_History_Data = scheduleJobHistoryRecord(@{query_type="UPDATE"; InsertId=$Schedule_Job_Run_History_InsertID; RunStartTime=$RUN_START_TIME; RunEndTime=$RUN_END_TIME; RunStatus=$RUN_STATUS; RunOutput=$RUN_OUTPUT})

	$Schedule_Job_Run_History_Data_Parsed = $Schedule_Job_Run_History_Data | ConvertFrom-Json

	if($schedule_Job_Run_History_Data_Parsed.output -eq $false) {
		log($Schedule_Job_Run_History_Data_Parsed.Fail_Message)

	} else {
		log($Schedule_Job_Run_History_Data_Parsed.Success_Message)
	}
}

#Main Function
log("+++++++++++++ START STANDARD INSTANCE CONFIG CHECK ++++++++++++++")
log("JOB INPUTS:")
log("SCHEDULED_JOB_ID:	$SCHEDULED_JOB_ID ")
log("SCHEDULED_USER:	$SCHEDULED_USER")
mainFunction
log("+++++++++++++ END STANDARD INSTANCE CONFIG CHECK ++++++++++++++")
  