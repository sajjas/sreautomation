param(
	[String]$JOB_INPUT,
	[String]$SERVERNAME,
	[String]$INSTANCENAME
)

# -----------------------------------------------------------------------------
#          Global Definition
# -----------------------------------------------------------------------------
$SMTP = "smtp.ey.net"

$JSON_DATA = $JOB_INPUT | ConvertFrom-Json 
$SCHEDULED_JOB_ID = $JSON_DATA.scheduled_job_id
$SCHEDULED_USER = $JSON_DATA.email

$NON_STANDARD_CONFIG_LIST = New-Object System.Collections.ArrayList
$TABLE_DATA_COLLECTION = New-Object System.Collections.ArrayList

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"


# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# -----------------------------------------------------------------------------
# @function get_email_template:  a) pull data from givven email template name
#                                b) print to log the pulled db info
# @return:   - Dictionary which encapsules the EmailTemaplate data
#            - null in case of any error.
#
# @param TemplateName:   Name of Email Template same as stored in DB
#                        table "EYDBSRE.automation.emailTemplate"
function get_email_template([String]$TemplateName)
{
	# -----------------------------------------------------------------------------
	$QRY_GET_EMAIL_TEMPLATE = "SELECT subject,TopBody,BottomBody,[From],[To],[CC],[BCC],'Body' as Body FROM automation.emailTemplate WHERE TemplateName = @TemplateName"
	# -----------------------------------------------------------------------------

    log("  *********************************get_email_template starts ********************************************")
    $emailTemplateInstance = $null
    try {
        if ($null -ne $TemplateName) {
            $emailTemplateInstance = executeSelectQuery -select_qry $QRY_GET_EMAIL_TEMPLATE -parameters @{ TemplateName = $TemplateName }
            if ($null -ne $emailTemplateInstance -and $emailTemplateInstance) {
                log("Return Email Template Instance")
                foreach($itemEmail in $emailTemplateInstance.keys)
                {
                    $value =  $emailTemplateInstance.item($itemEmail)
                    log(" $itemEmail --> $value ")
                }
            }
        }
    }
    catch{
        log("------------ get_email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************get_email_template ends  ********************************************")
    return $emailTemplateInstance
}

# -----------------------------------------------------------------------------
# @function send_Email_template: a) Validate for required Fields
#                                b) Contenate Top, Middle and Bottom Body
#                                c) Create Dictionay with message Parameters
#                                d) Send Email
# @return:   False - if any error during email creation or email is not sent.
#            True  - Email sent
#
# @param emailTemplateInstance:  Dictionary which encapsules the EmailTemaplate
#                                Data, Body is 'Body' String as default.
# @TODO: It could be greate to have SMTP value as DB Lookup configuration.
function send_Email_template($emailTemplateInstance)
{
    log("  *********************************send_Email_template starts ********************************************")
    $returnStatus = $false
    try
    {
        if ($null -ne $emailTemplateInstance) {
            # -- Validate for required fields are in dicc starts
            $daRequiredFields = @('subject','TopBody','Body','BottomBody','From','To')
            $emailContinue = $true
            foreach( $itemRequired in $daRequiredFields)
            {
                $fieldKey = $itemRequired
                $fieldValue = $emailTemplateInstance.item("$fieldKey")
                if ($null -eq $fieldValue -and $fieldValue.Length -eq 0){
                    log("$fieldKey is missing")
                    $emailContinue = $false
                    break
                }
            }
            # -- Validate for required fields are in dicc ends
            #------------------------------------------------------------------
            # Continue only if required values are set
            if ($emailContinue)
            {
                # I know this is redundant but is the way how PS works   --- starts
                $subject = $emailTemplateInstance.subject
                $TopBody = $emailTemplateInstance.TopBody
                $Body = $emailTemplateInstance.Body
                $BottomBody = $emailTemplateInstance.BottomBody
                $From = $emailTemplateInstance.From
                $To = $emailTemplateInstance.To
                # I know this is redundant but is the way how PS works   --- ends
                #------------------------------------------------------------------
                # Create Body starts
                log(" Creating Body start ---------")
                $Full_Body = New-Object Collections.Generic.List[string]
                $Full_Body.add($TopBody)
                $Full_Body.add($Body)
                $Full_Body.add($BottomBody)
                log(" Email Body --> $Full_Body")
                log(" Creating Body ends ---------")
                # Create Body Ends
                #------------------------------------------------------------------
                # Assign Values to message dic starts
                $messageParameters = @{
                Subject = "$subject"
                Body = "$Full_Body"
                From = "$From"
                To = $To.split(',')
                SmtpServer = "$SMTP"   # This value is taken from global configuration variables
                }
                # Add CC to dictionary if not null
                if ($null -ne $emailTemplateInstance.CC -and $emailTemplateInstance.CC){
                    $CC = $emailTemplateInstance.CC
                    $messageParameters.Cc = $CC.split(',')
                }
                # Add BCC to dictionary if not null
                if ($null -ne $emailTemplateInstance.BCC -and $emailTemplateInstance.BCC){
                    $BCC = $emailTemplateInstance.BCC
                    $messageParameters.Bcc = $BCC.split(',')
                }
                # Assign Values to message dic ends
                #------------------------------------------------------------------
                Send-MailMessage @messageParameters -BodyAsHtml
                $returnStatus = $True
                log( "----> Email sent From: $From To: $To  using smtp server: $SMTP")
            }
        }
    }
    catch{
        log("------------ send_Email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************send_Email_template ends  ********************************************")
    return $returnStatus
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function executeSelectQuery
{
    param ([String]$select_qry, $parameters = @{ })
    log( "<----------- Pulling Data from DB Starts -------->")
    if ($null -ne $select_qry -and $select_qry)
    {
        $query_final = $select_qry
        $dataset_cursor = New-Object System.Data.DataTable
        $sqlSelectConnection = getConnection
        $return_dic = $null
        if ($null -ne $sqlSelectConnection -and $sqlSelectConnection)
        {
            try
            {
                $sqlSelectConnection.Open()
                #SQL QUERY Execution
                $sqlCommand = New-Object System.Data.SqlClient.SqlCommand;
                $sqlCommand.Connection = $sqlSelectConnection;
                $sqlCommand.CommandText = $query_final;
                #SQL Parameters
                if ($null -ne $parameters -and $parameters)
                {
                    foreach ($p in $parameters.Keys)
                    {
                        [Void] $sqlCommand.Parameters.AddWithValue("@$p", $parameters[$p])
                        $param_text = $parameters[$p]
                        log("parameter $p  --> $param_text ")
                    }
                }
                #SQL Return Values
                $READER_DATA = $sqlCommand.ExecuteReader()
                $dataset_cursor.Load($READER_DATA)
                $sqlSelectConnection.Close()
                if ($null -ne $dataset_cursor)
                {
                    $return_dic = @{ }
                    foreach ($tupla in $dataset_cursor)
                    {
                        foreach ($columna in $dataset_cursor.Columns)
                        {
                            $value_x = $tupla.item($columna)
                            $columna_x = $columna
                            $return_dic.Add("$columna_x", "$value_x")
                            log("[$columna_x]=$value_x")
                        }
                    }
                }
            }
            catch
            {
                $return_dic = $null
                log($ERROR[0])
                log("Not Able to execute Select Query - [ $select_qry ]")
            }
        }

    }
    log( "<----------- Pulling Data from DB Ends -------->")
    return $return_dic
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function getConnection
{
    try
    {
        $connectionString = "server=$VAR_SERVERINSTANCE;database='$VAR_DATABASE';user id=$VAR_USERNAME;password=$VAR_PASSWORD";
        #SQL Connection - connection to SQL server
        $sqlConnection = new-object System.Data.SqlClient.SqlConnection;
        $sqlConnection.ConnectionString = $connectionString;
    }
    catch
    {
        log($ERROR[0])
        $sqlConnection = $null
    }
    return $sqlConnection
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function sendEmailAlert
{
    param(
		$data, 
		[string]$email_template_type
	)

	# -------------------------------------------------------------------------------------
	# Format data into HTML Table
	$TABLE_DATA_COLLECTION = tableData($data)
	
	# -------------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User before DB Backup Start.
	$emailTemplateInstance = get_email_template($email_template_type)
	$emailTemplateInstance.To = $SCHEDULED_USER
	$emailTemplateInstance.Body = $TABLE_DATA_COLLECTION
	send_Email_template($emailTemplateInstance)
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function tableData($data) 
{
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
    $new_data = $data | ConvertFrom-Json
	$htmlTableOutput = $new_data | select-object -property @{N='Server Name';E={$SERVERNAME}}, @{N='Instance Name';E={$INSTANCENAME}}, @{N='Database Id';E={$_.database_id}}, @{N='Database Name';E={$_.database_name}}, @{N='Physical Name';E={$_.physical_name}}, @{N='Size';E={$_.size}}, @{N='Growth(MB)';E={$_.growth_in_MB}}, @{N='Is Percent Growth';E={$_.is_percent_growth}} | ConvertTo-Html -Head $style
    
    Add-Type -AssemblyName System.Web
    $tableOutput = [System.Web.HttpUtility]::HtmlDecode($htmlTableOutput)
	
    return $tableOutput
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function processSqlQuery
{
	param(
		$INSTANCENAME
	)
	
	#Get data below query result in r1 from remote Server
    $queryString = "select a.database_id, 
       a.[name] as database_name, 
       b.physical_name,
       a.is_auto_close_on, 
       a.is_auto_shrink_on, 
       b.size, 
       b.max_size,
		CASE WHEN b.is_percent_growth = 0
			THEN CONVERT(DECIMAL(20,0), (CONVERT(DECIMAL, b.growth)/128)) 
			ELSE b.growth
			END AS growth_in_MB, 
		b.is_percent_growth
        from (select * from sys.databases where name NOT IN('master', 'tempdb', 'msdb', 'Statistics_DB')) as a, sys.master_files as b
        where a.database_id = b.database_id and b.type_desc = 'ROWS' and b.file_id = 1"

    try {
        $result = invoke-sqlcmd -serverInstance $INSTANCENAME -query $queryString | Select-Object database_id, database_name, physical_name, is_auto_close_on, is_auto_shrink_on, size, max_size, growth_in_MB, is_percent_growth
    } catch {
        log("ERROR:	$_.Exception.Message")
        Exit
    }
	
	return $result
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function databaseConfig
{
	log("SERVERNAME:	$SERVERNAME")
	LOG("INSTANCENAME:	$INSTANCENAME")
	
	$r1_result = processSqlQuery $INSTANCENAME
    
    if ([String]::IsNullOrEmpty($r1_result) -eq $true) {
        log("No Output Returned from the Instance")
        Exit
    }

	$r1_display = $r1_result | Format-Table | Out-String 
    log("Result R1:	$r1_display")
	
	#Get data below query result in r2 from SREDB
	$getStandardValuesFromToolDBQuery = "SELECT B.ConfigId, B.ConfigName, B.ValueType, B.StdValue 
	 FROM automation.ConfigTemplates B 
	 Where ConfigType = 'DB'
	 ORDER BY B.ConfigId
    "
    try {
        $r2_result = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query $getStandardValuesFromToolDBQuery -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD | Select-Object ConfigId, ConfigName, ValueType, StdValue
        $r2_display = $r2_result | Format-Table | Out-String 
        log("Result R2:	$r2_display")
        
        return @{output=$true; Actual_data=$r1_result; Standard_data=$r2_result} | ConvertTo-Json
    } catch {
        return @{output=$false; Fail_Message=$_.Exception.Message} | ConvertTo-Json
    }
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function nonStandardDatabaseConfigCheck
{
    param (
        $LIST_OF_RESULTS_TO_COMPARE, 
        $LIST_OF_STANDARD_VALUES    
    )
    
    $UPDATED_LIST_OF_RESULTS_TO_COMPARE = New-Object System.Collections.ArrayList
	foreach ($ACTUAL_OBJ in $LIST_OF_RESULTS_TO_COMPARE) {
        foreach ($STD_OBJ in $LIST_OF_STANDARD_VALUES) {
            if ($STD_OBJ.ConfigName -eq "is_percent_growth") {
                # Simple
                if($STD_OBJ.ValueType -eq "Simple") {
                    if([string]$ACTUAL_OBJ.is_percent_growth -ne [string]$STD_OBJ.StdValue) {
                        $NON_STANDARD_CONFIG_LIST.add($ACTUAL_OBJ) | Out-Null
                    } else {
                        $UPDATED_LIST_OF_RESULTS_TO_COMPARE.add($ACTUAL_OBJ) | Out-Null
                    }
                }
            }
        }
    }
    
    foreach ($ACTUAL_OBJ in $UPDATED_LIST_OF_RESULTS_TO_COMPARE) {
        foreach ($STD_OBJ in $LIST_OF_STANDARD_VALUES) {
            if ($STD_OBJ.ConfigName -eq "growth") {
                # Simple
                if($STD_OBJ.ValueType -eq "Simple") {
                    if($ACTUAL_OBJ.growth_in_MB -lt $STD_OBJ.StdValue) {
                        $NON_STANDARD_CONFIG_LIST.add($ACTUAL_OBJ) | Out-Null
                    }
                }
            }
        }
    }
	
	return $NON_STANDARD_CONFIG_LIST
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function scheduleJobHistoryRecord($PARAM=@{})
{
    $INSERT_ID = $PARAM.InsertId
    $RUN_START_TIME = $PARAM.RunStartTime
    $RUN_END_TIME = $PARAM.RunEndTime
    $RUN_STATUS = $PARAM.RunStatus
	$RUN_OUTPUT = $PARAM.RunOutput
	
    # -----------------------------------------------------------------------------
	if($param.query_type -eq "INSERT") {
		$queryString = "
		INSERT INTO [automation].[ScheduledJobsRunHistory]
			   ([ScheduledJobid]
			   ,[ServerName]
			   ,[InstanceName]
			   ,[DBName]
			   ,[RunStarttime]
			   ,[RunEndtime]
			   ,[RunStatus]
			   ,[RunOutput])
			VALUES
			   ('$SCHEDULED_JOB_ID'
			   ,'$SERVERNAME'
			   ,'$INSTANCENAME'
			   ,'NULL'
			   ,'$RUN_START_TIME'
			   ,'$RUN_START_TIME'
			   ,'$RUN_STATUS'
			   ,'NULL');
			   SELECT SCOPE_IDENTITY();
		"
	} elseif($param.query_type -eq "UPDATE") {
		$queryString=" 
		UPDATE [automation].[ScheduledJobsRunHistory] SET
			RunEndtime='$RUN_END_TIME', 
			RunStatus='$RUN_STATUS',
            RunOutput='$RUN_OUTPUT'
		WHERE Id='$INSERT_ID'; 
		SELECT SCOPE_IDENTITY();
		"
	}
	
	try {
        $Schedulejob_Run_History_Query_Output = invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
        $Schedulejob_Run_History_Query_Output_Json = @{output = $true; Success_Message = $Schedulejob_Run_History_Query_Output.Column1} | ConvertTo-Json
	} catch {
		$Schedulejob_Run_History_Query_Output_Json = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	
	return $Schedulejob_Run_History_Query_Output_Json
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function AlertRecord($PARAM=@{})
{
	$ALERT_STATUS = $PARAM.Status
    $ALERT_TYPE = $PARAM.Type
    $INFO = $PARAM.Info
	$DATABASE = $PARAM.Database
	
	$queryString = "
	INSERT INTO [automation].[Alert]
		   ([TypeId]
		   ,[CreationDate]
		   ,[StatusId]
		   ,[Info]
		   ,[Last_Modified_Date]
		   ,[ServerName]
		   ,[InstanceName]
		   ,[DBName])
		VALUES
		   ('$ALERT_TYPE'
		   ,getdate()
		   ,'$ALERT_STATUS'
		   ,'$INFO'
		   ,getdate()
		   ,'$SERVERNAME'
		   ,'$INSTANCENAME'
		   ,'$DATABASE');
		SELECT SCOPE_IDENTITY();
	"
	try {
		$Alert_Insert_Query_Output = (invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD").Column1
		$Alert_Insert_Query_Output_Json = @{output = $true; Success_Message = $Alert_Insert_Query_Output} | ConvertTo-Json
	} catch {
		$Alert_Insert_Query_Output_Json = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	
	return $Alert_Insert_Query_Output_Json
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function log($Message)
{
	#$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir\non-standard-db-config-check-output.log
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function mainFunction
{
	# ----------- Starting with Creating an Entry in ScheduledJobRunHistory Table -----------
	log("=========== Starting with Creating an Entry in ScheduledJobRunHistory Table ===========")
	
	$RUN_START_TIME = Get-Date -format "yyyy-MM-dd HH:mm:ss"

	$RUN_STATUS = "STARTED"
	
	$Schedule_Job_Run_History_Data = scheduleJobHistoryRecord(@{query_type="INSERT"; RunStartTime=$RUN_START_TIME; RunEndTime=$RUN_START_TIME; RunStatus=$RUN_STATUS; RunOutput="NULL"})

	$Schedule_Job_Run_History_Data_Parsed = $Schedule_Job_Run_History_Data | ConvertFrom-Json

	if($schedule_Job_Run_History_Data_Parsed.output -eq $false) {
		log($Schedule_Job_Run_History_Data_Parsed.Fail_Message)

	} elseif($Schedule_Job_Run_History_Data_Parsed.output -eq $true -And [String]::IsNullOrEmpty($Schedule_Job_Run_History_Data_Parsed.Success_Message) -eq $true) {
		log("ERROR: Schedule Job Eun History InsertID missing")
        Exit

	} else {
		$Schedule_Job_Run_History_InsertID = $Schedule_Job_Run_History_Data_Parsed.Success_Message
		log("ScheduledJobRunHistory Insert ID:	$Schedule_Job_Run_History_InsertID")
	}

	# ----------- Fetching the Database Configurations -----------
	log("=========== Fetching the Database Confiugrations ===========")

	$LIST_OF_RESULTS_TO_COMPARE = databaseConfig

	$JSON_PARSED_DATA = $LIST_OF_RESULTS_TO_COMPARE | ConvertFrom-Json

	if ($JSON_PARSED_DATA.output -eq $true) {
		# ----------- Checking For Non Standard Database Configurations -----------
		log("=========== Checking For Non Standard Database Configurations ===========")

		$NON_STD_DB_CONFIG_DATA = nonStandardDatabaseConfigCheck $JSON_PARSED_DATA.Actual_data $JSON_PARSED_DATA.Standard_data | ConvertTo-Json
		
		if($NON_STD_DB_CONFIG_DATA.Count -le 1) {
			$INFO = @"
{
		"NonStandardDatabaseConfig": [$NON_STD_DB_CONFIG_DATA]
}
"@
		} else {
			$INFO = @"
{
		"NonStandardDatabaseConfig": $NON_STD_DB_CONFIG_DATA
}
"@
		}
		
		if ([String]::IsNullOrEmpty($NON_STD_DB_CONFIG_DATA) -eq $false) {

			# ----------- Inserting Entry with Non Standard Database Configurations in Alert Table -----------
			log("=========== Inserting Entry with Non Standard Database Configurations in Alert Table ===========")
			$ALERT_ENTRY = AlertRecord(@{Status="CREATED"; Type="NON_STANDARD_DATABASE"; Info=$INFO; Database="NULL"})
            $ALERT_ENTRY_JSON = $ALERT_ENTRY | ConvertFrom-Json
            
            if ($ALERT_ENTRY_JSON.output -eq $false) {
                log("ERROR:   $ALERT_ENTRY_JSON")
                Exit
            } else {
                $ALERT_INSERT_ID = $ALERT_ENTRY_JSON.Success_Message
                log("Alert Enter Insert ID:   $ALERT_INSERT_ID")
            }
			
			# ----------- Sending an Email to Scheduled user with Non Standard Database Configurations -----------
			log("=========== Sending an Email with Non Standard Database Configurations ===========")
			sendEmailAlert $NON_STD_DB_CONFIG_DATA "NON_STANDARD_DB_ALERT"
		
		} else {
			log("=========== No Non Standard Database Configurations Found! ===========")
		}
		
		$RUN_OUTPUT = $INFO
		
		$RUN_STATUS = "COMPLETED"

	} elseif ($JSON_PARSED_DATA.output -eq $false) {
		log($JSON_PARSED_DATA.Fail_Message)
		
		$RUN_OUTPUT = $JSON_PARSED_DATA.Fail_Message
		
		$RUN_STATUS = "FAILED"
	}

	# ----------- Finishing with Creating an Entry in ScheduledJobRunHistory Table -----------
	log("=========== Finishing with Creating an Entry in ScheduledJobRunHistory Table ===========")

	$RUN_END_TIME = Get-Date -format "yyyy-MM-dd HH:mm:ss"

	$Schedule_Job_Run_History_Data = scheduleJobHistoryRecord(@{query_type="UPDATE"; InsertId=$Schedule_Job_Run_History_InsertID; RunStartTime=$RUN_START_TIME; RunEndTime=$RUN_END_TIME; RunStatus=$RUN_STATUS; RunOutput=$RUN_OUTPUT})

	$Schedule_Job_Run_History_Data_Parsed = $Schedule_Job_Run_History_Data | ConvertFrom-Json

	if($schedule_Job_Run_History_Data_Parsed.output -eq $false) {
		log($Schedule_Job_Run_History_Data_Parsed.Fail_Message)
	} else {
		log($Schedule_Job_Run_History_Data_Parsed.Success_Message)
	}
}
# -----------------------------------------------------------------------------

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

#Main Function
log("+++++++++++++ START STANDARD DATABASE CONFIG CHECK ++++++++++++++")
log("JOB INPUTS:")
log("SCHEDULED_JOB_ID:	$SCHEDULED_JOB_ID ")
log("SCHEDULED_USER:	$SCHEDULED_USER")
mainFunction
log("+++++++++++++ END STANDARD DATABASE CONFIG CHECK ++++++++++++++")