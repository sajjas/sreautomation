param(
	[String]$JOB_INPUT
)

# -----------------------------------------------------------------------------
#          Global Definition
# -----------------------------------------------------------------------------
$JSON_DATA = $JOB_INPUT | ConvertFrom-Json
$SHEDULEDTASKID = $JSON_DATA.scheduleTaskId

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

Function nonStandardInstanceConfigUpdate
{
    # ---------------------------------------------------------------------------------
	# fetching scheduledtask details
    $SCHEDULE_TASK_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "select ServerName, InstanceName, SNRefNo, TaskInfo, ScheduledUser from [automation].[ScheduledTasks] where Id = '$SHEDULEDTASKID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	
	$SERVERNAME = $SCHEDULE_TASK_DATA.ServerName
	$INSTANCENAME = $SCHEDULE_TASK_DATA.InstanceName
	$SCHEDULED_USER = $SCHEDULE_TASK_DATA.ScheduledUser
	$SNREFNO = $SCHEDULE_TASK_DATA.SNRefNo
    $TASK_INFO = $SCHEDULE_TASK_DATA.TaskInfo | ConvertFrom-Json
	
	log("SERVERNAME:	$SERVERNAME")
	log("INSTANCENAME:	$INSTANCENAME")
	log("SNREFNO:	$SNREFNO")
	log("SCHEDULED_USER:	$SCHEDULED_USER")
	log("TASK_INFO:	$TASK_INFO")
	
	foreach ($info in $TASK_INFO.NonStandardInstanceConfig) {
		$CONFIG_NAME = $info.name
        $CONFIG_STD_VALUE = $info.StdValue
        
        log("PROCESSING FOR ...")
        log("CONFIG_NAME:	$CONFIG_NAME")
		
		$QUERY = "EXEC sp_configure '$CONFIG_NAME', $CONFIG_STD_VALUE 
		RECONFIGURE;"
        
		log("QUERY:	$QUERY")
        
        try {
            $SP_CONFIG_OUTPUT = invoke-sqlcmd -serverInstance $INSTANCENAME -query $QUERY
        } catch {
            $SP_CONFIG_OUTPUT = $_.Exception.Message
        }
				
		log("SP_CONFIG_OUTPUT:	$SP_CONFIG_OUTPUT")
	}
}

# -----------------------------------------------------------------------------
function log($Message)
{
	#$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/update-standard-instance-output.log
}

# Main Function
log("+++++++++++++ START UPDATE STANDARD INSTANCE CONFIG TASK ++++++++++++++")
nonStandardInstanceConfigUpdate
log("+++++++++++++ END UPDATED STANDARD INSTANCE CONFIG TASK ++++++++++++++")