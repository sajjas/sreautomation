param(
	[String]$JOB_INPUT
)

$scriptDir = Get-Location

# -----------------------------------------------------------------------------
#          Global Definition
# -----------------------------------------------------------------------------
$SMTP = "smtp.ey.net"

$JSON_DATA = $JOB_INPUT | ConvertFrom-Json
$SHEDULEDTASKID = $JSON_DATA.scheduled_task_id
$ALERTID = $JSON_DATA.alertId

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------


# -----------------------------------------------------------------------------
# @function get_email_template:  a) pull data from givven email template name
#                                b) print to log the pulled db info
# @return:   - Dictionary which encapsules the EmailTemaplate data
#            - null in case of any error.
#
# @param TemplateName:   Name of Email Template same as stored in DB
#                        table "EYDBSRE.automation.emailTemplate"
function get_email_template([String]$TemplateName)
{
	# -----------------------------------------------------------------------------
	$QRY_GET_EMAIL_TEMPLATE = "SELECT subject,TopBody,BottomBody,[From],[To],[CC],[BCC],'Body' as Body FROM automation.emailTemplate WHERE TemplateName = @TemplateName"
	# -----------------------------------------------------------------------------

    log("  *********************************get_email_template starts ********************************************")
    $emailTemplateInstance = $null
    try {
        if ($null -ne $TemplateName) {
            $emailTemplateInstance = executeSelectQuery -select_qry $QRY_GET_EMAIL_TEMPLATE -parameters @{ TemplateName = $TemplateName }
            if ($null -ne $emailTemplateInstance -and $emailTemplateInstance) {
                log("Return Email Template Instance")
                foreach($itemEmail in $emailTemplateInstance.keys)
                {
                    $value =  $emailTemplateInstance.item($itemEmail)
                    log(" $itemEmail --> $value ")
                }
            }
        }
    }
    catch{
        log("------------ get_email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************get_email_template ends  ********************************************")
    return $emailTemplateInstance
}

# -----------------------------------------------------------------------------
# @function send_Email_template: a) Validate for required Fields
#                                b) Contenate Top, Middle and Bottom Body
#                                c) Create Dictionay with message Parameters
#                                d) Send Email
# @return:   False - if any error during email creation or email is not sent.
#            True  - Email sent
#
# @param emailTemplateInstance:  Dictionary which encapsules the EmailTemaplate
#                                Data, Body is 'Body' String as default.
# @TODO: It could be greate to have SMTP value as DB Lookup configuration.
function send_Email_template($emailTemplateInstance)
{
    log("  *********************************send_Email_template starts ********************************************")
    $returnStatus = $false
    try
    {
        if ($null -ne $emailTemplateInstance) {
            # -- Validate for required fields are in dicc starts
            $daRequiredFields = @('subject','TopBody','Body','BottomBody','From','To')
            $emailContinue = $true
            foreach( $itemRequired in $daRequiredFields)
            {
                $fieldKey = $itemRequired
                $fieldValue = $emailTemplateInstance.item("$fieldKey")
                if ($null -eq $fieldValue -and $fieldValue.Length -eq 0){
                    log("$fieldKey is missing")
                    $emailContinue = $false
                    break
                }
            }
            # -- Validate for required fields are in dicc ends
            #------------------------------------------------------------------
            # Continue only if required values are set
            if ($emailContinue)
            {
                # I know this is redundant but is the way how PS works   --- starts
                $subject = $emailTemplateInstance.subject
                $TopBody = $emailTemplateInstance.TopBody
                $Body = $emailTemplateInstance.Body
                $BottomBody = $emailTemplateInstance.BottomBody
                $From = $emailTemplateInstance.From
                $To = $emailTemplateInstance.To
                # I know this is redundant but is the way how PS works   --- ends
                #------------------------------------------------------------------
                # Create Body starts
                log(" Creating Body start ---------")
                $Full_Body = New-Object Collections.Generic.List[string]
                $Full_Body.add($TopBody)
                $Full_Body.add($Body)
                $Full_Body.add($BottomBody)
                log(" Email Body --> $Full_Body")
                log(" Creating Body ends ---------")
                # Create Body Ends
                #------------------------------------------------------------------
                # Assign Values to message dic starts
                $messageParameters = @{
                Subject = "$subject"
                Body = "$Full_Body"
                From = "$From"
                To = $To.split(',')
                SmtpServer = "$SMTP"   # This value is taken from global configuration variables
                }
                # Add CC to dictionary if not null
                if ($null -ne $emailTemplateInstance.CC -and $emailTemplateInstance.CC){
                    $CC = $emailTemplateInstance.CC
                    $messageParameters.Cc = $CC.split(',')
                }
                # Add BCC to dictionary if not null
                if ($null -ne $emailTemplateInstance.BCC -and $emailTemplateInstance.BCC){
                    $BCC = $emailTemplateInstance.BCC
                    $messageParameters.Bcc = $BCC.split(',')
                }
                # Assign Values to message dic ends
                #------------------------------------------------------------------
                Send-MailMessage @messageParameters -BodyAsHtml
                $returnStatus = $True
                log( "----> Email sent From: $From To: $To  using smtp server: $SMTP")
            }
        }
    }
    catch{
        log("------------ send_Email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************send_Email_template ends  ********************************************")
    return $returnStatus
}

# -----------------------------------------------------------------------------
function executeSelectQuery
{
    param ([String]$select_qry, $parameters = @{ })
    log( "<----------- Pulling Data from DB Starts -------->")
    if ($null -ne $select_qry -and $select_qry)
    {
        $query_final = $select_qry
        $dataset_cursor = New-Object System.Data.DataTable
        $sqlSelectConnection = getConnection
        $return_dic = $null
        if ($null -ne $sqlSelectConnection -and $sqlSelectConnection)
        {
            try
            {
                $sqlSelectConnection.Open()
                #SQL QUERY Execution
                $sqlCommand = New-Object System.Data.SqlClient.SqlCommand;
                $sqlCommand.Connection = $sqlSelectConnection;
                $sqlCommand.CommandText = $query_final;
                #SQL Parameters
                if ($null -ne $parameters -and $parameters)
                {
                    foreach ($p in $parameters.Keys)
                    {
                        [Void] $sqlCommand.Parameters.AddWithValue("@$p", $parameters[$p])
                        $param_text = $parameters[$p]
                        log("parameter $p  --> $param_text ")
                    }
                }
                #SQL Return Values
                $READER_DATA = $sqlCommand.ExecuteReader()
                $dataset_cursor.Load($READER_DATA)
                $sqlSelectConnection.Close()
                if ($null -ne $dataset_cursor)
                {
                    $return_dic = @{ }
                    foreach ($tupla in $dataset_cursor)
                    {
                        foreach ($columna in $dataset_cursor.Columns)
                        {
                            $value_x = $tupla.item($columna)
                            $columna_x = $columna
                            $return_dic.Add("$columna_x", "$value_x")
                            log("[$columna_x]=$value_x")
                        }
                    }
                }
            }
            catch
            {
                $return_dic = $null
                log($ERROR[0])
                log("Not Able to execute Select Query - [ $select_qry ]")
            }
        }

    }
    log( "<----------- Pulling Data from DB Ends -------->")
    return $return_dic
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function getConnection
{
    try
    {
        $connectionString = "server=$VAR_SERVERINSTANCE;database='$VAR_DATABASE';user id=$VAR_USERNAME;password=$VAR_PASSWORD";
        #SQL Connection - connection to SQL server
        $sqlConnection = new-object System.Data.SqlClient.SqlConnection;
        $sqlConnection.ConnectionString = $connectionString;
    }
    catch
    {
        log($ERROR[0])
        $sqlConnection = $null
    }
    return $sqlConnection
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function sendEmailAlert
{
	param(
		$data, 
		[string]$email_template_type
	)
	
	# -------------------------------------------------------------------------------------
	# Format data into HTML Table
	$TABLE_DATA_COLLECTION = tableData($data)
	
	# -------------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User before DB Backup Start.
	$emailTemplateInstance = get_email_template($email_template_type)
	$emailTemplateInstance.To = $SCHEDULED_USER_EMAIL
	$emailTemplateInstance.Body = $TABLE_DATA_COLLECTION
	
	send_Email_template($emailTemplateInstance)
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function tableData($data) 
{
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
	$new_data = $data | ConvertFrom-Json

	if ($new_data.psobject.Properties.name -Contains "ErrorMessage") {
		$htmlTableOutput = $new_data | select-object -property @{N='ServiceNow Ticket';E={$_.SNRefNo}}, @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, @{N='Configuration Name';E={$_.TaskInfo}}, @{N='Error Message';E={$_.ErrorMessage}} | ConvertTo-Html -Head $style
	} else {
		$htmlTableOutput = $new_data | select-object -property @{N='ServiceNow Ticket';E={$_.SNRefNo}}, @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, @{N='Configuration Name';E={$_.TaskInfo}} | ConvertTo-Html -Head $style
	}
	
	Add-Type -AssemblyName System.Web
	$tableOutput = [System.Web.HttpUtility]::HtmlDecode($htmlTableOutput)
	
	return $tableOutput
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function sp_query
{
    param(
        $INSTANCENAME,
        $QUERY   
    )

    invoke-sqlcmd -ServerInstance $INSTANCENAME -Query "EXEC sp_configure 'show advanced options', 1; RECONFIGURE;"

    invoke-sqlcmd -serverInstance $INSTANCENAME -query $QUERY
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function nonStandardInstanceConfigUpdate
{
	log("SCHEDULED_TASK_ID:	$SHEDULEDTASKID")
	log("ALERT_ID:	$ALERTID")

    # ---------------------------------------------------------------------------------
	# fetching scheduledtask details
	$SCHEDULE_TASK_DATA_QUERY = "select ServerName, InstanceName, SNRefNo, TaskInfo, ScheduledUser from [automation].[ScheduledTasks] where Id = '$SHEDULEDTASKID'"
    $SCHEDULE_TASK_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query $SCHEDULE_TASK_DATA_QUERY -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	
	$SERVERNAME = $SCHEDULE_TASK_DATA.ServerName
	$INSTANCENAME = $SCHEDULE_TASK_DATA.InstanceName
	$SCHEDULED_USER = $SCHEDULE_TASK_DATA.ScheduledUser
	$SNREFNO = $SCHEDULE_TASK_DATA.SNRefNo
    $TASK_INFO = $SCHEDULE_TASK_DATA.TaskInfo | ConvertFrom-Json

    # ---------------------------------------------------------------------------------
	# fetching User Email details
    $SCHEDULED_USER_EMAIL_QUERY = "select Email from [automation].[User] where UserName = '$SCHEDULED_USER'"
    $SCHEDULED_USER_EMAIL_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query $SCHEDULED_USER_EMAIL_QUERY -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	$SCHEDULED_USER_EMAIL = $SCHEDULED_USER_EMAIL_DATA.Email

    log("SCHEDULED_USER:	$SCHEDULED_USER")
	log("SCHEDULED_USER_EMAIL:	$SCHEDULED_USER_EMAIL")
	log("SERVERNAME:	$SERVERNAME")
	log("INSTANCENAME:	$INSTANCENAME")
	log("SNREFNO:	$SNREFNO")
	log("TASK_INFO:	$TASK_INFO")

	# ----------- Create Remote Session to DB Server/Jump Server -----------
	$remote_session = Create-PSSession -ServerName $SERVERNAME

    $TASK_INFO_LIST = $TASK_INFO.NonStandardInstanceConfig
    
    $ConfigName = $TASK_INFO_LIST.name

    $data = @{SNRefNo=$SNREFNO; ServerName=$SERVERNAME; InstanceName=$INSTANCENAME; ConfigName=$ConfigName; AlertId=$TASK_INFO.alertId} | ConvertTo-Json

    try {
        foreach ($info in $TASK_INFO_LIST) {
            if ($info.IsApply -eq $true -And $info.IsSuppressed -eq $false) {
	            $CONFIG_NAME = $info.name
	    		$CONFIG_STD_VALUE = $info.DefaultValue
	            
	            log("PROCESSING FOR ...")
	            log("CONFIG_NAME:	$CONFIG_NAME")
	            
	            $QUERY = "EXEC sp_configure '$CONFIG_NAME', $CONFIG_STD_VALUE RECONFIGURE;"
	            
	            log("QUERY:	$QUERY")
	            
	            if ($remote_session.Count -eq 1) {
	                $SP_CONFIG_OUTPUT = Invoke-Command -Session $remote_session -ScriptBlock {invoke-sqlcmd -serverInstance $using:INSTANCENAME -query $using:QUERY}
	            } elseif ($remote_session.Count -eq 2) {
	                $Jumpy_session = $remote_session[0]
	                $SP_CONFIG_OUTPUT = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:sp_query});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:INSTANCENAME, $using:QUERY))}
	            }
	            
	            log("SP_CONFIG_OUTPUT:	$SP_CONFIG_OUTPUT")
			} elseif ($info.IsApply -eq $false -And $info.IsSuppressed -eq $true) {
            	SuppressConfig($info)
        	}
        }

        if ([string]::IsNullOrEmpty($ALERTID) -eq $false) {
            # ---------------------------------------------------------------------------------
            # Update Alert Status
            log("Updating Alert Status")
            invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[Alert] SET StatusId='COMPLETED', Last_Modified_Date=getutcdate() WHERE Id='$ALERTID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
        }

        # ---------------------------------------------------------------------------------
        # Update Scheduled Task Status
        log("Updating Scheduled Task Status")
        invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[ScheduledTasks] SET StatusId='COMPLETED', Last_Modified_Date=getutcdate(), ReturnInfo='Updating Non Standard DB Config Completed Successfully' WHERE Id='$SHEDULEDTASKID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
        
        # ---------------------------------------------------------------------------------
        # Sending Email Notification to Scheduled User after Updating Instance Configurations.
        sendEmailAlert $data "INSTANCE_CONFIG_TASK_SUCCESS"

        if ([string]::IsNullOrEmpty($remote_session) -eq $false) {
            # ---------------------------------------------------------------------------------
            # Remove Remote Session
            Remove-RemoteSession $remote_session $Jumpy_session
        }

    } catch {
        $Error_Output = $_.Exception.Message
        log("Failed to Update Non Standard Instance Configurations Due to :   $Error_Output")
        
        # ---------------------------------------------------------------------------------
        # send email and return incase of Failure
		BackupFailedEmail($error_output)
        
        if ([string]::IsNullOrEmpty($remote_session) -eq $false) {
            # ---------------------------------------------------------------------------------
            # Remove Remote Session
            Remove-RemoteSession $remote_session $Jumpy_session
        }
    }
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function BackupFailedEmail($ERROR_OUTPUT)
{
    if ([string]::IsNullOrEmpty($ALERTID) -eq $false) {
	    # ---------------------------------------------------------------------------------
	    # Update Alert Status
        log("Updating Alert Status")
	    invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[Alert] SET StatusId='FAILED', Last_Modified_Date=getutcdate() WHERE Id='$ALERTID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD  
	}

    # ---------------------------------------------------------------------------------
	# Update ScheduledTask Status
    log("Updating Scheduled Task Status")
	invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[ScheduledTasks] SET StatusId='FAILED', ReturnInfo='$ERROR_OUTPUT', Last_Modified_Date=getutcdate() WHERE Id='$SHEDULEDTASKID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	 
	# ---------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User If Update Std DB Config Failed.
	$NewData = $data | ConvertFrom-Json
	$NewData | add-member -Name "ErrorMessage" -value "Error Occured! $ERROR_OUTPUT" -MemberType NoteProperty
	$data = $NewData | ConvertTo-Json
	sendEmailAlert $data "INSTANCE_CONFIG_TASK_FAILURE"
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function SuppressConfig ($info) 
{
    $CONFIG_ID = $info.ConfigId
    $CONFIG_NAME = $info.name
    
    log("SUPPRESS CONFIG PROCESSING FOR ...")
    log("CONFIG ID:	$CONFIG_ID")
    log("CONFIG NAME:	$CONFIG_NAME")
    
    # -----------------------------------------------------------------------------
    $QRY_GET_ALERT_SUPPRESSION_INFO = "SELECT Id FROM automation.AlertSuppression WHERE AlertType='NON_STANDARD_INSTANCE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME'"
    # -----------------------------------------------------------------------------

    log($QRY_GET_ALERT_SUPPRESSION_INFO)

    $ALERT_SUPPRESSION_ID = executeSelectQuery -select_qry $QRY_GET_ALERT_SUPPRESSION_INFO -parameters @{}
    
    if ($ALERT_SUPPRESSION_ID.Count -eq 0) {
        $queryString = "
        INSERT INTO [automation].[AlertSuppression]
            ([AlertType]
            ,[ServerName]
            ,[InstanceName]
            ,[DBName]
            ,[ConfigKeys]
            ,[CreationDate])
            VALUES
                ('NON_STANDARD_INSTANCE'
                ,'$SERVERNAME'
                ,'$INSTANCENAME'
                ,''
                ,'$CONFIG_ID'
                ,getutcdate());
            SELECT SCOPE_IDENTITY();
        "
        
        $ALERT_SUPPRESSION_ID = (invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD").Column1
        log("Alert Suppression Entry Created in the AlertSuppression Table With ID:  $ALERT_SUPPRESSION_ID")
    } else {
        $ALERT_SUPPRESSION_ID = $ALERT_SUPPRESSION_ID.Id

        # -----------------------------------------------------------------------------
        $QRY_GET_CONFIG_KEYS = "SELECT ConfigKeys FROM automation.AlertSuppression WHERE Id=$ALERT_SUPPRESSION_ID"
        # -----------------------------------------------------------------------------

        log($QRY_GET_CONFIG_KEYS)

        $ALERT_SUPPRESSION_CONFIG_KEYS = (invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$QRY_GET_CONFIG_KEYS" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD").Configkeys
        
        $SPLIT_KEYS = $ALERT_SUPPRESSION_CONFIG_KEYS.split(",")

        if ($SPLIT_KEYS -notcontains $CONFIG_ID) {
            $SPLIT_KEYS += $CONFIG_ID
            $UPDATED_CONFIG_KEYS = $SPLIT_KEYS -join ","

            $queryString = "
            UPDATE [automation].[AlertSuppression] 
            SET ConfigKeys= '$UPDATED_CONFIG_KEYS', CreationDate= getutcdate()
            WHERE Id= $ALERT_SUPPRESSION_ID
            "
          
            invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
            log("Alert Suppression Entry Update in the AlertSuppression Table With ID:  $ALERT_SUPPRESSION_ID")
        }
    }
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function Remove-RemoteSession 
{
	param (
        $remote_session,
        $Jumpy_session
    )

    # ----------- Remove Remote Session/s -----------
	if ($remote_session.Count -eq 1) {
		Remove-PSSession $remote_session
	} elseif ($remote_session.Count -eq 2) {
		Invoke-Command -Session $Jumpy_session -ScriptBlock {Remove-PSSession $(Get-PSSession)}
		Remove-PSSession $Jumpy_session
	}
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function Create-PSSession {

[CmdletBinding()]
Param (
    [Parameter(Mandatory=$True)]
    [string]$ServerName
)

$Session_Jumpy = $null

    If ($ServerName -match "ey.net" -and $ServerName -notmatch "cloudapp"){
        $InventAccount = "EY\P.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
    } Elseif ($ServerName -match "eydmz.net"){
        $Session_Jumpy = JumpServer
		$InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AGEAUgBEAEQAQgBDAHUAaQA0ADQAagBHAHUAZAAvAHkASgAzAG0AUgBKAHcAPQA9AHwAMAA5AGYAZQAxADkAYQA3ADMAYgBhAGQANQAyADUAMgA4ADcANgAwAGIAYgBhADQAMwAwAGQAYgAyAGMANgBjADUAZgBjADIAZQBiAGMAYgA1ADMANgBjAGQANQBlADAAYgA0ADUANgBlAGYAYgA4AGIANwAyADMAOAA5ADkAOAA=='
    } Elseif ($ServerName -match "eyxstaging.net"){
		$Session_Jumpy = JumpServer
		$InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoAWgByAHYAMAB4AFkAUQBVAFAAQgBXAEEAUgA5ADAAawBmAEQAbAB5AEEAPQA9AHwAMgA4ADkAZgBmAGEANQA3AGUANwAwADQAZAA3ADUAYQBkADMAZgAwADMANQBmADQAYwA1AGQAZgA0ADIAMwA0ADgAYwA3AGYANQBkAGUAMwA1ADUANgA3ADYAYgBhAGQAOQAxADcAMgBmAGQAOAA0ADkAMABhAGIAYgBkADUAYQA='
	} Elseif ($ServerName -match "eyua.net"){
        $InventAccount = "EYUA\U.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHIAZgBzAGkAOABxADUAOQBmAGIAVwB4AFcAKwBHAEEANQA4ADYANQBpAGcAPQA9AHwAMQBkADUAOAA3ADEAYwA2AGYANwA3AGIANwA2AGUANQAwADUAYQA2AGYAZQBkADAAZgAwAGEAMQA5ADAAMQBkADMAYQAwADcAYwAyAGIAZQAwADUAMgBiADYAMQBjAGYAMwA5ADcAMAA2ADUAMgBjADYAMwA5ADkAYgA0ADUAOQA='
    } Elseif ($ServerName -match "eyqa.net"){
        $InventAccount = "EYQA\Q.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHcAdgA3AFcAcQBIAEYAUQAwAHoAUABNAGkAZAB4AHYANQBoAFcAVABWAHcAPQA9AHwANQAxADYAYwBiADcAOABmADMAYQAzADUAMgBlAGYANgBmAGUAMwA0ADEAMwA0AGMANwBkADcAYgA2ADMAZgAxADEAYQA4AGIAMQAxADcANwA2ADcAYwAwADYAMABkADQANwAyADcAMwA1ADMAYQA1ADYAZABlADgAMABmAGIAZQA='
    } Elseif ($ServerName -match "eydev.net"){
        $InventAccount = "EYDEV\D.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEYANABNAFcAbgBTAHAAUQBMAGYAYgAxAGgASAAyAGUAQgBtAGYAZQBTAFEAPQA9AHwAMwBlAGEAZQA1AGYAMgBlAGUANABkADQAOABlAGYAMwBlAGMAMABhADYAMAAzADkAMgBjADgAYQAxADQAMgA0AGEAMQA4ADQAZQA4AGMAMAAzADgAMgA5AGIANwBmADEAYgA0ADkAZgBmADIAYgA3AGEAYQBhAGYAZABhADIANwA='
    } Elseif ($ServerName -match "ey.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPP.NET\C.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEEAaQBJADUAMgA1AEMAVABXAC8AbQBSAHgAWgBaAHgAUABoAHIAVwA0AEEAPQA9AHwAZAAwAGYAOQAzAGUAMwAzADIAMwA2AGYAZgA4ADUAOABlADkANQBkADcANwAzAGYAOQBiADMAMAA2AGIANwBkADMAZABiADIAOAAyAGMANgBhADEAYgA5ADQAZAAwAGYAYQA4ADMAMgAyADAANwA1ADQAZQA4ADkAOAAxADAAMAA='
    } Elseif ($ServerName -match "eydev.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AFQAYgBWAGMANwBWAHQAbQBRAHAATgA0AHIAQQBUAEIAbQBiAG4AbgBtAEEAPQA9AHwAMgA5AGYAZgA3ADUAOQAwAGEAZAAwADYAYQA2ADQAZgAxAGMAYgBjADIANQBmAGEAYQAxADcAZgBmAGQAOQBkADYAZAAzAGYAOAAxADEAOAA4ADgAMAAyAGUANABkADAAYgBkADgANQBhAGEANwA1AGYAYgBlADIAMAAwADEAZAA='
    } Else {
        If ($ServerName -match '^[A-Z]{6,7}[Pp]' -and ($($ServerName.Substring(0,2)) -notmatch "^AC" -and $ServerName -notmatch ".cloudapp.ey.net$")){
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Zz]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
			$Session_Jumpy = JumpServer
            $InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AGEAUgBEAEQAQgBDAHUAaQA0ADQAagBHAHUAZAAvAHkASgAzAG0AUgBKAHcAPQA9AHwAMAA5AGYAZQAxADkAYQA3ADMAYgBhAGQANQAyADUAMgA4ADcANgAwAGIAYgBhADQAMwAwAGQAYgAyAGMANgBjADUAZgBjADIAZQBiAGMAYgA1ADMANgBjAGQANQBlADAAYgA0ADUANgBlAGYAYgA4AGIANwAyADMAOAA5ADkAOAA=='
            If ($ServerName -notmatch 'eydmz.net$'){
                $ServerName += ".eydmz.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Xx]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $Session_Jumpy = JumpServer
			$InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoAWgByAHYAMAB4AFkAUQBVAFAAQgBXAEEAUgA5ADAAawBmAEQAbAB5AEEAPQA9AHwAMgA4ADkAZgBmAGEANQA3AGUANwAwADQAZAA3ADUAYQBkADMAZgAwADMANQBmADQAYwA1AGQAZgA0ADIAMwA0ADgAYwA3AGYANQBkAGUAMwA1ADUANgA3ADYAYgBhAGQAOQAxADcAMgBmAGQAOAA0ADkAMABhAGIAYgBkADUAYQA='
            If ($ServerName -notmatch 'eyxstaging.net$'){
                $ServerName += ".eyxstaging.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Uu]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYUA\U.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHIAZgBzAGkAOABxADUAOQBmAGIAVwB4AFcAKwBHAEEANQA4ADYANQBpAGcAPQA9AHwAMQBkADUAOAA3ADEAYwA2AGYANwA3AGIANwA2AGUANQAwADUAYQA2AGYAZQBkADAAZgAwAGEAMQA5ADAAMQBkADMAYQAwADcAYwAyAGIAZQAwADUAMgBiADYAMQBjAGYAMwA5ADcAMAA2ADUAMgBjADYAMwA5ADkAYgA0ADUAOQA='
            If ($ServerName -notmatch 'eyua.net$'){
                $ServerName += ".eyua.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Qq]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYQA\Q.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHcAdgA3AFcAcQBIAEYAUQAwAHoAUABNAGkAZAB4AHYANQBoAFcAVABWAHcAPQA9AHwANQAxADYAYwBiADcAOABmADMAYQAzADUAMgBlAGYANgBmAGUAMwA0ADEAMwA0AGMANwBkADcAYgA2ADMAZgAxADEAYQA4AGIAMQAxADcANwA2ADcAYwAwADYAMABkADQANwAyADcAMwA1ADMAYQA1ADYAZABlADgAMABmAGIAZQA='
            If ($ServerName -notmatch 'eyqa.net$'){
                $ServerName += ".eyqa.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYDEV\D.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEYANABNAFcAbgBTAHAAUQBMAGYAYgAxAGgASAAyAGUAQgBtAGYAZQBTAFEAPQA9AHwAMwBlAGEAZQA1AGYAMgBlAGUANABkADQAOABlAGYAMwBlAGMAMABhADYAMAAzADkAMgBjADgAYQAxADQAMgA0AGEAMQA4ADQAZQA4AGMAMAAzADgAMgA5AGIANwBmADEAYgA0ADkAZgBmADIAYgA3AGEAYQBhAGYAZABhADIANwA='
            If ($ServerName -notmatch 'eydev.net$'){
                $ServerName += ".eydev.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Pp]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPP.NET\C.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEEAaQBJADUAMgA1AEMAVABXAC8AbQBSAHgAWgBaAHgAUABoAHIAVwA0AEEAPQA9AHwAZAAwAGYAOQAzAGUAMwAzADIAMwA2AGYAZgA4ADUAOABlADkANQBkADcANwAzAGYAOQBiADMAMAA2AGIANwBkADMAZABiADIAOAAyAGMANgBhADEAYgA5ADQAZAAwAGYAYQA4ADMAMgAyADAANwA1ADQAZQA4ADkAOAAxADAAMAA='
            If ($ServerName -notmatch 'cloudapp.ey.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AFQAYgBWAGMANwBWAHQAbQBRAHAATgA0AHIAQQBUAEIAbQBiAG4AbgBtAEEAPQA9AHwAMgA5AGYAZgA3ADUAOQAwAGEAZAAwADYAYQA2ADQAZgAxAGMAYgBjADIANQBmAGEAYQAxADcAZgBmAGQAOQBkADYAZAAzAGYAOAAxADEAOAA4ADgAMAAyAGUANABkADAAYgBkADgANQBhAGEANwA1AGYAYgBlADIAMAAwADEAZAA='
            If ($ServerName -notmatch 'cloudapp.eydev.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Else {
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        }
    }
    If ($InventAccount -ne $null) {
		# retrieve the password.
		[Byte[]] $key = (1..16)  	
        $SecurePassword = ConvertTo-SecureString $DBAToolPassword -Key $key
        $InventCredential = New-Object System.Management.Automation.PSCredential ($InventAccount, $SecurePassword)
        $RemoteSessionOption = New-PSSessionOption -SkipCACheck -OpenTimeout 180000 -IdleTimeout 180000 #3 minutes
        if ($Session_Jumpy -ne $null) {
			$Session = Invoke-Command -Session $Session_Jumpy -ScriptBlock {New-PSSession -ComputerName $using:ServerName -Credential $using:InventCredential -SessionOption $using:RemoteSessionOption -ErrorAction Stop}
			return $Session_Jumpy, $Session.Name
		} else {
			$Session = New-PSSession -ComputerName $ServerName -Credential $InventCredential -SessionOption $RemoteSessionOption -ErrorAction Stop
			return $Session
		}
    }
}

Function JumpServer() {
	$InventAccount_Jumpy = "EY\P.SMOO.SQL"
	$DBAToolPassword_Jumpy = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
    [Byte[]] $key = (1..16)  	
    $SecurePassword_Jumpy = ConvertTo-SecureString $DBAToolPassword_Jumpy -Key $key
	$InventCredential_Jumpy = New-Object System.Management.Automation.PSCredential ($InventAccount_Jumpy, $SecurePassword_Jumpy)
	$RemoteSessionOption_Jumpy = New-PSSessionOption -SkipCACheck -OpenTimeout 180000 -IdleTimeout 180000 #3 minutes
	$Session_Jumpy = New-PSSession -ComputerName "USSECVMPDBTSQ01.ey.net" -Credential $InventCredential_Jumpy -SessionOption $RemoteSessionOption_Jumpy -ErrorAction Stop
	return $Session_Jumpy
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function log($Message)
{
	Push-Location -Path $scriptDir
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/non-standard-instance-update-output.log
}
# -----------------------------------------------------------------------------

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# Main Function
log("+++++++++++++ START UPDATE STANDARD INSTANCE CONFIG TASK ++++++++++++++")
nonStandardInstanceConfigUpdate
log("+++++++++++++ END UPDATED STANDARD INSTANCE CONFIG TASK ++++++++++++++")

#.\get_UpdateStandardInstanceConfig_Local.ps1 -JOB_INPUT '{"scheduled_task_id":"407","alertId":"4609"}'