$scriptDir = Get-Location

# -----------------------------------------------------------------------------
#          Global Lists Definition
# -----------------------------------------------------------------------------
$SMTP = "smtp.ey.net"

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# -----------------------------------------------------------------------------
# @function get_email_template:  a) pull data from givven email template name
#                                b) print to log the pulled db info
# @return:   - Dictionary which encapsules the EmailTemaplate data
#            - null in case of any error.
#
# @param TemplateName:   Name of Email Template same as stored in DB
#                        table "EYDBSRE.automation.emailTemplate"
function get_email_template([String]$TemplateName)
{
	# -----------------------------------------------------------------------------
	$QRY_GET_EMAIL_TEMPLATE = "SELECT subject,TopBody,BottomBody,[From],[To],[CC],[BCC],'Body' as Body FROM automation.emailTemplate WHERE TemplateName = @TemplateName"
	# -----------------------------------------------------------------------------

    log("  *********************************get_email_template starts ********************************************")
    $emailTemplateInstance = $null
    try {
        if ($null -ne $TemplateName) {
            $emailTemplateInstance = executeSelectQuery -select_qry $QRY_GET_EMAIL_TEMPLATE -parameters @{ TemplateName = $TemplateName }
            if ($null -ne $emailTemplateInstance -and $emailTemplateInstance) {
                log("Return Email Template Instance")
                foreach($itemEmail in $emailTemplateInstance.keys)
                {
                    $value =  $emailTemplateInstance.item($itemEmail)
                    log(" $itemEmail --> $value ")
                }
            }
        }
    }
    catch{
        log("------------ get_email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************get_email_template ends  ********************************************")
    return $emailTemplateInstance
}

# -----------------------------------------------------------------------------
# @function send_Email_template: a) Validate for required Fields
#                                b) Contenate Top, Middle and Bottom Body
#                                c) Create Dictionay with message Parameters
#                                d) Send Email
# @return:   False - if any error during email creation or email is not sent.
#            True  - Email sent
#
# @param emailTemplateInstance:  Dictionary which encapsules the EmailTemaplate
#                                Data, Body is 'Body' String as default.
# @TODO: It could be greate to have SMTP value as DB Lookup configuration.
function send_Email_template($emailTemplateInstance)
{
    log("  *********************************send_Email_template starts ********************************************")
    $returnStatus = $false
    try
    {
        if ($null -ne $emailTemplateInstance) {
            # -- Validate for required fields are in dicc starts
            $daRequiredFields = @('subject','TopBody','Body','BottomBody','From','To')
            $emailContinue = $true
            foreach( $itemRequired in $daRequiredFields)
            {
                $fieldKey = $itemRequired
                $fieldValue = $emailTemplateInstance.item("$fieldKey")
                if ($null -eq $fieldValue -and $fieldValue.Length -eq 0){
                    log("$fieldKey is missing")
                    $emailContinue = $false
                    break
                }
            }
            # -- Validate for required fields are in dicc ends
            #------------------------------------------------------------------
            # Continue only if required values are set
            if ($emailContinue)
            {
                # I know this is redundant but is the way how PS works   --- starts
                $subject = $emailTemplateInstance.subject
                $TopBody = $emailTemplateInstance.TopBody
                $Body = $emailTemplateInstance.Body
                $BottomBody = $emailTemplateInstance.BottomBody
                $From = $emailTemplateInstance.From
                $To = $emailTemplateInstance.To
                # I know this is redundant but is the way how PS works   --- ends
                #------------------------------------------------------------------
                # Create Body starts
                log(" Creating Body start ---------")
                $Full_Body = New-Object Collections.Generic.List[string]
                $Full_Body.add($TopBody)
                $Full_Body.add($Body)
                $Full_Body.add($BottomBody)
                log(" Email Body --> $Full_Body")
                log(" Creating Body ends ---------")
                # Create Body Ends
                #------------------------------------------------------------------
                # Assign Values to message dic starts
                $messageParameters = @{
                Subject = "$subject"
                Body = "$Full_Body"
                From = "$From"
                To = $To.split(',')
                SmtpServer = "$SMTP"   # This value is taken from global configuration variables
                }
                # Add CC to dictionary if not null
                if ($null -ne $emailTemplateInstance.CC -and $emailTemplateInstance.CC){
                    $CC = $emailTemplateInstance.CC
                    $messageParameters.Cc = $CC.split(',')
                }
                # Add BCC to dictionary if not null
                if ($null -ne $emailTemplateInstance.BCC -and $emailTemplateInstance.BCC){
                    $BCC = $emailTemplateInstance.BCC
                    $messageParameters.Bcc = $BCC.split(',')
                }
                # Assign Values to message dic ends
                #------------------------------------------------------------------
                Send-MailMessage @messageParameters -BodyAsHtml
                $returnStatus = $True
                log( "----> Email sent From: $From To: $To  using smtp server: $SMTP")
            }
        }
    }
    catch{
        log("------------ send_Email_template  FAILED -------------- ")
        log("$ERROR[0]")
    }
    log("  *********************************send_Email_template ends  ********************************************")
    return $returnStatus
}

# -----------------------------------------------------------------------------
function executeSelectQuery
{
    param ([String]$select_qry, $parameters = @{ })
    log( "<----------- Pulling Data from DB Starts -------->")
    if ($null -ne $select_qry -and $select_qry)
    {
        $query_final = $select_qry
        $dataset_cursor = New-Object System.Data.DataTable
        $sqlSelectConnection = getConnection
        $return_dic = $null
        if ($null -ne $sqlSelectConnection -and $sqlSelectConnection)
        {
            try
            {
                $sqlSelectConnection.Open()
                #SQL QUERY Execution
                $sqlCommand = New-Object System.Data.SqlClient.SqlCommand;
                $sqlCommand.Connection = $sqlSelectConnection;
                $sqlCommand.CommandText = $query_final;
                #SQL Parameters
                if ($null -ne $parameters -and $parameters)
                {
                    foreach ($p in $parameters.Keys)
                    {
                        [Void] $sqlCommand.Parameters.AddWithValue("@$p", $parameters[$p])
                        $param_text = $parameters[$p]
                        log("parameter $p  --> $param_text ")
                    }
                }
                #SQL Return Values
                $READER_DATA = $sqlCommand.ExecuteReader()
                $dataset_cursor.Load($READER_DATA)
                $sqlSelectConnection.Close()
                if ($null -ne $dataset_cursor)
                {
                    $return_dic = @{ }
                    foreach ($tupla in $dataset_cursor)
                    {
                        foreach ($columna in $dataset_cursor.Columns)
                        {
                            $value_x = $tupla.item($columna)
                            $columna_x = $columna
                            $return_dic.Add("$columna_x", "$value_x")
                            log("[$columna_x]=$value_x")
                        }
                    }
                }
            }
            catch
            {
                $return_dic = $null
                log($ERROR[0])
                log("Not Able to execute Select Query - [ $select_qry ]")
            }
        }

    }
    log( "<----------- Pulling Data from DB Ends -------->")
    return $return_dic
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function getConnection
{
    try
    {
        $connectionString = "server=$VAR_SERVERINSTANCE;database='$VAR_DATABASE';user id=$VAR_USERNAME;password=$VAR_PASSWORD";
        #SQL Connection - connection to SQL server
        $sqlConnection = new-object System.Data.SqlClient.SqlConnection;
        $sqlConnection.ConnectionString = $connectionString;
    }
    catch
    {
        log($ERROR[0])
        $sqlConnection = $null
    }
    return $sqlConnection
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function sendEmailAlert
{
    param(
		$data, 
		[string]$email_template_type
	)

	# -------------------------------------------------------------------------------------
	# Format data into HTML Table
	$TABLE_DATA_COLLECTION = tableData($data)
	
	# -------------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User before DB Backup Start.
	$emailTemplateInstance = get_email_template($email_template_type)
	$emailTemplateInstance.TopBody = $emailTemplateInstance.TopBody.replace("REMINDER_MINS", $REMINDERMINS)
	$emailTemplateInstance.To = $TO_ADDR
	$emailTemplateInstance.Body = $TABLE_DATA_COLLECTION
	send_Email_template($emailTemplateInstance)
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function tableData($data) {
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
	$new_data = $data | ConvertFrom-Json

	if ($new_data.PSobject.Properties.Name -contains "DBName") {
		$htmlTableOutput = $new_data | select-object -property @{N='ServiceNow Ticket';E={$_.SNRefNo}}, @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, DBName, @{N='Scheduled Time(in GMT)';E={$_.ScheduledTime}} | ConvertTo-Html -Head $style
	} else {
		$htmlTableOutput = $new_data | select-object -property @{N='ServiceNow Ticket';E={$_.SNRefNo}}, @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, @{N='Scheduled Time(in GMT)';E={$_.ScheduledTime}} | ConvertTo-Html -Head $style
	}

	Add-Type -AssemblyName System.Web
    $tableOutput = [System.Web.HttpUtility]::HtmlDecode($htmlTableOutput)

	return $tableOutput
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# About the query
# For Example,
# If schedule time at 7:50 - (ReminderMins 15 + 15) = 7:20 <= 7:10 (if trigger at 7:10) - No Action
# If schedule time at 7:50 - (ReminderMins 15 + 15) = 7:20 <= 7:25 (if trigger at 7:25) - Trigger Action
function fetchReminderScheduledTaskInfo {
	log("======== START FETCHING TASK LIST FOR EMAIL REMINDER ========")
    try {
		$taskListForEmailReminders = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "select Id, ProcessId, ServerName, InstanceName, DBName, ReminderEmail, ScheduledTime, ReminderMins, ReminderCount, SNRefNo from [automation].[ScheduledTasks] where StatusId = 'SCHEDULED' And ReminderCount = 0 And DATEADD(MINUTE, -(ReminderMins + 15) ,ScheduledTime) <= (CONVERT(datetime, DATEADD(minute, DATEDIFF(minute, GETDATE(), GETUTCDATE()), GETDATE())))" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	} catch {
		log("ERROR:	Failed to Fetch the List of scheduledTasks to send Reminder with $_.Exception.Message")
	}
	
	if ([String]::IsNullOrEmpty($taskListForEmailReminders) -ne $true) {
		foreach ($taskItem in $taskListForEmailReminders) {
			log("PROCESSING TASK LIST ITEM")
			# ---------------------------------------------------------------------------------
			$PROCESSID = $taskItem.ProcessId
			$TASKID = $taskItem.Id
			$SERVERNAME = $taskItem.ServerName
			$INSTANCENAME = $taskItem.InstanceName
			$SCHEDULETIME = $taskItem.ScheduledTime
			$SNREFNO = $taskItem.SNRefNo
			$REMINDERMINS = $taskItem.ReminderMins
			$INC_REMINDERCOUNT = $taskItem.ReminderCount + 1
			# ---------------------------------------------------------------------------------
			$PROCESS_ACTION = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "select Action from [automation].[Process] where Id = $PROCESSID" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
			
			$PROCESSACTION = $PROCESS_ACTION.Action
			if ($PROCESSACTION -eq "BACKUP_NOW" -Or $PROCESSACTION -eq "SCHEDULE_BACKUP") {
				$DBNAME = $taskItem.DBName
				$PROCESSACTION = "DB_BACKUP_TASK_REMINDER"
				$data = @{SNRefNo = $SNREFNO; ServerName= $SERVERNAME; InstanceName= $INSTANCENAME; DBName= $DBNAME; ScheduledTime= $SCHEDULETIME | Out-String} | ConvertTo-Json
			} elseif ($PROCESSACTION -eq "SCHEDULE_REBALANCE" -Or $PROCESSACTION -eq "REBALANCE_NOW") {
				$PROCESSACTION = "CLUSTER_BALANCE_TASK_REMINDER"
				$data = @{SNRefNo = $SNREFNO; ServerName= $SERVERNAME; InstanceName= $INSTANCENAME; ScheduledTime= $SCHEDULETIME | Out-String} | ConvertTo-Json
			}
			
			log("SNREFNO:	$SNREFNO")
			log("PROCESSID:	$PROCESSID")
			log("TASKID:	$TASKID")
			log("SERVERNAME:	$SERVERNAME")
			log("INSTANCENAME:	$INSTANCENAME")
			log("DBNAME:	$DBNAME")
			log("SCHEDULEDTIME:	$SCHEDULETIME")
			log("REMINDERMINS:	$REMINDERMINS")
			log("PROCESSACTION:	$PROCESSACTION")
			
			# ---------------------------------------------------------------------------------
			log("======== START SENDING EMAIL ========")
			$TO_ADDR = $taskItem.ReminderEmail
			sendEmailAlert $data $PROCESSACTION
			log("======== END SENDING EMAIL ========")
			
			# ---------------------------------------------------------------------------------
			log("======== UPDATING SCHEDULED TASK TABLE WITH REMINDERCOUNT TO 1 ========")
			try {
				invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[ScheduledTasks] SET ReminderCount = $INC_REMINDERCOUNT WHERE Id = $TASKID" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
			} catch {
				log("ERROR:	Updating scheduledTask Entry Failed with $_.Exception.Message")
			}
		}
	} else {
		log("TASK LIST FOR EMAIL REMINDER --> [EMPTY]")
	}
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function log ($Message)
{
    Push-Location -Path $scriptDir
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/reminder-task-output.log
}
# -----------------------------------------------------------------------------

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# -----------------------------------------------------------------------------
function createReminderScheduledTask {
	$InventAccount = "EY\P.SMOO.SQL"
	$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
    
	[Byte[]] $key = (1..16)  	
    $SecurePassword = ConvertTo-SecureString $DBAToolPassword -Key $key
	$InventCredential = New-Object System.Management.Automation.PSCredential ($InventAccount, $SecurePassword)
	$Password = $InventCredential.GetNetworkCredential().Password
	
	$action = New-ScheduledTaskAction -Execute 'Powershell.exe' -Argument '"C:\DBSRE\jobs\scheduledTaskReminder.ps1"'
	$trigger = New-ScheduledTaskTrigger `
		-Once `
		-At (Get-Date) `
		-RepetitionInterval (New-TimeSpan -Minutes 15) `
		-RepetitionDuration (New-TimeSpan -Days (365 * 23))
	Register-ScheduledTask -Action $action -Trigger $trigger -TaskPath "\DBSRE" -TaskName "DBSRE-ScheduledTask-Reminder" -Description "DBSRE-ScheduledTask-Reminder" -User "$InventAccount" -Password "$Password" -RunLevel Highest
}
# -----------------------------------------------------------------------------

# Main Function
log("+++++++++++++ START SCHEDULED TASK REMINDER ++++++++++++++")
fetchReminderScheduledTaskInfo
log("+++++++++++++ END SCHEDULED TASK REMINDER ++++++++++++++")