param(
	[String]$JOB_INPUT
)

# -----------------------------------------------------------------------------
#          Global Definition
# -----------------------------------------------------------------------------
$JSON_DATA = $JOB_INPUT | ConvertFrom-Json
$SHEDULEDTASKID = $JSON_DATA.scheduleTaskId

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

Function nonStandardDatabaseConfigUpdate
{
    # ---------------------------------------------------------------------------------
	# fetching scheduledtask details
    $SCHEDULE_TASK_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "select ServerName, InstanceName, SNRefNo, TaskInfo, ScheduledUser from [automation].[ScheduledTasks] where Id = '$SHEDULEDTASKID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	
	$SERVERNAME = $SCHEDULE_TASK_DATA.ServerName
	$INSTANCENAME = $SCHEDULE_TASK_DATA.InstanceName
	$SCHEDULED_USER = $SCHEDULE_TASK_DATA.ScheduledUser
	$SNREFNO = $SCHEDULE_TASK_DATA.SNRefNo
    $TASK_INFO = $SCHEDULE_TASK_DATA.TaskInfo | ConvertFrom-Json
	
	log("SERVERNAME:	$SERVERNAME")
	log("INSTANCENAME:	$INSTANCENAME")
	log("SNREFNO:	$SNREFNO")
	log("SCHEDULED_USER:	$SCHEDULED_USER")
	log("TASK_INFO:	$TASK_INFO")
    
    # ---------------------------------------------------------------------------------
	# Get Standard Growth Value from DBSRE ConfigTemplates Table
	$getStandardValuesFromToolDBQuery = "SELECT StdValue FROM automation.ConfigTemplates 
    Where ConfigType = 'DB' and ConfigName = 'growth'"

    try {
        $std_growth_value = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query $getStandardValuesFromToolDBQuery -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD | Select-Object StdValue
        log("STD GROWTH VALUE:	$std_growth_value")
        $STD_GROWTH_VLAUE = std_growth_value
    } catch {
        log("Failed to get Standard Growth Value From Application Database due to :   $_.Exception.Message")
        Exit
    }
	
	foreach ($info in $TASK_INFO.NonStandardDatabaseConfig) {
		$DATABASE_NAME = $info.database_name
        $PHYSICAL_NAME = $info.physical_name
        
        log("PROCESSING FOR ...")
        log("DATABASE_NAME:	$DATABASE_NAME")
	    log("DATABASE_NAME:	$DATABASE_NAME")
		
		$QUERY = "ALTER DATABASE $DATABASE_NAME MODIFY FILE (NAME = $PHYSICAL_NAME,FILEGROWTH = [String]$STD_GROWTH_VLAUE + 'MB')"
        
		log("QUERY:	$QUERY")
        
        try {
            $QUERY_CONFIG_OUTPUT = invoke-sqlcmd -serverInstance $INSTANCENAME -query $QUERY
        } catch {
            $QUERY_CONFIG_OUTPUT = $_.Exception.Message
        }
		
		log("QUERY_CONFIG_OUTPUT:	$QUERY_CONFIG_OUTPUT")
	}
}

# -----------------------------------------------------------------------------
function log($Message)
{
	#$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/update-standard-instance-output.log
}

# Main Function
log("+++++++++++++ START UPDATE STANDARD INSTANCE CONFIG TASK ++++++++++++++")
nonStandardDatabaseConfigUpdate
log("+++++++++++++ END UPDATED STANDARD INSTANCE CONFIG TASK ++++++++++++++")