Function TriggerDatabaseBackup 
{
	[cmdletbinding()]
	param (
		[string]$SQLINSTANCE,
		[string]$DBNAME,
		[string]$BACKUPTYPE,
		[string]$NASDRIVELETTER,
		[string]$DBSPLITS
	)

	$Date = Get-Date -format yyyy-MM-dd
	$sql_srv_inst = $SQLINSTANCE -replace '\\','-'
	$backupfiles = @()
	
	if ($DBSPLITS -gt 1){
		foreach ($split in 1..$DBSPLITS) {
			$backupfiles += "$NASDRIVELETTER\$sql_srv_inst-$DBNAME-DOU-$BACKUPTYPE-$date-split-$split.bak"
		}
	} else {
		$backupfiles = "$NASDRIVELETTER\$sql_srv_inst-$DBNAME-DOU-$BACKUPTYPE-$date.bak"
	}

	If ($BACKUPTYPE -eq 'FULL')  {
		try {
			# Backup a complete database
			Backup-SqlDatabase -ServerInstance $SQLINSTANCE `
								-Database $DBNAME `
								-CopyOnly `
								-BackupFile $backupfiles `
								-CompressionOption On `
								-BackupAction Database `
								-Initialize `
								-checksum
			$output_data = @{output = $true; Success_Message = @{BACKUPTYPE= $BACKUPTYPE; database= $DBNAME; Location= $backupfiles; Server= $SQLINSTANCE}} | ConvertTo-Json
			return $output_data
		} catch {
			$error_data = "$_".replace('\', '\\').replace('"', ' ') 
			$output_data = @{output= $false; Fail_Message= $error_data} | ConvertTo-Json
			return $output_data
		}
	}  ElseIf ($BACKUPTYPE -eq 'DIFF')  {
		try {
			# Create a differential backup
			Backup-SqlDatabase -ServerInstance $SQLINSTANCE `
								-Database $DBNAME `
								-BackupFile $backupfiles `
								-CompressionOption On `
								-BackupAction Database `
								-Incremental `
								-Initialize `
								-checksum
			$output_data = @{output = $true; Success_Message = @{BACKUPTYPE= $BACKUPTYPE; database= $DBNAME; Location= $backupfiles; Server= $SQLINSTANCE}} | ConvertTo-Json
			return $output_data
		} catch {
			$error_data = "$_".replace('\', '\\').replace('"', ' ') 
			$output_data = @{output= $false; Fail_Message= $error_data} | ConvertTo-Json
			return $output_data
		}

	}  Else {
		'Cannot determine Backup Type Selected'
	} 
}

Function SelectNASDrivePath 
{
	[cmdletbinding()]
    Param
    (
        [string]$VAR_NASREGION,
		[string]$VAR_DBSERVER,
		[string]$VAR_DATABASE,
		[string]$VAR_USERNAME,
		[string]$VAR_PASSWORD
    )
	$select_nas_path_query = "SELECT BackupPath FROM automation.NASShare where RegionId = '$VAR_NASREGION'"

	$select_nas_drive_path = Invoke-SqlCmd -ServerInstance "$VAR_DBSERVER" -Query "$select_nas_path_query" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"

	return $select_nas_drive_path | ConvertTo-Json
}

Function InsertOrUpdateServiceRequestTable
{
	param (
		$type,
		$insert_id,
		$connString_db_server,
		$connString_db_name,
		$connString_username,
		$connString_password,
		$type_id,
		$status_id,
		$db_srv_instance,
		$sql_srv_instance,
		$db_name,
		$service_now_incident_id,
		$info,
		$user_id,
		$comments
	)

	if ($type -eq "INSERT") {
		$queryString=" 
		INSERT INTO [automation].[ServiceRequest] 
				   ([TypeId]
				   ,[StatusId]
				   ,[ServerName] 
				   ,[InstanceName]
				   ,[DBName]
				   ,[SNRefNo]
				   ,[Info] 
				   ,[Comments] 
				   ,[RequestedUser]
				   ,[CreationDate]) 
			 VALUES 
				   ('$type_id'
				   ,'$status_id'
				   ,'$db_srv_instance' 
				   ,'$sql_srv_instance' 
				   ,'$db_name'
				   ,'$service_now_incident_id'
				   ,'$info'
				   ,'$comments'
				   ,'$user_id'
				   ,getutcdate());
		SELECT SCOPE_IDENTITY();
		"	
	} elseif ($type -eq "UPDATE") {
		$queryString=" 
		UPDATE [automation].[ServiceRequest] SET
			TypeId='$type_id', 
			StatusId='$status_id',
			ServerName='$db_srv_instance',
			InstanceName='$sql_srv_instance', 
			DBName='$db_name', 
			SNRefNo='$service_now_incident_id', 
			Info='$info', 
			Comments='$comments', 
			RequestedUser='$user_id',
			CompletedDate=getutcdate()
		WHERE Id='$insert_id'; 
		SELECT SCOPE_IDENTITY();
		"
	}

	$sql_cmd_output_data = Invoke-SQLcmd -ServerInstance "$connString_db_server" -Query "$queryString" -Database "$connString_db_name" -Username "$connString_username" -Password "$connString_password"

	return $sql_cmd_output_data.Column1
}

Function UpdateBackupCancelServiceRequestTable
{
	param (
		$ServiceRequest_Id,
		$status_id,
		$dismissed_Comments,
		$connString_db_server,
		$connString_db_name,
		$connString_username,
		$connString_password
	)

	$queryString=" 
		UPDATE [automation].[ServiceRequest] SET 
			Comments='$dismissed_Comments', 
			StatusId = 
				CASE StatusId
					WHEN 'INPROGRESS' THEN '$status_id'
				END 
		
		WHERE Id='$ServiceRequest_Id'
		"
	
	try {
	
		$sql_cmd_output_data = Invoke-SQLcmd -ServerInstance "$connString_db_server" -Query "$queryString" -Database "$connString_db_name" -Username "$connString_username" -Password "$connString_password"
		
		return @{output=$true; Success_Message=$ServiceRequest_Id} | ConvertTo-Json
	} catch {
		return @{output=$false; Fail_Message=$Error[0].Exception.Message} | ConvertTo-Json
	}
}

Function CreateNASDrive
{
	param (
		[string]$SQLInstance,
		[string]$NASDriveLetter,
		[string]$NASDrivePath
	)

	try {
		# ----------------------------Enable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 1; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 1; RECONFIGURE;"

		$sp_configure_enable_output = @{output= $true} | ConvertTo-Json
	}catch {
		$sp_configure_enable_output = @{output= $false; Fail_Message= $_.Exception.Message} | ConvertTo-Json
	}

	$result = $sp_configure_enable_output | ConvertFrom-Json

	if ($result.output -eq 'True') {
		# --------------------Share Mapping is here, use appropriate value-----------------
		try {
			$InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
			# retrieve the password.
			[Byte[]] $key = (1..16)  	
			$SecurePassword = ConvertTo-SecureString $DBAToolPassword -Key $key
			$DBAToolPassword = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword))
			$sql_cmd_output_data = (Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC xp_cmdshell 'net use $NASDriveLetter $NASDrivePath /User:$InventAccount $DBAToolPassword'").output | Out-String
			
			if ($sql_cmd_output_data -NotMatch "The local device name is already in use" -And $sql_cmd_output_data -Match "error") {
				$output_data = @{output= $false; Fail_Message= @{NAS_Drive_Letter= $NASDriveLetter; actual_output= $sql_cmd_output_data}} | ConvertTo-Json
			} else {
				$output_data = @{output= $true; Success_Message= @{NAS_Drive_Letter= $NASDriveLetter; actual_output= $sql_cmd_output_data}} | ConvertTo-Json
			}      
		} catch {
			$error_data = "$_.Exception.Message"
			$output_data = @{output= $false; Fail_Message= $error_data} | ConvertTo-Json
		}
		
		# ----------------------------Disable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"

		return $output_data

	} elseif ($result.output -eq "False") {
		$error_data = "$_.Exception.Message"
		$output_data = @{output= $false; Fail_Message= $error_data} | ConvertTo-Json
		
		# ----------------------------Disable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"
		return $output_data
	}  
}

Function GetLastBackupFileSize 
{
	param (
		[string]$db_server,
		[string]$db_name
	)

	$queryString = "declare @backup_type CHAR(1) = 'D' --'D' full, 'L' log
			;with Radhe as (
				SELECT  @@Servername as [Server_Name],
				B.name as Database_Name, 
				ISNULL(STR(ABS(DATEDIFF(day, GetDate(), MAX(Backup_finish_date)))), 'NEVER') as DaysSinceLastBackup,
				ISNULL(Convert(char(11), MAX(backup_finish_date), 113)+ ' ' + CONVERT(VARCHAR(8),MAX(backup_finish_date),108), 'NEVER') as LastBackupDate
				,BackupSize_GB=CAST(COALESCE(MAX(A.BACKUP_SIZE),0)/1024.00/1024.00/1024.00 AS NUMERIC(18,2))
				,BackupSize_MB=CAST(COALESCE(MAX(A.BACKUP_SIZE),0)/1024.00/1024.00 AS NUMERIC(18,2))
				,media_set_id = MAX(A.media_set_id)
				,[AVG Backup Duration]= AVG(CAST(DATEDIFF(s, A.backup_start_date, A.backup_finish_date) AS int))
				,[Longest Backup Duration]= MAX(CAST(DATEDIFF(s, A.backup_start_date, A.backup_finish_date) AS int))
				,A.type
				FROM sys.databases B

				LEFT OUTER JOIN msdb.dbo.backupset A 
							 ON A.database_name = B.name 
							AND A.is_copy_only = 1
							AND (@backup_type IS NULL OR A.type = @backup_type) 
				where A.Database_Name = '$db_name'
				GROUP BY B.Name, A.type

			)

			 SELECT r.[Server_Name]
				   ,r.Database_Name
				   ,[Backup Type] = r.type 
				   ,r.DaysSinceLastBackup
				   ,r.LastBackupDate
				   ,r.BackupSize_GB
				   ,r.BackupSize_MB
				   ,F.physical_device_name
				   ,r.[AVG Backup Duration]
				   ,r.[Longest Backup Duration]

			   FROM Radhe r

				LEFT OUTER JOIN msdb.dbo.backupmediafamily F
							 ON R.media_set_id = F.media_set_id
				where r.Database_Name = '$db_name'
				ORDER BY r.Server_Name, r.Database_Name"

	try {
	   $sql_cmd_output_data = Invoke-Sqlcmd -Query $queryString -ServerInstance "$db_server" | Select-Object -Property @{Name="LastBackupDate"; Expression={$_.LastBackupDate}}, @{Name="physical_device_name"; Expression={$_.physical_device_name}}, @{label="BackupSize_GB";expression={[math]::round($_.BackupSize_GB)}}, * -ExcludeProperty LastBackupDate, physical_device_name, BackupSize_GB, ItemArray, Table, RowError, RowState, HasErrors | ConvertTo-Json

	   $output_data = @{output= $true; Success_Message= @{actual_output= $sql_cmd_output_data}} | ConvertTo-Json
	
	}catch {
	
		$output_data = @{output= $false; Fail_Message= $_.Exception.Message} | ConvertTo-Json
	
	}
	
	return $output_data
}

Function DeleteNASDrive
{
	param (
		$SQLInstance,
		$NASDriveLetter
	)

	try {
		# ----------------------------Enable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 1; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 1; RECONFIGURE;"

		$sp_configure_enable_output = @{output= $true} | ConvertTo-Json
	}catch {
		$sp_configure_enable_output = @{output= $false; Fail_Message= $_.Exception.Message} | ConvertTo-Json
	}

	$result = $sp_configure_enable_output | ConvertFrom-Json

	if ($result.output -eq 'True') {
		try {
			# --------------Delete Mapped Drive---------------- 
			$response = (Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC xp_cmdshell 'net use $NASDriveLetter /delete'").output

			$delete_drive = @{outpu= $true; Success_Message= $response} | ConvertTo-Json
		}catch {
			$delete_drive = @{output= $false; Fail_Message= $_.Exception.Message} | ConvertTo-Json
		}
		# ----------------------------Disable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"
		return $delete_drive

	} elseif ($result.output -eq "False") {
		$output_data = @{output= $false; Fail_Message= $_.Exception.Message}
		# ----------------------------Disable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"
		return $output_data
	}  
}

Function GetDBBackupProgress
{
	param (
		$sql_srv_instance,
		$db_name,
		$request_type
	)

	$queryString = "SELECT session_id as SPID, command, a.text AS Query, start_time, percent_complete, dateadd(second,estimated_completion_time/1000, getdate()) as estimated_completion_time
	FROM sys.dm_exec_requests r CROSS APPLY sys.dm_exec_sql_text(r.sql_handle) a
	WHERE r.command in ('BACKUP DATABASE')"

	$output = Invoke-Sqlcmd -Query $queryString -ServerInstance "$sql_srv_instance" | ConvertTo-Json
	$data = $output | ConvertFrom-Json

	if($data -ne $null) { 
		foreach ($item in $data) {
			if ($item.Query -Match $db_name) {
				if ($request_type -eq "backup_create_progress") {
					$status_complete = [math]::round($item.percent_complete) | ConvertTo-Json 
					return $status_complete
				} elseif ($request_type -eq "backup_create_cancel"){
					# https://blog.sqlauthority.com/2015/08/31/sql-server-spid-is-killedrollback-state-what-to-do-next/
					$spid = $item.SPID | ConvertTo-Json
					$hostprocess = (invoke-sqlcmd -query "select distinct(hostprocess) from master..sysprocesses where spid = '$spid'" -ServerInstance "$sql_srv_instance").hostprocess
					Invoke-Sqlcmd -Query "KILL $spid;KILL $spid with STATUSONLY;" -ServerInstance "$sql_srv_instance"
					invoke-command -scriptblock { stop-process $hostprocess -Force} 
					Start-Sleep -s 15
					$hostprocess = (invoke-sqlcmd -query "select distinct(hostprocess) from master..sysprocesses where spid = '$spid'" -ServerInstance "$sql_srv_instance").hostprocess
					if ([String]::IsNullOrEmpty($hostprocess) -eq $true) {
						return $true
					} else {
						return $false
					}
				}
			}
		}	
	} else {
		return 0 | Out-String
	}
}

Function GetDBsSizeLastBackupData
{
	param (
		$sql_srv_instance
	)
	
	$queryString = "SELECT  
	   A.[Server],  
	   A.database_name,
	   MAX(C.data_size_gb) as data_size_gb,
	   MAX(C.log_size_gb) as log_size_gb,
	   MAX(C.total_size_gb) as total_size_gb ,
	   MAX(A.last_db_backup_date) as last_db_backup_date,  
	   MAX(B.backup_start_date) as backup_start_date,  
	   MAX(B.expiration_date)as expiration_date, 
	   MAX(B.backup_size) as backup_size, 
	   MAX(B.Compressed_Backup_Size) as Compressed_Backup_Size,
	   MAX(B.logical_device_name) as logical_device_name,  
	   MAX(B.physical_device_name) as physical_device_name,   
	   MAX(B.backupset_name) as backupset_name, 
	   MAX(B.description)as description ,
	   MAX(B.device_type) as device_type,
	   device_type_string = CASE   
		  WHEN  MAX(B.device_type) = 2  THEN  'Disk'
		   WHEN  MAX(B.device_type) = 5  THEN  'Tape'
		   WHEN  MAX(B.device_type) = 7  THEN  'Virtual Device'
		   WHEN  MAX(B.device_type) = 9  THEN  'Azure Storage' 
		   WHEN  MAX(B.device_type) = 105 THEN  'Permanent Backup Device'		   
		   ELSE  'Unknown'
	   END, 
	   COUNT(B.physical_device_name) as no_of_files,
	   MAX(B.backup_finish_date) as backup_finish_date 
	FROM 
	   ( 
	   SELECT   
		   CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
		   msdb.dbo.backupset.database_name,  
		   MAX(msdb.dbo.backupset.backup_finish_date) AS last_db_backup_date 
	   FROM    msdb.dbo.backupmediafamily  
		   INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id  
	   WHERE   msdb..backupset.type = 'D' 
		  and  msdb.dbo.backupset.database_name  NOT IN ('model', 'msdb','master','Statistics_DB')
	   GROUP BY 
		   msdb.dbo.backupset.database_name  
	   ) AS A 
		
	   INNER JOIN  

	   ( SELECT   
	   CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
	   msdb.dbo.backupset.database_name,  
	   msdb.dbo.backupset.backup_start_date,  
	   msdb.dbo.backupset.backup_finish_date, 
	   msdb.dbo.backupset.expiration_date, 
	   CAST( (msdb.dbo.backupset.backup_size / (1024. * 1024. * 1024) ) AS DECIMAL(8,2))  AS backup_size, 
	   CAST (round(msdb.dbo.backupset.compressed_backup_size/1024/1024/1024,2)as decimal(10,2)) as Compressed_Backup_Size,
	   msdb.dbo.backupmediafamily.logical_device_name,  
	   msdb.dbo.backupmediafamily.physical_device_name,   
	   msdb.dbo.backupset.name AS backupset_name, 
	   msdb.dbo.backupset.description ,
	   msdb.dbo.backupmediafamily.device_type
	FROM   msdb.dbo.backupmediafamily  
	   INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id  
	WHERE  msdb..backupset.type = 'D' 
	and msdb.dbo.backupset.database_name   NOT IN ('model', 'msdb','master','Statistics_DB')
	) AS B 
	   ON A.[server] = B.[server] AND A.[database_name] = B.[database_name] AND A.[last_db_backup_date] = B.[backup_finish_date] 
	   
	   INNER JOIN 
	   (
		  SELECT 
		  database_name = DB_NAME(database_id)
		, log_size_gb = CAST((SUM(CASE WHEN type_desc = 'LOG' THEN size END) * 8. / (1024. * 1024.))AS DECIMAL(8,2))
		, data_size_gb = CAST((SUM(CASE WHEN type_desc = 'ROWS' THEN size END) * 8. / (1024. *1024)) AS DECIMAL(8,2))
		, total_size_gb = CAST((SUM(size) * 8. / (1024. * 1024.)) AS DECIMAL(8,2))
	FROM sys.master_files 
	GROUP BY database_id
	   )  AS  C
	   on A.[database_name] = C.[database_name]
	AND A.[database_name] = B.[database_name]
	group BY  A.[Server],A.[database_name]"
	
	#log -Filename "$LOG_FILE_NAME" -Message "SQL INSTANCE:	$sql_srv_instance"
	
	try {
	   $sql_cmd_list_output_data = Invoke-Sqlcmd -Query $queryString -ServerInstance "$sql_srv_instance" | Select-Object -Property @{Name="last_db_backup_date"; Expression={$_.last_db_backup_date.DateTime}}, @{Name="backup_start_date"; Expression={$_.backup_start_date.DateTime}}, @{Name="backup_finish_date"; Expression={$_.backup_finish_date.DateTime}}, @{Name="physical_device_name"; Expression={$_.physical_device_name}}, * -ExcludeProperty last_db_backup_date, backup_start_date, backup_finish_date, physical_device_name, ItemArray, Table, RowError, RowState, HasErrors
	   $sql_cmd_output_data = $sql_cmd_list_output_data | ConvertTo-Json
	   
	   #log -Filename "$LOG_FILE_NAME" -Message "SQL QUERY OUTPUT:	$sql_cmd_output_data"
	   
	   if ($sql_cmd_list_output_data.Count -ge 1){
		$output_data = @"
{
   "output": "$true" ,
   "data": {
      "DBsSizeAndLastBkpData": $sql_cmd_output_data
   }
}
"@
   } else {
		$output_data = @"
{
   "output": "$true" ,
   "data": {
      "DBsSizeAndLastBkpData": [$sql_cmd_output_data]
   }
}
"@
   }
}catch {
   $output_data = @"
{
   "output": "$false" ,
   "Error": "$_.Exception.Message"
}
"@
}
	#log -Filename "$LOG_FILE_NAME" -Message "RETURN OUTPUT:	$output_data"
	return $output_data
}

Function GetDBBackupHistoryWithDateRange
{
	param (
		$sql_srv_instance,
		$db_name,
		$start_date,
		$end_date
	)

	$start_date = "'" + "$start_date" + "'" | out-string
	$end_date = "'" + "$end_date" + "'" | out-string

	if ([String]::IsNullOrEmpty($db_name) -eq $true) {
		$where_cmd = "WHERE ((CONVERT(datetime, msdb.dbo.backupset.backup_start_date, 102) BETWEEN CONVERT(datetime, $start_date) AND CONVERT(datetime, $end_date) + 1))"
	} else {
		$where_cmd = "WHERE (msdb.dbo.backupset.database_name = '$db_name' AND (CONVERT(datetime, msdb.dbo.backupset.backup_start_date, 102) BETWEEN CONVERT(datetime, $start_date) AND CONVERT(datetime, $end_date) + 1))"
	}

	# --------------------------------------------------------------------------------- 
	# --Database Backups for all databases For Previous Week 
	# --------------------------------------------------------------------------------- 
	$queryString = "SELECT 
	CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
	msdb.dbo.backupset.database_name, 
	msdb.dbo.backupset.backup_start_date, 
	msdb.dbo.backupset.backup_finish_date, 
	msdb.dbo.backupset.expiration_date, 
	CASE msdb..backupset.type 
	WHEN 'D' THEN 'Full' 
	WHEN 'I' THEN 'Differential'
	WHEN 'L' THEN 'Log' 
	END AS backup_type, 
	CAST( (msdb.dbo.backupset.backup_size / (1024. * 1024. * 1024) ) AS DECIMAL(8,2))  AS backup_size, 
	CAST (round(msdb.dbo.backupset.compressed_backup_size/1024/1024/1024,2)as decimal(10,2)) as Compressed_Backup_Size,
	msdb.dbo.backupmediafamily.logical_device_name, 
	msdb.dbo.backupmediafamily.physical_device_name, 
	msdb.dbo.backupset.name AS backupset_name, 
	msdb.dbo.backupset.description 
	FROM msdb.dbo.backupmediafamily 
	INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id " +
	$where_cmd + " ORDER BY 
	msdb.dbo.backupset.database_name, 
	msdb.dbo.backupset.backup_finish_date
	"

	try {
		$sql_cmd_list_output_data = Invoke-Sqlcmd -Query $queryString -ServerInstance "$sql_srv_instance" | Select-Object -Property @{Name="last_db_backup_date"; Expression={$_.last_db_backup_date.DateTime}}, @{Name="backup_start_date"; Expression={$_.backup_start_date.DateTime}}, @{Name="backup_finish_date"; Expression={$_.backup_finish_date.DateTime}}, @{Name="physical_device_name"; Expression={$_.physical_device_name}}, * -ExcludeProperty last_db_backup_date, backup_start_date, backup_finish_date, physical_device_name, ItemArray, Table, RowError, RowState, HasErrors
		$sql_cmd_output_data = $sql_cmd_list_output_data | ConvertTo-Json
		
		if ($sql_cmd_list_output_data.Count -ge 1){
			$output_data = @"
{
			"output": "$true" ,
		   "data": {
			  "DatabaseBackupHistory": $sql_cmd_output_data
		   }
}
"@	
		} else {
			$output_data = @"
{
		   "output": "$true" ,
		   "data": {
			  "DatabaseBackupHistory": [$sql_cmd_output_data]
		   }
}
"@
		}
	} catch {
		$output_data = @"
{
	   "output": "$false" ,
	   "Error": "$_.Exception.Message"
}
"@
	}
	
	return $output_data
}