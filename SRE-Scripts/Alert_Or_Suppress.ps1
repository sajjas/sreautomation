# -----------------------------------------------------------------------------
# @function AlertInsertOrSuppressCheck:
#   In the following Cases, the alerts creation to be Suppressed. 
#   a) Cluster Unbalance Alert: When for the Same Server/another Server in the Cluster an already exists in Created/New State.
#   b) DB Backup Overdue Alert: When for the Same Server, InstanceName and DBName if alert already exists in Created/New State.
#   c) Non Standard Instance: When for the same server and Instance if alert already exists in Created/New State.
#   d) non Standard Database: When for the same server, Instance and DBName if alert is already exists in Created/New State.
# @return:   Existing Alert Id or New Alert Id and Suppression Flag in Json Format
#            
# @param emailTemplateInstance:  Dictionary which encapsules the AlertInfo
#
# The Check is based on AlertType + Status + serverNamenodomain + InstanceName + DBName and based on applicable criteria.
#
# -----------------------------------------------------------------------------
function getConnection
{
    try
    {
        $connectionString = "server=$VAR_SERVERINSTANCE;database='$VAR_DATABASE';user id=$VAR_USERNAME;password=$VAR_PASSWORD";
        #SQL Connection - connection to SQL server
        $sqlConnection = new-object System.Data.SqlClient.SqlConnection;
        $sqlConnection.ConnectionString = $connectionString;
    }
    catch
    {
        log($ERROR[0])
        $sqlConnection = $null
    }
    return $sqlConnection
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function executeSelectQuery
{
    param ([String]$select_qry, $parameters = @{ })
    log( "<----------- Pulling Data from DB Starts -------->")
    if ($null -ne $select_qry -and $select_qry)
    {
        $query_final = $select_qry
        $dataset_cursor = New-Object System.Data.DataTable
        $sqlSelectConnection = getConnection
        $return_dic = $null
        if ($null -ne $sqlSelectConnection -and $sqlSelectConnection)
        {
            try
            {
                $sqlSelectConnection.Open()
                #SQL QUERY Execution
                $sqlCommand = New-Object System.Data.SqlClient.SqlCommand;
                $sqlCommand.Connection = $sqlSelectConnection;
                $sqlCommand.CommandText = $query_final;
                #SQL Parameters
                if ($null -ne $parameters -and $parameters)
                {
                    foreach ($p in $parameters.Keys)
                    {
                        [Void] $sqlCommand.Parameters.AddWithValue("@$p", $parameters[$p])
                        $param_text = $parameters[$p]
                        log("parameter $p  --> $param_text ")
                    }
                }
                #SQL Return Values
                $READER_DATA = $sqlCommand.ExecuteReader()
                $dataset_cursor.Load($READER_DATA)
                $sqlSelectConnection.Close()
                if ($null -ne $dataset_cursor)
                {
                    $return_dic = @{ }
                    foreach ($tupla in $dataset_cursor)
                    {
                        foreach ($columna in $dataset_cursor.Columns)
                        {
                            $value_x = $tupla.item($columna)
                            $columna_x = $columna
                            $return_dic.Add("$columna_x", "$value_x")
                            log("[$columna_x]=$value_x")
                        }
                    }
                }
            }
            catch
            {
                $return_dic = $null
                log($ERROR[0])
                log("Not Able to execute Select Query - [ $select_qry ]")
            }
        }

    }
    log( "<----------- Pulling Data from DB Ends -------->")
    return $return_dic
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
Function AlertInsertOrSuppressCheck ($PARAM=@{}) 
{
    #$ALERT_STATUS = $PARAM.Status
    $ALERT_TYPE = $PARAM.Type
    #$ALERT_INFO = $PARAM.Info
    $SERVERNAME = $PARAM.ServerName
    $INSTANCENAME = $PARAM.InstanceName
    $DBNAME = $PARAM.Database
    #$CLUSTER_INSTANCES = $PARAM.ClusterInstances
    
    # -----------------------------------------------------------------------------
    # First Check in AlertSuppression Table
    if ($ALERT_TYPE -eq "CLUSTER_UNBALANCE") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_INFO = "SELECT Id FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "OVERDUE_BACKUP") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_INFO = "SELECT Id FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and DBName='$DBNAME'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_INSTANCE") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_INFO = "SELECT Id FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_DATABASE") {
         # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_INFO = "SELECT Id FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and DBName='$DBNAME'"
        # -----------------------------------------------------------------------------
    }

    log($QRY_GET_ALERT_SUPPRESSION_INFO)

    $ALERT_SUPPRESSION_ID = executeSelectQuery -select_qry $QRY_GET_ALERT_SUPPRESSION_INFO -parameters @{}
    
    if ($ALERT_SUPPRESSION_ID.Count -eq 0) {
        if ($ALERT_TYPE -eq "CLUSTER_UNBALANCE") {
            $ALERT_ID_JSON_DATA = checkAlertTable $ALERT_TYPE $PARAM
        } elseif ($ALERT_TYPE -eq "OVERDUE_BACKUP") {
            $ALERT_ID_JSON_DATA = checkAlertTable $ALERT_TYPE $PARAM
        } elseif ($ALERT_TYPE -eq "NON_STANDARD_INSTANCE") {
            $ALERT_ID_JSON_DATA = checkAlertTable $ALERT_TYPE $PARAM
        } elseif ($ALERT_TYPE -eq "NON_STANDARD_DATABASE") {
            $ALERT_ID_JSON_DATA = checkAlertTable $ALERT_TYPE $PARAM
        }
    } else {
        $ALERT_ID_JSON_DATA = suppressedAlertInfo $ALERT_TYPE $PARAM
    }

    return $ALERT_ID_JSON_DATA
}

Function suppressedAlertInfo
{
    param (
        $ALERT_TYPE,
        $PARAM
    )

    if ($ALERT_TYPE -eq "CLUSTER_UNBALANCE") {
        
        $MSG = "This Alert is Suppressed. Hence Skipping Raising an Alert!"

        log($MSG)
        
        $ALERT_ID_JSON = @{output = $true; Success_Message = $MSG; Alert_Suppression = $true} | ConvertTo-Json
    
    } elseif ($ALERT_TYPE -eq "OVERDUE_BACKUP") {
        
        $MSG = "This Alert is Suppressed. Hence Skipping Raising an Alert!"

        log($MSG)
        
        $ALERT_ID_JSON = @{output = $true; Success_Message = $MSG; Alert_Suppression = $true} | ConvertTo-Json
    
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_DATABASE") {
        
        $MSG = "This Alert is Suppressed. Hence Skipping Raising an Alert!"

        log($MSG)
        
        $ALERT_ID_JSON = @{output = $true; Success_Message = $MSG; Alert_Suppression = $true} | ConvertTo-Json
    
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_INSTANCE") {

        $ALERT_INFO_JSON = New-Object System.Collections.ArrayList

        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_SUPPRESSION_CONFIG_KEYS = "SELECT ConfigKeys FROM automation.AlertSuppression WHERE AlertType='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME'"
        # -----------------------------------------------------------------------------
        
        log($QRY_GET_ALERT_SUPPRESSION_CONFIG_KEYS)

        $CONFIG_KEYS = executeSelectQuery -select_qry $QRY_GET_ALERT_SUPPRESSION_CONFIG_KEYS -parameters @{}
        $CONFIG_KEYS =  $CONFIG_KEYS.ConfigKeys.split(',')
        
        $ALERT_INFO_FROM_TABLE = $PARAM.Info | ConvertFrom-Json

        foreach ($i in $ALERT_INFO_FROM_TABLE.NonStandardInstanceConfig) { 
            if ($CONFIG_KEYS -NotContains $i.ConfigId) { 
                $ALERT_INFO_JSON.add(
                    @{configuration_id=$i.configuration_id; 
                        name=$i.name; 
                        value=$i.value;
                        maximum=$i.maximum; 
                        value_in_use=$i.value_in_use; 
                        description=$i.description; 
                        ConfigId=$i.ConfigId; 
                        ValueType=$i.ValueType; 
                        StdValue=$i.StdValue
                    }
                ) 
            } 
        }

        $NON_STD_INSTANCE_CONFIG_DATA = $ALERT_INFO_JSON | ConvertTo-Json

        if($ALERT_INFO_JSON.Count -le 1) {
            $INFO = @"
{
    "NonStandardInstanceConfig": [$NON_STD_INSTANCE_CONFIG_DATA]
}
"@
        } else {
            $INFO = @"
{
    "NonStandardInstanceConfig": $NON_STD_INSTANCE_CONFIG_DATA
}
"@
        }

        $PARAM.Info = $INFO

        $ALERT_ID_JSON = checkAlertTable $ALERT_TYPE $PARAM

    }

    return $ALERT_ID_JSON
}

Function checkAlertTable 
{
    param (
        $ALERT_TYPE, 
        $PARAM=@{}
    )

    $ALERT_STATUS = $PARAM.Status
    #$ALERT_INFO = $PARAM.Info
    $SERVERNAME = $PARAM.ServerName
    $INSTANCENAME = $PARAM.InstanceName
    $DBNAME = $PARAM.Database
    #$CLUSTER_INSTANCES = $PARAM.ClusterInstances

    # -----------------------------------------------------------------------------
    # Second Check in Alert Table
    if ($ALERT_TYPE -eq "CLUSTER_UNBALANCE") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_ID = "SELECT Id FROM automation.Alert WHERE TypeId='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and StatusId='$ALERT_STATUS'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "OVERDUE_BACKUP") {
        # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_ID = "SELECT Id FROM automation.Alert WHERE TypeId='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and DBName='$DBNAME' and StatusId='$ALERT_STATUS'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_INSTANCE") {
         # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_ID = "SELECT Id FROM automation.Alert WHERE TypeId='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and StatusId='$ALERT_STATUS'"
        # -----------------------------------------------------------------------------
    } elseif ($ALERT_TYPE -eq "NON_STANDARD_DATABASE") {
         # -----------------------------------------------------------------------------
        $QRY_GET_ALERT_ID = "SELECT Id FROM automation.Alert WHERE TypeId='$ALERT_TYPE' and ServerName='$SERVERNAME' and InstanceName='$INSTANCENAME' and DBName='$DBNAME' and StatusId='$ALERT_STATUS'"
        # -----------------------------------------------------------------------------
    }

    log($QRY_GET_ALERT_ID)
    
    $ALERT_ID = executeSelectQuery -select_qry $QRY_GET_ALERT_ID -parameters @{}
    
    if ($ALERT_ID.Count -eq 0) {
        $ALERT_ID_JSON = AlertRecordInsert($PARAM)
    } else {    
        log("The AlertId is Already Present in the Database!")
        $ALERT_ID_JSON = @{output = $true; Success_Message = $ALERT_ID; Alert_Suppression = $true} | ConvertTo-Json
    }

    return $ALERT_ID_JSON
}

Function AlertRecordInsert($PARAM=@{})
{
    $ALERT_STATUS = $PARAM.Status
    $ALERT_TYPE = $PARAM.Type
    $INFO = $PARAM.Info
    $SERVERNAME = $PARAM.ServerName
    $INSTANCENAME = $PARAM.InstanceName
    $DBNAME = $PARAM.Database

	$queryString = "
	INSERT INTO [automation].[Alert]
		   ([TypeId]
		   ,[CreationDate]
		   ,[StatusId]
		   ,[Info]
		   ,[Last_Modified_Date]
		   ,[ServerName]
		   ,[InstanceName]
		   ,[DBName])
		VALUES
            ('$ALERT_TYPE'
            ,getutcdate()
            ,'$ALERT_STATUS'
            ,'$INFO'
            ,getutcdate()
            ,'$SERVERNAME'
            ,'$INSTANCENAME'
            ,'$DBNAME');
		SELECT SCOPE_IDENTITY();
	"
	try {
		$ALERT_ID = (invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD").Column1
		$ALERT_ID_JSON_DATA = @{output = $true; Success_Message = $ALERT_ID; Alert_Suppression = $false} | ConvertTo-Json
	} catch {
		$ALERT_ID_JSON_DATA = @{output = $false; Fail_Message = $_.Exception.Message; Alert_Suppression = $false} | ConvertTo-Json
	}
	
	return $ALERT_ID_JSON_DATA
}
# -----------------------------------------------------------------------------