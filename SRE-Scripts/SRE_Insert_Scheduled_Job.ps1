Function InsertScheduledJob
{
	Param (
		$JobName,
		$AlertType,
		$CreatedBy,
		$JobTarget,
		$JobScriptFile,
		$JobInputs,
		$ScheduledJobFrequency,
		$ScheduledJobTime,
		$ScheduledJobStatus,
		$type,
		$connString_db_server,
		$connString_db_name,
		$connString_username,
		$connString_password,
		$insert_id
	)

	if ($type -eq "INSERT") {
		$queryString = "
		INSERT INTO [automation].[ScheduledJobs]
				   ([JobName]
				   ,[AlertType]
				   ,[JobTarget]
				   ,[JobScriptFile]
				   ,[JobInputs]
				   ,[ScheduledJobFrequency]
				   ,[ScheduledJobTime]
				   ,[ScheduledJobStatus]
				   ,[CreatedBy]
				   ,[CreatedDate])
			 VALUES
				   ('$JobName'
				   ,'$AlertType'
				   ,'$JobTarget'
				   ,'$JobScriptFile'
				   ,'$JobInputs'
				   ,'$ScheduledJobFrequency'
				   ,'$ScheduledJobTime'
				   ,'$ScheduledJobStatus'
				   ,'$CreatedBy'
				   , getutcdate());
		SELECT SCOPE_IDENTITY();
		"
	} elseif ($type -eq "UPDATE") {
		$queryString="
		UPDATE [automation].[ScheduledJobs] SET
			JobName='$JobName',
			AlertType='$AlertType',
			JobTarget='$JobTarget',
			JobScriptFile='$JobScriptFile',
			JobInputs='$JobInputs',
			ScheduledJobFrequency='$ScheduledJobFrequency',
			ScheduledJobTime='$ScheduledJobTime',
			ScheduledJobStatus='$ScheduledJobStatus',
			CreatedBy='$CreatedBy',
			CreatedDate=getutcdate()
		WHERE Id='$insert_id';
		"
	}

	try {
		$sql_cmd_output_data = invoke-sqlcmd -ServerInstance "$connString_db_server" -Query "$queryString" -Database "$connString_db_name" -ErrorAction "stop" -Username "$connString_username" -Password "$connString_password"
		
		if($null -eq $sql_cmd_output_data -And $type -eq "INSERT") {
		
			$sql_cmd_output_data_return = @{output=$false; Fail_Message=$Error[0].Exception.Message} | ConvertTo-Json
		
		} elseif($type -eq "UPDATE") {
		
			$sql_cmd_output_data_return = @{output=$true; Success_Message=$insert_id} | ConvertTo-Json
		
		} else {
			
			$sql_cmd_output_data_return = @{output=$true; Success_Message=$sql_cmd_output_data.Column1} | ConvertTo-Json
			
		}
	
	} catch {
	
		$sql_cmd_output_data_return = @{output=$false; Fail_Message=$Error[0].Exception.Message} | ConvertTo-Json
	}
	
	return $sql_cmd_output_data_return
}