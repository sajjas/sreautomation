# -----------------------------------------------------------------------------
Function NonStdInstanceConfig
{
	Param (
		$CASE,
		$SQLINSTANCE,
		$VAR_SERVERINSTANCE, 
		$VAR_DATABASE, 
		$VAR_USERNAME, 
		$VAR_PASSWORD
	)

	if ($CASE -eq "R1") {
		#Get data below query result in r1 from remote Server
		$getInstanceDetailsQuery = "SELECT A.configuration_id, A.name, A.value, A.maximum, A.value_in_use, A.description
		FROM sys.configurations A
		ORDER BY A.configuration_id
		"
		
		$NON_STD_INST_CONFIG = Invoke-Sqlcmd -ServerInstance "$SQLINSTANCE" -query "$getInstanceDetailsQuery"
	} elseif ($CASE -eq "R2") {
		#Get data below query result in r2 from SREDB
		$getStandardValuesFromToolDBQuery = "SELECT B.ConfigId, B.ConfigClass, B.IsShown, B.IsChangeable
		FROM [automation].ConfigValues B 
		ORDER BY B.configid
		"
		
		$NON_STD_INST_CONFIG = Invoke-Sqlcmd -serverInstance "$VAR_SERVERINSTANCE" -query "$getStandardValuesFromToolDBQuery" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
	} elseif ($CASE -eq "R3") {
		#Get data below query result in r3 from SREDB
		$getStandardValuesFromToolDBQuery = "SELECT B.ConfigId, B.ValueType, B.StdValue, B.DefaultValue 
		FROM automation.ConfigTemplates B 
		Where InstanceType = 'Type1'
		ORDER BY B.ConfigId
		"
		$NON_STD_INST_CONFIG = Invoke-Sqlcmd -serverInstance "$VAR_SERVERINSTANCE" -query "$getStandardValuesFromToolDBQuery" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
	}
	return $NON_STD_INST_CONFIG
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function nonStandardInstanceConfigCheck
{
    param (
        $LIST_OF_RESULTS_TO_COMPARE, 
        $MEMORY_IN_BYTES    
    )
	
	$NON_STANDARD_CONFIG_LIST = New-Object System.Collections.ArrayList

	foreach ($OBJ in $LIST_OF_RESULTS_TO_COMPARE) {
		# Simple
		if($OBJ.ValueType -eq "Simple") {
			if($OBJ.value_in_use -ne $OBJ.StdValue) {
				$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
			}
		# Range
		} elseif($OBJ.ValueType -eq "Range") {
			if ($OBJ.StdValue -Match "-") {
				$range = $OBJ.StdValue.split("-")
				$min = $range[0]
				$max = $range[1]
				if($min -gt $OBJ.value_in_use -And $OBJ.value_in_use -gt $max) {
					$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
				}
			} else {
				if($OBJ.value_in_use -lt $OBJ.StdValue) {
					$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
				}
			}
		# Complex
		} elseif($OBJ.ValueType -eq "Complex" -And $OBJ.ConfigId -eq 1544) {
			$80_PERC_VALUE = $MEMORY_IN_BYTES * ($OBJ.StdValue/100)
			if($OBJ.value_in_use -gt $80_PERC_VALUE) {
				$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
			}
		}
	}
	
	# return $NON_STANDARD_CONFIG_LIST
	$NON_STD_INSTANCE_CONFIG_DATA = $NON_STANDARD_CONFIG_LIST | ConvertTo-Json
		
	if($NON_STANDARD_CONFIG_LIST.Count -le 1) {
		$INFO = @"
{
	"output": "true",
	"NonStandardInstanceConfig": [$NON_STD_INSTANCE_CONFIG_DATA]
}
"@
	} else {
		$INFO = @"
{
	"output": "true",
	"NonStandardInstanceConfig": $NON_STD_INSTANCE_CONFIG_DATA
}
"@
	}

	return $INFO
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function nonStandardDatabaseConfigCheck
{
    param (
        $LIST_OF_RESULTS_TO_COMPARE,
        $LIST_OF_STANDARD_VALUES
    )

	$NON_STANDARD_CONFIG_LIST = New-Object System.Collections.ArrayList

    foreach ($STD_OBJ in $LIST_OF_STANDARD_VALUES) {
        if ($STD_OBJ.ConfigName -eq "is_percent_growth") {
            $STD_IS_PERC_GROWTH = $STD_OBJ.StdValue
            $DEFAULT_IS_PERC_GROWTH = $STD_OBJ.DefaultValue
        } elseif ($STD_OBJ.ConfigName -eq "growth") {
            $STD_GROWTH = $STD_OBJ.StdValue
            $DEFAULT_GROWTH = $STD_OBJ.DefaultValue
        } elseif ($STD_OBJ.ConfigName -eq "size") {
            $STD_SIZE = $STD_OBJ.StdValue
            $DEFAULT_SIZE = $STD_OBJ.DefaultValue
        }
    }

    $PER_DATABASE_INFO = New-Object System.Collections.ArrayList
    
    $DATABASE_NAMES = $LIST_OF_RESULTS_TO_COMPARE.database_name | Sort-Object | Get-Unique
    
    $SIZE_NON_STD = $false
    
    $GROWTH_NON_STD = $false

    foreach ($DB_NAME in $DATABASE_NAMES) {
        
        foreach ($obj in $LIST_OF_RESULTS_TO_COMPARE) {
            
            if ($obj.database_name -eq $DB_NAME) {
                
                $PER_DATABASE_INFO += $obj
                
            }
        }

        $NON_STANDARD_CONFIG_LIST_PER_DB = New-Object System.Collections.ArrayList

        
        foreach ($ROW in $PER_DATABASE_INFO) {
        
            # Size Checker
            if ($ROW.size -lt $STD_SIZE) {
                
                $SIZE_NON_STD = $true

                $ROW | add-member -Name "Is_Initial_Size_Non_Std" -value $SIZE_NON_STD -MemberType NoteProperty

                $ROW | add-member -Name "Current_Initial_Size" -value $ROW.size -MemberType NoteProperty

                $ROW | add-member -Name "Initial_Size_DefaultValue" -value $DEFAULT_SIZE -MemberType NoteProperty
                        
            } else {

                $ROW | add-member -Name "Is_Initial_Size_Non_Std" -value $SIZE_NON_STD -MemberType NoteProperty

                $ROW | add-member -Name "Current_Initial_Size" -value $ROW.size -MemberType NoteProperty

                $ROW | add-member -Name "Initial_Size_DefaultValue" -value $DEFAULT_SIZE -MemberType NoteProperty
                
            }

            # Growth Checker
            if ([string]$ROW.is_percent_growth -ne [string]$STD_IS_PERC_GROWTH) {
                
                $GROWTH_NON_STD = $true

                $ROW | add-member -Name "Is_Growth_Non_Std" -value $GROWTH_NON_STD -MemberType NoteProperty

                $ROW | add-member -Name "Current_Growth_Value" -value $ROW.growth_in_MB -MemberType NoteProperty
 
                $ROW | add-member -Name "Growth_DefaultValue" -value $DEFAULT_GROWTH -MemberType NoteProperty

                $ROW | add-member -Name "Current_Is_Percent_Growth" -value $ROW.is_percent_growth -MemberType NoteProperty

                $ROW | add-member -Name "Is_Percent_Growth_DefaultValue" -value $DEFAULT_IS_PERC_GROWTH -MemberType NoteProperty
        
            
            } elseif (([string]$ROW.is_percent_growth -eq [string]$STD_IS_PERC_GROWTH) -And ($ROW.growth_in_MB -lt $STD_GROWTH)) {

                $GROWTH_NON_STD = $true

                $ROW | add-member -Name "Is_Growth_Non_Std" -value $GROWTH_NON_STD -MemberType NoteProperty

                $ROW | add-member -Name "Current_Growth_Value" -value $ROW.growth_in_MB -MemberType NoteProperty
 
                $ROW | add-member -Name "Growth_DefaultValue" -value $DEFAULT_GROWTH -MemberType NoteProperty

                $ROW | add-member -Name "Current_Is_Percent_Growth" -value $ROW.is_percent_growth -MemberType NoteProperty

                $ROW | add-member -Name "Is_Percent_Growth_DefaultValue" -value $DEFAULT_IS_PERC_GROWTH -MemberType NoteProperty
        
            } else {
            
                $ROW | add-member -Name "Is_Growth_Non_Std" -value $GROWTH_NON_STD -MemberType NoteProperty

                $ROW | add-member -Name "Current_Growth_Value" -value $ROW.growth_in_MB -MemberType NoteProperty
 
                $ROW | add-member -Name "Growth_DefaultValue" -value $DEFAULT_GROWTH -MemberType NoteProperty

                $ROW | add-member -Name "Current_Is_Percent_Growth" -value $ROW.is_percent_growth -MemberType NoteProperty

                $ROW | add-member -Name "Is_Percent_Growth_DefaultValue" -value $DEFAULT_IS_PERC_GROWTH -MemberType NoteProperty
        
            } 
            
            # Final Check to add to non std List
            if ($true -eq $SIZE_NON_STD -Or $true -eq $GROWTH_NON_STD) {
            
                $NON_STANDARD_CONFIG_LIST_PER_DB.add($ROW) | Out-Null
            } 

        }

        $NON_STANDARD_CONFIG_LIST.add($NON_STANDARD_CONFIG_LIST_PER_DB) | Out-Null
        
        $PER_DATABASE_INFO = @()
   
    }

	$NON_STD_DATABASE_CONFIG_DATA = $NON_STANDARD_CONFIG_LIST | ConvertTo-Json
		
	$INFO = @"
{
		"output": "true",
		"NonStandardDatabaseConfig": $NON_STD_DATABASE_CONFIG_DATA
}
"@
	
	return $INFO
}
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
function NonStdDatabaseConfig
{
	param(
		$INSTANCENAME,
		$DBNAME
	)

	#Get data below query result in r1 from remote Server
    $queryString = "select a.database_id,
	   b.file_id,
       a.[name] as database_name,
	   b.physical_name,
       b.name,
       a.is_auto_close_on,
       a.is_auto_shrink_on,
       b.size,
       b.max_size,
		CASE WHEN b.is_percent_growth = 0
			THEN CONVERT(DECIMAL(20,0), (CONVERT(DECIMAL, b.growth)/128))
			ELSE b.growth
			END AS growth_in_MB,
		b.is_percent_growth
        from (select * from sys.databases where name IN('$DBNAME')) as a, sys.master_files as b
        where a.database_id = b.database_id"

    try {
        $result = invoke-sqlcmd -serverInstance $INSTANCENAME -query $queryString | Select-Object database_id, file_id, database_name, physical_name, name, is_auto_close_on, is_auto_shrink_on, size, max_size, growth_in_MB, is_percent_growth
    } catch {
		$Error_Message = $_.Exception.Message
        log("ERROR:	$Error_Message")
        Exit
    }

	return $result
}
# -----------------------------------------------------------------------------