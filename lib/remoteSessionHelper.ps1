﻿# Steps for password encrypt and decrypt
# https://www.pdq.com/blog/secure-password-with-powershell-encrypting-credentials-part-2/

Function Create-PSSession {

[CmdletBinding()]
Param (
    [Parameter(Mandatory=$True)]
    [string]$ServerName
)

$Session_Jumpy = $null

    If ($ServerName -match "ey.net" -and $ServerName -notmatch "cloudapp"){
        $InventAccount = "EY\P.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
    } Elseif ($ServerName -match "eydmz.net"){
        $Session_Jumpy = JumpServer
		$InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AGEAUgBEAEQAQgBDAHUAaQA0ADQAagBHAHUAZAAvAHkASgAzAG0AUgBKAHcAPQA9AHwAMAA5AGYAZQAxADkAYQA3ADMAYgBhAGQANQAyADUAMgA4ADcANgAwAGIAYgBhADQAMwAwAGQAYgAyAGMANgBjADUAZgBjADIAZQBiAGMAYgA1ADMANgBjAGQANQBlADAAYgA0ADUANgBlAGYAYgA4AGIANwAyADMAOAA5ADkAOAA='
    } Elseif ($ServerName -match "eyxstaging.net"){
		$Session_Jumpy = JumpServer
		$InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoAWgByAHYAMAB4AFkAUQBVAFAAQgBXAEEAUgA5ADAAawBmAEQAbAB5AEEAPQA9AHwAMgA4ADkAZgBmAGEANQA3AGUANwAwADQAZAA3ADUAYQBkADMAZgAwADMANQBmADQAYwA1AGQAZgA0ADIAMwA0ADgAYwA3AGYANQBkAGUAMwA1ADUANgA3ADYAYgBhAGQAOQAxADcAMgBmAGQAOAA0ADkAMABhAGIAYgBkADUAYQA='
	} Elseif ($ServerName -match "eyua.net"){
        $InventAccount = "EYUA\U.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHIAZgBzAGkAOABxADUAOQBmAGIAVwB4AFcAKwBHAEEANQA4ADYANQBpAGcAPQA9AHwAMQBkADUAOAA3ADEAYwA2AGYANwA3AGIANwA2AGUANQAwADUAYQA2AGYAZQBkADAAZgAwAGEAMQA5ADAAMQBkADMAYQAwADcAYwAyAGIAZQAwADUAMgBiADYAMQBjAGYAMwA5ADcAMAA2ADUAMgBjADYAMwA5ADkAYgA0ADUAOQA='
    } Elseif ($ServerName -match "eyqa.net"){
        $InventAccount = "EYQA\Q.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHcAdgA3AFcAcQBIAEYAUQAwAHoAUABNAGkAZAB4AHYANQBoAFcAVABWAHcAPQA9AHwANQAxADYAYwBiADcAOABmADMAYQAzADUAMgBlAGYANgBmAGUAMwA0ADEAMwA0AGMANwBkADcAYgA2ADMAZgAxADEAYQA4AGIAMQAxADcANwA2ADcAYwAwADYAMABkADQANwAyADcAMwA1ADMAYQA1ADYAZABlADgAMABmAGIAZQA='
    } Elseif ($ServerName -match "eydev.net"){
        $InventAccount = "EYDEV\D.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEYANABNAFcAbgBTAHAAUQBMAGYAYgAxAGgASAAyAGUAQgBtAGYAZQBTAFEAPQA9AHwAMwBlAGEAZQA1AGYAMgBlAGUANABkADQAOABlAGYAMwBlAGMAMABhADYAMAAzADkAMgBjADgAYQAxADQAMgA0AGEAMQA4ADQAZQA4AGMAMAAzADgAMgA5AGIANwBmADEAYgA0ADkAZgBmADIAYgA3AGEAYQBhAGYAZABhADIANwA='
    } Elseif ($ServerName -match "ey.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPP.NET\C.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEEAaQBJADUAMgA1AEMAVABXAC8AbQBSAHgAWgBaAHgAUABoAHIAVwA0AEEAPQA9AHwAZAAwAGYAOQAzAGUAMwAzADIAMwA2AGYAZgA4ADUAOABlADkANQBkADcANwAzAGYAOQBiADMAMAA2AGIANwBkADMAZABiADIAOAAyAGMANgBhADEAYgA5ADQAZAAwAGYAYQA4ADMAMgAyADAANwA1ADQAZQA4ADkAOAAxADAAMAA='
    } Elseif ($ServerName -match "eydev.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AFQAYgBWAGMANwBWAHQAbQBRAHAATgA0AHIAQQBUAEIAbQBiAG4AbgBtAEEAPQA9AHwAMgA5AGYAZgA3ADUAOQAwAGEAZAAwADYAYQA2ADQAZgAxAGMAYgBjADIANQBmAGEAYQAxADcAZgBmAGQAOQBkADYAZAAzAGYAOAAxADEAOAA4ADgAMAAyAGUANABkADAAYgBkADgANQBhAGEANwA1AGYAYgBlADIAMAAwADEAZAA='
    } Else {
        If ($ServerName -match '^[A-Z]{6,7}[Pp]' -and ($($ServerName.Substring(0,2)) -notmatch "^AC" -and $ServerName -notmatch ".cloudapp.ey.net$")){
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Zz]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
			$Session_Jumpy = JumpServer
            $InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AGEAUgBEAEQAQgBDAHUAaQA0ADQAagBHAHUAZAAvAHkASgAzAG0AUgBKAHcAPQA9AHwAMAA5AGYAZQAxADkAYQA3ADMAYgBhAGQANQAyADUAMgA4ADcANgAwAGIAYgBhADQAMwAwAGQAYgAyAGMANgBjADUAZgBjADIAZQBiAGMAYgA1ADMANgBjAGQANQBlADAAYgA0ADUANgBlAGYAYgA4AGIANwAyADMAOAA5ADkAOAA='
            If ($ServerName -notmatch 'eydmz.net$'){
                $ServerName += ".eydmz.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Xx]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $Session_Jumpy = JumpServer
			$InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoAWgByAHYAMAB4AFkAUQBVAFAAQgBXAEEAUgA5ADAAawBmAEQAbAB5AEEAPQA9AHwAMgA4ADkAZgBmAGEANQA3AGUANwAwADQAZAA3ADUAYQBkADMAZgAwADMANQBmADQAYwA1AGQAZgA0ADIAMwA0ADgAYwA3AGYANQBkAGUAMwA1ADUANgA3ADYAYgBhAGQAOQAxADcAMgBmAGQAOAA0ADkAMABhAGIAYgBkADUAYQA='
            If ($ServerName -notmatch 'eyxstaging.net$'){
                $ServerName += ".eyxstaging.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Uu]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYUA\U.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHIAZgBzAGkAOABxADUAOQBmAGIAVwB4AFcAKwBHAEEANQA4ADYANQBpAGcAPQA9AHwAMQBkADUAOAA3ADEAYwA2AGYANwA3AGIANwA2AGUANQAwADUAYQA2AGYAZQBkADAAZgAwAGEAMQA5ADAAMQBkADMAYQAwADcAYwAyAGIAZQAwADUAMgBiADYAMQBjAGYAMwA5ADcAMAA2ADUAMgBjADYAMwA5ADkAYgA0ADUAOQA='
            If ($ServerName -notmatch 'eyua.net$'){
                $ServerName += ".eyua.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Qq]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYQA\Q.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHcAdgA3AFcAcQBIAEYAUQAwAHoAUABNAGkAZAB4AHYANQBoAFcAVABWAHcAPQA9AHwANQAxADYAYwBiADcAOABmADMAYQAzADUAMgBlAGYANgBmAGUAMwA0ADEAMwA0AGMANwBkADcAYgA2ADMAZgAxADEAYQA4AGIAMQAxADcANwA2ADcAYwAwADYAMABkADQANwAyADcAMwA1ADMAYQA1ADYAZABlADgAMABmAGIAZQA='
            If ($ServerName -notmatch 'eyqa.net$'){
                $ServerName += ".eyqa.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYDEV\D.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEYANABNAFcAbgBTAHAAUQBMAGYAYgAxAGgASAAyAGUAQgBtAGYAZQBTAFEAPQA9AHwAMwBlAGEAZQA1AGYAMgBlAGUANABkADQAOABlAGYAMwBlAGMAMABhADYAMAAzADkAMgBjADgAYQAxADQAMgA0AGEAMQA4ADQAZQA4AGMAMAAzADgAMgA5AGIANwBmADEAYgA0ADkAZgBmADIAYgA3AGEAYQBhAGYAZABhADIANwA='
            If ($ServerName -notmatch 'eydev.net$'){
                $ServerName += ".eydev.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Pp]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPP.NET\C.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEEAaQBJADUAMgA1AEMAVABXAC8AbQBSAHgAWgBaAHgAUABoAHIAVwA0AEEAPQA9AHwAZAAwAGYAOQAzAGUAMwAzADIAMwA2AGYAZgA4ADUAOABlADkANQBkADcANwAzAGYAOQBiADMAMAA2AGIANwBkADMAZABiADIAOAAyAGMANgBhADEAYgA5ADQAZAAwAGYAYQA4ADMAMgAyADAANwA1ADQAZQA4ADkAOAAxADAAMAA='
            If ($ServerName -notmatch 'cloudapp.ey.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AFQAYgBWAGMANwBWAHQAbQBRAHAATgA0AHIAQQBUAEIAbQBiAG4AbgBtAEEAPQA9AHwAMgA5AGYAZgA3ADUAOQAwAGEAZAAwADYAYQA2ADQAZgAxAGMAYgBjADIANQBmAGEAYQAxADcAZgBmAGQAOQBkADYAZAAzAGYAOAAxADEAOAA4ADgAMAAyAGUANABkADAAYgBkADgANQBhAGEANwA1AGYAYgBlADIAMAAwADEAZAA='
            If ($ServerName -notmatch 'cloudapp.eydev.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Else {
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        }
    }
    If ($InventAccount -ne $null) {
		# retrieve the password.
		[Byte[]] $key = (1..16)  	
        $SecurePassword = ConvertTo-SecureString $DBAToolPassword -Key $key
        $InventCredential = New-Object System.Management.Automation.PSCredential ($InventAccount, $SecurePassword)
        $RemoteSessionOption = New-PSSessionOption -SkipCACheck -OpenTimeout 180000 -IdleTimeout 180000 #3 minutes
        if ($Session_Jumpy -ne $null) {
			$Session = Invoke-Command -Session $Session_Jumpy -ScriptBlock {New-PSSession -ComputerName $using:ServerName -Credential $using:InventCredential -SessionOption $using:RemoteSessionOption -ErrorAction Stop}
			return $Session_Jumpy, $Session.Name
		} else {
			$Session = New-PSSession -ComputerName $ServerName -Credential $InventCredential -SessionOption $RemoteSessionOption -ErrorAction Stop
			return $Session
		}
    }
}

Function JumpServer() {
	$InventAccount_Jumpy = "EY\P.SMOO.SQL"
	$DBAToolPassword_Jumpy = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
    [Byte[]] $key = (1..16)  	
    $SecurePassword_Jumpy = ConvertTo-SecureString $DBAToolPassword_Jumpy -Key $key
	$InventCredential_Jumpy = New-Object System.Management.Automation.PSCredential ($InventAccount_Jumpy, $SecurePassword_Jumpy)
	$RemoteSessionOption_Jumpy = New-PSSessionOption -SkipCACheck -OpenTimeout 180000 -IdleTimeout 180000 #3 minutes
	$Session_Jumpy = New-PSSession -ComputerName "USSECVMPDBTSQ01.ey.net" -Credential $InventCredential_Jumpy -SessionOption $RemoteSessionOption_Jumpy -ErrorAction Stop
	return $Session_Jumpy
}
