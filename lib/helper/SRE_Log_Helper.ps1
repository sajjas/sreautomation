# -----------------------------------------------------------------------------
function log {
	[CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, Position = 0)]
        [string]$Filename,

        [Parameter(Mandatory = $true, Position = 1)]
        [string]$Message
    )
	
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> log/$Filename
}